﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WCFConsumer.Calculator;

namespace WCFConsumer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int val1, val2, res;
            Service1Client ws = new Service1Client();

            if (String.IsNullOrEmpty(textBox1.Text))
                val1 = 0;
            else
                val1 = Int32.Parse(textBox1.Text);

            if (String.IsNullOrEmpty(textBox2.Text))
                val2 = 0;
            else
                val2 = Int32.Parse(textBox2.Text);

            res = ws.Add(val1, val2);
            textBox3.Text = Convert.ToString(res);
        }
    }
}
