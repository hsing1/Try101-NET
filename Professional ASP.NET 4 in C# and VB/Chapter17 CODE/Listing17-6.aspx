﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multiple Selector matches</title>
    <style type="text/css">
        p
        {
            font-family:Arial;
            color:Blue;
        }

        p#book
        {
            font-size:xx-large;
        }

        p.title
        {
            font-family: Courier New;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       <p id="book" class="title" style="letter-spacing:5pt;">Lorum Ipsum</p>
    </form>
</body>
</html>
