﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Basic HTML layout</title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="1">
            <tr>
                <td></td>
                <td valign="top"><h1>Lorem Ipsum</h1></td>
            </tr>
            <tr>
                <td valign="middle">
                    <ul>
                        <li>Home</li>
                        <li>Products</li>
                        <li>Services</li>
                        <li>Contact</li>
                    </ul>
                </td>
                <td>
                    <h2>Mauris ante odio, laoreet vel.</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris suscipit consectetur euismod. Maecenas risus mi, laoreet eu viverra mollis, commodo ut ipsum. Pellentesque ac scelerisque mauris. Ut ac pellentesque arcu. Donec sed risus vel erat mattis egestas ac in enim. Mauris tincidunt laoreet ipsum, ut faucibus libero porta at. Maecenas a ligula sapien. Aliquam erat volutpat. Phasellus nec nibh odio. Phasellus mollis, arcu eget vehicula mollis, mi magna semper nunc, id placerat mauris purus eu mi.</p>
                    <p>Donec a blandit mauris. Aliquam lorem lectus, faucibus quis vestibulum facilisis, convallis at mauris. Nam eleifend porttitor diam eget rutrum. Ut eget erat nec ipsum interdum fringilla eget eu ligula. Nullam nec dignissim est. Nunc convallis nisi eu tortor hendrerit viverra viverra velit pharetra. Duis malesuada tempus lectus, id vestibulum mi lobortis vel. Etiam et rhoncus odio. Vivamus sollicitudin, massa ac dignissim iaculis, ipsum tellus bibendum ante, et viverra lorem mauris eget leo. Duis id est libero, fringilla bibendum enim. In hac habitasse platea dictumst. Nulla faucibus, risus suscipit facilisis tempor, leo dolor pretium mi, non egestas arcu enim non elit. Maecenas congue condimentum tristique. Nulla elementum massa quis quam tincidunt id vulputate felis accumsan. Praesent ac venenatis neque. Donec at ligula nisi, ac scelerisque nisi.</p>
                    <p>Nunc elit elit, molestie eu malesuada vel, viverra commodo tortor. Sed est erat, congue quis rhoncus eget, porta at urna. Morbi porttitor metus justo, non fermentum dui. Nam imperdiet accumsan dignissim. Nullam auctor facilisis elit ut bibendum. Nullam non lectus urna. Aliquam venenatis aliquet cursus. Vivamus volutpat mattis ipsum, vel varius nulla consectetur nec. Morbi in felis non est ornare blandit non ac elit. Nulla tristique tellus vel eros rutrum commodo. Nam eget augue quis odio blandit ultrices a ut massa.</p>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
