﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multiple Class Selectors</title>
    <style type="text/css">
        p.title
        {
            font-family: Courier New;
            letter-spacing:5pt;
        }

        p.summer
        {
            color:Blue;
        }

        p.newproduct
        {
            font-weight:bold;
            color:Red;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <p class="title newproduct summer">Lorum Ipsum</p>
    </form>
</body>
</html>
