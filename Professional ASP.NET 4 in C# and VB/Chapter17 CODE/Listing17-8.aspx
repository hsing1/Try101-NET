﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Styling Overriding</title>
    <style type="text/css">
        p
        {
            font-family:Arial;
        }

        p
        {
            font-family: Courier New;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <p>Lorum Ipsum</p>
    </form>
</body>
</html>
