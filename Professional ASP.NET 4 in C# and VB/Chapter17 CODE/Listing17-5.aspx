﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Merging Styles</title>
    <style type="text/css">
         .title
        {
            text-decoration:underline;
        }

        p
        {
            font-family:Courier New;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <p class="title">Lorum Ipsum</p>
    </form>
</body>
</html>
