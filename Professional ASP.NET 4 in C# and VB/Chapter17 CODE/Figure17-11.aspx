﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Box Elements</title>
</head>
<body>   
    <form id="form1" runat="server">
        <div>
            Lorem <b>ipsum</b> dolor sit amet, consectetuer adipiscing elit.
            <div style="display:inline;">Donec et velit a risus <b>convallis</b> porttitor.
            Vestibulum nisi metus, imperdiet sed, mollis condimentum, 
            nonummy eu, magna.</div>
            <div>Duis lobortis felis in est. <span>Nulla eu velit ut nisi
            consequat vulputate.</span>
            </div>
            <i>Vestibulum vel metus.</i> Integer ut quam. Ut dignissim,
            sapien sit amet malesuada aliquam, quam quam vulputate nibh,
            ut pulvinar velit lorem at eros. Sed semper lacinia diam. In
            faucibus nonummy arcu. Duis venenatis interdum quam. Aliquam ut
            dolor id leo scelerisque convallis. Suspendisse non velit.
            Quisque nec metus. Lorem ipsum dolor sit amet, consectetuer 
            adipiscing elit. Praesent pellentesque interdum magna.
        </div>
    </form>
</body>
</html>
