<%@ Page Language="VB" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DataList Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                Company Name:
                <asp:Label ID="CompanyNameLabel" runat="server" 
                    Text='<%# Eval("CompanyName") %>' />
                <br />
                <br />
            </ItemTemplate>
            <AlternatingItemTemplate>
                CompanyName:
                <asp:Label ID="CompanyNameLabel" runat="server" 
                 BackColor="LightGray" 
                 Text='<%# Eval("CompanyName") %>' />
                <br />
                <br />
            </AlternatingItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:DSN_Northwind %>" 
            SelectCommand="SELECT [CompanyName] FROM [Customers]">
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
