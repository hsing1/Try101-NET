<%@ Page Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection DBCon;
        SqlCommand Command = new SqlCommand();
        SqlDataReader OrdersReader;
        IAsyncResult ASyncResult;

        DBCon = new SqlConnection();
        DBCon.ConnectionString =
          ConfigurationManager.ConnectionStrings["DSN_NorthWind"].ConnectionString;

        Command.CommandText = 
                "SELECT TOP 5 Customers.CompanyName, Customers.ContactName, " +
                "Orders.OrderID, Orders.OrderDate, " +
                "Orders.RequiredDate, Orders.ShippedDate " +
                "FROM Orders, Customers " +
                "WHERE Orders.CustomerID = Customers.CustomerID " +
                "ORDER BY Customers.CompanyName, Customers.ContactName";

        Command.CommandType = CommandType.Text;
        Command.Connection = DBCon;

        DBCon.Open();

        // Starting the asynchronous processing
        ASyncResult = Command.BeginExecuteReader();

        // This loop with keep the main thread waiting until the 
        // asynchronous process is finished
        while (!ASyncResult.IsCompleted)
        {
            // Sleeping current thread for 10 milliseconds
            System.Threading.Thread.Sleep(10);
        }

        // Retrieving result from the asynchronous process
        OrdersReader = Command.EndExecuteReader(ASyncResult);

        // Displaying result on the screen
        gvOrders.DataSource = OrdersReader;
        gvOrders.DataBind();

        // Closing connection
        DBCon.Close();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>The Poll Approach</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvOrders" runat="server" 
     AutoGenerateColumns="False" Width="100%">
       <Columns>
          <asp:BoundField HeaderText="Company Name" 
           DataField="CompanyName"></asp:BoundField>
          <asp:BoundField HeaderText="Contact Name" 
           DataField="ContactName"></asp:BoundField>
          <asp:BoundField HeaderText="Order Date" 
           DataField="orderdate" DataFormatString="{0:d}"></asp:BoundField>
          <asp:BoundField HeaderText="Required Date" DataField="requireddate" 
           DataFormatString="{0:d}"></asp:BoundField>
          <asp:BoundField HeaderText="Shipped Date" DataField="shippeddate" 
           DataFormatString="{0:d}"></asp:BoundField>
       </Columns>
    </asp:GridView>
    </div>
    </form>
</body>
</html>
