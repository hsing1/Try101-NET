<%@ Page Language="C#" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack )
        {
            SqlConnection MyConnection;
            SqlCommand MyCommand;
            DataTable MyDataTable;
            SqlDataReader MyReader;
            SqlParameter CityParam;

            MyConnection = new SqlConnection();
            MyConnection.ConnectionString = 
        ConfigurationManager.ConnectionStrings["DSN_Northwind"].ConnectionString;

            MyCommand = new SqlCommand();
            MyCommand.CommandText = 
               "SELECT * FROM CUSTOMERS WHERE CITY = @CITY";
            MyCommand.CommandType = CommandType.Text;
            MyCommand.Connection = MyConnection;

            CityParam = new SqlParameter();
            CityParam.ParameterName = "@CITY";
            CityParam.SqlDbType = SqlDbType.VarChar;
            CityParam.Size = 15;
            CityParam.Direction = ParameterDirection.Input;
            CityParam.Value = "London";

            MyCommand.Parameters.Add(CityParam);

            MyCommand.Connection.Open();
            MyReader = MyCommand.ExecuteReader(CommandBehavior.CloseConnection);

            MyDataTable = new DataTable();

            // Loading DataTable using a DataReader
            MyDataTable.Load(MyReader);

            gvCustomers.DataSource = MyDataTable;
            gvCustomers.DataBind();

            MyDataTable.Dispose();
            MyCommand.Dispose();
            MyConnection.Dispose();
        }
    }
</script>

<html>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvCustomers" runat="server">
        </asp:GridView>    
    </div>
    </form>
</body>
</html>
