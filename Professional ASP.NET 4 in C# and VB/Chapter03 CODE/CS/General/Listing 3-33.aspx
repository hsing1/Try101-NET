﻿<%@ Page Language="C#"%>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField1.Value = System.Guid.NewGuid().ToString();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>HiddenField Server Control</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:HiddenField ID="HiddenField1" runat="Server" />
    </form>
</body>
</html>
