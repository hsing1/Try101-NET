﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Button1_Click(object sender, EventArgs e)
    {
       ListBox1.Items.Add(TextBox1.Text.ToString());
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
       Label1.Text = "You selected from the ListBox:<br>";
       foreach (ListItem li in ListBox1.Items) {
           if (li.Selected) {
               Label1.Text += li.Text + "<br>";
           }
       }   
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Using the ListBox</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Add an additional item" 
         OnClick="Button1_Click" /><br /><br />
    
        <asp:ListBox ID="ListBox1" runat="server" SelectionMode="multiple">
           <asp:ListItem>Hematite</asp:ListItem>
           <asp:ListItem>Halite</asp:ListItem>
           <asp:ListItem>Limonite</asp:ListItem>
           <asp:ListItem>Magnetite</asp:ListItem>
        </asp:ListBox><br /><br />

        <asp:Button ID="Button2" runat="server" Text="Submit" 
         OnClick="Button2_Click" /><br /><br />

        <asp:Label ID="Label1" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
