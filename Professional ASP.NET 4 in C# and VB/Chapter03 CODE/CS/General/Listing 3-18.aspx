﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        TableRow tr = new TableRow();

        TableCell fname = new TableCell();
        fname.Text = "Scott";
        tr.Cells.Add(fname);

        TableCell lname = new TableCell();
        lname.Text = "Hanselman";
        tr.Cells.Add(lname);

        Table1.Rows.Add(tr);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Table</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Table ID="Table1" runat="server">
        </asp:Table>
    </div>
    </form>
</body>
</html>
