﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Button1_Click(object sender, EventArgs e)
    {
        // Put in your own image.
       Image1.ImageUrl = "~/MyImage2.gif";
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Image control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MyImage1.gif" /><br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Change Image" 
         OnClick="Button1_Click" />
    </div>
    </form>
</body>
</html>
