﻿<%@ Page Language="C#"%>

<script runat="server">
    protected void Imagemap1_Click(object sender,
       System.Web.UI.WebControls.ImageMapEventArgs e) {
          
          Response.Write("You selected: " + e.PostBackValue);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>ImageMap Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <!-- You will have to supply your own image here -->
        <asp:ImageMap ID="Imagemap1" runat="server" ImageUrl="Kids.jpg" 
         Width="300" OnClick="Imagemap1_Click" HotSpotMode="PostBack">
            <asp:RectangleHotSpot Top="0" Bottom="225" Left="0" Right="150" 
             AlternateText="Sofia" PostBackValue="Sofia">
            </asp:RectangleHotSpot>
            <asp:RectangleHotSpot Top="0" Bottom="225" Left="151" Right="300" 
             AlternateText="Henri" PostBackValue="Henri">
            </asp:RectangleHotSpot>
        </asp:ImageMap>
    </form>
</body>
</html>
