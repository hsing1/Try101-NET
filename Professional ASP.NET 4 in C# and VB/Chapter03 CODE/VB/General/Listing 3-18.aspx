﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim tr As New TableRow()
       
        Dim fname As New TableCell()
        fname.Text = "Scott"
        tr.Cells.Add(fname)
        
        Dim lname As New TableCell()
        lname.Text = "Hanselman"
        tr.Cells.Add(lname)
        
        Table1.Rows.Add(tr)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Table</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Table ID="Table1" runat="server">
        </asp:Table>
    </div>
    </form>
</body>
</html>
