﻿<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Response.Write("Thanks for your donation!")
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CheckBox control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:CheckBox ID="CheckBox1" runat="server" Text="Donate $10 to our cause!" 
         OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" />    
    </div>
    </form>
</body>
</html>
