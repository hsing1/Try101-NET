﻿<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Put in your own image
        Image1.ImageUrl = "~/MyImage2.gif"
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Image control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Image ID="Image1" runat="server" ImageUrl="~/MyImage1.gif" /><br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Change Image" 
         OnClick="Button1_Click" />
    </div>
    </form>
</body>
</html>
