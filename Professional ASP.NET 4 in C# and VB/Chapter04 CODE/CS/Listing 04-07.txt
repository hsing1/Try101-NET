<asp:DropDownList id="DropDownList1" runat="server">
   <asp:ListItem Selected="True">Select a profession</asp:ListItem>
   <asp:ListItem>Programmer</asp:ListItem>
   <asp:ListItem>Lawyer</asp:ListItem>
   <asp:ListItem>Doctor</asp:ListItem>
   <asp:ListItem>Artist</asp:ListItem>
</asp:DropDownList>
  &nbsp;
<asp:RequiredFieldValidator id="RequiredFieldValidator1"  
  runat="server" Text="Please make a selection" 
  ControlToValidate="DropDownList1" 
  InitialValue="Select a profession">
</asp:RequiredFieldValidator>
