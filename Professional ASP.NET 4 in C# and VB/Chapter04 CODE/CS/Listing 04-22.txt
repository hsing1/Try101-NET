protected void Page_Load(Object sender, EventArgs e) {
   foreach(BaseValidator bv in Page.Validators)
   {
      bv.EnableClientScript = false;
   }
}