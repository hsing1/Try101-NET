<%@ Page Language="VB" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Validation Groups</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>St. Louis .NET User Group</h1>
        <p>Username:
        <asp:TextBox ID="TextBox1" Runat="server"></asp:TextBox>&nbsp; Password:
        <asp:TextBox ID="TextBox2" Runat="server" 
         TextMode="Password"></asp:TextBox>&nbsp;
        <asp:Button ID="Button1" Runat="server" Text="Login" 
         ValidationGroup="Login" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Runat="server" 
             Text="* You must submit a username!"
             ControlToValidate="TextBox1" ValidationGroup="Login">
            </asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Runat="server" 
             Text="* You must submit a password!"
             ControlToValidate="TextBox2" ValidationGroup="Login">
            </asp:RequiredFieldValidator>
        <p>
            Our main meeting is almost always held on the last Monday of the month. 
            Sometimes due to holidays or other extreme circumstances, 
            we move it to another night but that is very rare. Check the home page 
            of the web site for details. The special
            interest groups meet at other times during the month. Check the SIG 
            page and visit their individual sites for more information.
            You can also check our calendar page for a summary of events.<br />
        </p>
        <h2>Sign-up for the newsletter!</h2>
        <p>Email:
        <asp:TextBox ID="TextBox3" Runat="server"></asp:TextBox>&nbsp;
        <asp:Button ID="Button2" Runat="server" Text="Sign-up" 
         ValidationGroup="Newsletter" />&nbsp;
            <br />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
             Runat="server"
             Text="* You must submit a correctly formatted email address!"
             ControlToValidate="TextBox3" ValidationGroup="Newsletter"
             ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
            </asp:RegularExpressionValidator>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Runat="server" 
             Text="* You forgot your email address!"
             ControlToValidate="TextBox3" ValidationGroup="Newsletter">
            </asp:RequiredFieldValidator>
        </p>
    </div>
    </form>
</body>
</html>
