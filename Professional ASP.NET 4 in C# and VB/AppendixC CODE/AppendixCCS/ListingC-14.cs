﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AppendixCCS
{
    public class ListingC_14
    {
        private bool _status = false;

        public ListingC_14()
        {
        }

        [System.Windows.Browser.ScriptableMember]
        public string FirstName { get; set; }
        [System.Windows.Browser.ScriptableMember]
        public string LastName { get; set; }
        public string Department { get; set; }
        public string SSN { get; set; }
        [System.Windows.Browser.ScriptableMember]
        public DateTime StartDate { get; set; }
        [System.Windows.Browser.ScriptableMember]
        public bool Status { get { return _status; } }

        public void ChangeStatus(bool status)
        {
            this._status = status;
        }

        public string GetFullName()
        {
            return string.Format("{0} {1)", this.FirstName, this.LastName);
        }
    }
}
