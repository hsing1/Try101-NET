﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AppendixCCS
{
    [System.Windows.Browser.ScriptableType()]
    public class ListingC_11
    {
        private bool _status = false;

        public ListingC_11()
        {
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public string SSN { get; set; }
        public DateTime StartDate { get; set; }
        public bool Status { get { return _status; } }

        public void ChangeStatus(bool status)
        {
            this._status = status;
        }

        public string GetFullName()
        {
            return string.Format("{0} {1)", this.FirstName, this.LastName);
        }
    }
}
