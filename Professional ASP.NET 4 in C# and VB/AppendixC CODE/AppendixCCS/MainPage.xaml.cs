﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AppendixCCS
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //AddListItem();
            //Calculate();
        }

        // **** LISTING C-16 ****
        public void AddListItem()
        {
            System.Windows.Browser.HtmlElement unorderedlist = 
                System.Windows.Browser.HtmlPage.Document.GetElementById(
                    "demoList");

            if (unorderedlist != null)
            {
                System.Windows.Browser.HtmlElement listitem =
                    System.Windows.Browser.HtmlPage.Document.CreateElement(
                        "li");
                listitem.SetAttribute("Id", "listitem1");
                listitem.SetAttribute("innerHTML", "Hello World!");
                unorderedlist.AppendChild(listitem);
            }
        }

        // **** LISTING C-17 ****
        public void Calculate()
        {
            var calculator =
                System.Windows.Browser.HtmlPage.Window.CreateInstance(
                    "Calculator");
            var sum = Convert.ToInt32(calculator.Invoke("add", 5, 1));
            System.Windows.Browser.HtmlPage.Window.Alert(sum.ToString());
        }
    }
}
