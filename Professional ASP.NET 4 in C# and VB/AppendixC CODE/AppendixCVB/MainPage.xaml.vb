﻿Partial Public Class MainPage
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub UserControl_Loaded(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        'AddListItem()
        'Calculate()
    End Sub

    ' **** LISTING C-16 ****
    Public Sub AddListItem()
        Dim unorderedlist As System.Windows.Browser.HtmlElement =
            System.Windows.Browser.HtmlPage.Document.GetElementById(
                "demoList")

        If unorderedlist IsNot Nothing Then

            Dim listitem As System.Windows.Browser.HtmlElement =
                System.Windows.Browser.HtmlPage.Document.CreateElement(
                    "li")
            listitem.SetAttribute("Id", "listitem1")
            listitem.SetAttribute("innerHTML", "Hello World!")
            unorderedlist.AppendChild(listitem)
        End If
    End Sub

    ' **** LISTING C-17 ****
    Public Sub Calculate()
        Dim calculator =
            System.Windows.Browser.HtmlPage.Window.CreateInstance(
                "Calculator")
        Dim sum = Convert.ToInt32(calculator.Invoke("add", 5, 1))
        System.Windows.Browser.HtmlPage.Window.Alert(sum.ToString())
    End Sub
End Class
