﻿<System.Windows.Browser.ScriptableType()>
Public Class ListingC_11 'Employee Class
    Private _status As Boolean = False

    Public Sub New()

    End Sub

    Public Property FirstName() As String
    Public Property LastName() As String
    Public Property Department() As String
    Public Property SSN() As String
    Public Property StartDate() As DateTime

    Public ReadOnly Property Status As Boolean
        Get
            Return _status
        End Get
    End Property

    Public Sub ChangeStatus(ByVal status As Boolean)
        Me._status = status
    End Sub

    Public Function GetFullName() As String
        Return String.Format("{0} {1}", Me.FirstName, Me.LastName)
    End Function
End Class
