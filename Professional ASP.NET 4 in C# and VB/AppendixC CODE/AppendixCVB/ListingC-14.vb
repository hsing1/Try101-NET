﻿Public Class ListingC_14 'Employee Class
    Private _status As Boolean = False

    Public Sub New()

    End Sub
    <System.Windows.Browser.ScriptableMember()>
    Public Property FirstName() As String
    <System.Windows.Browser.ScriptableMember()>
    Public Property LastName() As String
    Public Property Department() As String
    Public Property SSN() As String
    <System.Windows.Browser.ScriptableMember()>
    Public Property StartDate() As DateTime

    <System.Windows.Browser.ScriptableMember()>
    Public ReadOnly Property Status As Boolean
        Get
            Return _status
        End Get
    End Property

    Public Sub ChangeStatus(ByVal status As Boolean)
        Me._status = status
    End Sub

    Public Function GetFullName() As String
        Return String.Format("{0} {1}", Me.FirstName, Me.LastName)
    End Function
End Class
