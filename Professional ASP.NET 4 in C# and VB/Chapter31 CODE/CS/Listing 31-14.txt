[WebService(Namespace = "http://www.wrox.com/helloworld")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class HelloSoapHeader : System.Web.Services.WebService
{

    public HelloHeader myHeader;

    [WebMethod]
    [SoapHeader("myHeader")]
    public string HelloWorld() {
        if (myHeader == null) {
           return "Hello World";
        }
        else {
           return "Hello " + myHeader.Username + ". " +
              "<br>Your password is: " + myHeader.Password;
        }
    }
    
}
