protected void Button1_Click(object sender, EventArgs e)
{
   ServiceReference.HelloCustomerClient ws = new 
      ServiceReference.HelloCustomerClient();
   ServiceReference.Customer myCustomer = new ServiceReference.Customer();

   myCustomer.Firstname = "Bill";
   myCustomer.Lastname = "Evjen";

   Response.Write(ws.HelloFullName(myCustomer));

   ws.Close();
}
