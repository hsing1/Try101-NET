using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;

[WebService(Namespace = "http://www.wrox.com/customers")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Customers : System.Web.Services.WebService
{

    [WebMethod]
    public DataSet GetCustomers() {
       SqlConnection conn;
       SqlDataAdapter myDataAdapter;
       DataSet myDataSet;
       string cmdString = "Select * From Customers";

       conn = new SqlConnection("Server=localhost;uid=sa;pwd=;database=Northwind");
       myDataAdapter = new SqlDataAdapter(cmdString, conn);

       myDataSet = new DataSet();
       myDataAdapter.Fill(myDataSet, "Customers");

       return myDataSet;
    }
    
}
