Dim svc As New NORTHWNDEntities(New _
   Uri("http://localhost:4113/NorthwindDataService.svc").ToString())

Dim query = From c In svc.Customers 
            Where c.Country.Contains("Germany") 
            Select c

GridView1.DataSource = query
GridView1.DataBind()
