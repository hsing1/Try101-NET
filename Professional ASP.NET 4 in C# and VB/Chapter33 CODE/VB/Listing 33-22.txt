<system.web>
    <authorization>
        <deny verbs="GET" roles="Admin" />
        <deny verbs="POST" users="*" />
    </authorization>
</system.web>
