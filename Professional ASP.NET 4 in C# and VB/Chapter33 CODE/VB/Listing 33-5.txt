<IPermission
   class="FileIOPermission"
   version="1"
   Read="$AppDir$"
   Write="$AppDir$"
   Append="$AppDir$"
   PathDiscovery="$AppDir$"
/>
