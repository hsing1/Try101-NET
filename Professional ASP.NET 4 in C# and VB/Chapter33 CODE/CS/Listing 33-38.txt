// Connect to the web application Expense on Optra.Microsoft.com server 
Configuration configSetting =
   System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration
   ("/Expense", "1", "Optra.Microsoft.com");

ConfigurationSection section = configSetting.GetSection("appSettings");

KeyValueConfigurationElement element = 
   configSetting.AppSettings.Settings["keySection"];

if (element != null)
{
   string value = "New Value";
   element.Value = value;

   try
   {
      configSetting.Save();
   }
      catch (Exception ex)
   {
      Response.Write(ex.Message);
   }
}
