<%@ Page Language="C#" %>
<%@ Import Namespace="System.Web.Management" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        MailEventNotificationInfo meni = 
          TemplatedMailWebEventProvider.CurrentNotification;
        Label1.Text = "Events Discarded By Buffer: " + 
          meni.EventsDiscardedByBuffer.ToString();
        Label2.Text = "Events Discarded Due To Message Limit: " +  
          meni.EventsDiscardedDueToMessageLimit.ToString();
        Label3.Text = "Events In Buffer: " + meni.EventsInBuffer.ToString();
        Label4.Text = "Events In Notification: " + 
          meni.EventsInNotification.ToString();
        Label5.Text = "Events Remaining: " + meni.EventsRemaining.ToString();
        Label6.Text = "Last Notification UTC: " + 
          meni.LastNotificationUtc.ToString();
        Label7.Text = "Number of Messages In Notification: " + 
          meni.MessagesInNotification.ToString();

        DetailsView1.DataSource = meni.Events;
        DetailsView1.DataBind();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
   <head><title>My Page</title></head>
   <body>
      <form id="form1" runat="server">
         <asp:label id="Label1" runat="server"></asp:label><br />
         <asp:label id="Label2" runat="server"></asp:label><br />
         <asp:label id="Label3" runat="server"></asp:label><br />
         <asp:label id="Label4" runat="server"></asp:label><br />
         <asp:label id="Label5" runat="server"></asp:label><br />
         <asp:label id="Label6" runat="server"></asp:label><br />
         <asp:label id="Label7" runat="server"></asp:label><br />

         <br />

         <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" 
          Width="500px" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" 
          BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
            <EditRowStyle BackColor="#738A9C" Font-Bold="True" 
             ForeColor="#F7F7F7" />
            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" 
             HorizontalAlign="Right" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" 
             ForeColor="#F7F7F7" />
            <AlternatingRowStyle BackColor="#F7F7F7" />
         </asp:DetailsView>
      </form>
   </body>
</html>
