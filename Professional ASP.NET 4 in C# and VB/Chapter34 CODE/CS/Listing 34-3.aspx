<%@ Page Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            List<string> pcc = new List<string>();
                           
            foreach (PerformanceCounterCategory item in 
               PerformanceCounterCategory.GetCategories())
            {
                pcc.Add(item.CategoryName);
            }

            pcc.Sort();
            pcc.Remove(".NET CLR Data");

            DropDownList1.DataSource = pcc;
            DropDownList1.DataBind();

            PerformanceCounterCategory myPcc;
            myPcc = new 
               PerformanceCounterCategory(DropDownList1.SelectedItem.Text);

            DisplayCounters(myPcc);
        } 
    }

    void DisplayCounters(PerformanceCounterCategory pcc)
    {
        DisplayInstances(pcc);
        
        List<string> myPcc = new List<string>();

        if (DropDownList3.Items.Count > 0)
        {
            foreach (PerformanceCounter pc in 
               pcc.GetCounters(DropDownList3.Items[0].Value))
            {
                myPcc.Add(pc.CounterName);
            }
        }
        else
        {
            foreach (PerformanceCounter pc in pcc.GetCounters())
            {
                myPcc.Add(pc.CounterName);
            }
        }

        myPcc.Sort();

        DropDownList2.DataSource = myPcc;
        DropDownList2.DataBind();
    }

    void DisplayInstances(PerformanceCounterCategory pcc)
    {
        List<string> listPcc = new List<string>();

        foreach (string item in pcc.GetInstanceNames())
        {
            listPcc.Add(item.ToString());
        }

        listPcc.Sort();

        DropDownList3.DataSource = listPcc;
        DropDownList3.DataBind();
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        PerformanceCounterCategory pcc;
        pcc = new PerformanceCounterCategory(DropDownList1.SelectedItem.Text);

        DropDownList2.Items.Clear();
        DropDownList3.Items.Clear();

        DisplayCounters(pcc);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        PerformanceCounter pc;
        if (DropDownList3.Items.Count > 0)
        {
            pc = new PerformanceCounter(DropDownList1.SelectedItem.Text, 
              DropDownList2.SelectedItem.Text, DropDownList3.SelectedItem.Text);
        }
        else
        {
            pc = new PerformanceCounter(DropDownList1.SelectedItem.Text, 
              DropDownList2.SelectedItem.Text);
        }

        Label1.Text = "<b>Latest Value:</b> " + pc.NextValue().ToString();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Working with Performance Counters</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong>Performance Object:</strong><br />
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True"   
         OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList><br />
        <br />
        <strong>Performance Counter:</strong><br />
        <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList><br />
        <br />
        <strong>Instances:</strong><br />
        <asp:DropDownList ID="DropDownList3" runat="server">
        </asp:DropDownList><br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Retrieve Value" /><br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label></div>
    </form>
</body>
</html>
