<configuration>
   <system.web>

      <healthMonitoring enabled="true">

         <eventMappings>
            <!-- Code removed for clarity -->
         </eventMappings>

         <providers>
            <!-- Code removed for clarity -->
         </providers>

         <rules>
            
            <clear />

            <add name="All Errors Default" eventName="All Errors" 
             provider="EventLogProvider"
             profile="Default" minInstances="1" maxLimit="Infinite" 
             minInterval="00:01:00" custom="" />
            <add name="Failure Audits Default" eventName="Failure Audits"
             provider="EventLogProvider" profile="Default" minInstances="1"
             maxLimit="Infinite" minInterval="00:01:00" custom="" />

         </rules>

      </healthMonitoring>

   </system.web>
</configuration>
