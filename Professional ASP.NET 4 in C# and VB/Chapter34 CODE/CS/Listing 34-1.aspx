<%@ Page Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script runat="server">
    protected void Button1_Click(object sender, EventArgs e)
    {
        EventLog el = new EventLog();
        el.Source = DropDownList1.SelectedItem.Text;

        GridView1.DataSource = el.Entries;
        GridView1.DataBind();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Working with Event Logs</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem>Application</asp:ListItem>
            <asp:ListItem>Security</asp:ListItem>
            <asp:ListItem>System</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Submit" /><br />
        <br />
        <asp:GridView ID="GridView1" runat="server" 
         BackColor="LightGoldenrodYellow" BorderColor="Tan"
            BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None">
            <FooterStyle BackColor="Tan" />
            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
             HorizontalAlign="Center" />
            <HeaderStyle BackColor="Tan" Font-Bold="True" />
            <AlternatingRowStyle BackColor="PaleGoldenrod" />
            <RowStyle VerticalAlign="Top" />
        </asp:GridView>    
    </div>
    </form>
</body>
</html>
