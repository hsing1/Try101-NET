<%@ Page Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!EventLog.SourceExists("My Application"))
        {
            EventLog.CreateEventSource("My Application", "Application");
        }
    }
    
    protected void Button1_Click(object sender, EventArgs e)
    {
        EventLog el = new EventLog();
        el.Source = "My Application";
        el.WriteEntry(TextBox1.Text);

        Label1.Text = "ENTERED: " + TextBox1.Text;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Working with Event Logs</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server" Height="75px" 
         TextMode="MultiLine" Width="250px"></asp:TextBox><br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Submit to custom event log" /><br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>&nbsp;</div>
    </form>
</body>
</html>
