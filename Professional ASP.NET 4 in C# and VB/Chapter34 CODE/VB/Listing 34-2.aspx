<%@ Page Language="VB" %>
<%@ Import Namespace="System.Diagnostics" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, 
      ByVal e As System.EventArgs)

        If Not EventLog.SourceExists("My Application") Then
            EventLog.CreateEventSource("My Application", "Application")
        End If
    End Sub
    
    Protected Sub Button1_Click(ByVal sender As Object, 
      ByVal e As System.EventArgs)

        Dim el As EventLog = New EventLog()
        el.Source = "My Application"
        el.WriteEntry(TextBox1.Text)

        Label1.Text = "ENTERED: " & TextBox1.Text
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Working with Event Logs</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server" Height="75px" 
         TextMode="MultiLine" Width="250px"></asp:TextBox><br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Submit to custom event log" /><br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label>&nbsp;</div>
    </form>
</body>
</html>
