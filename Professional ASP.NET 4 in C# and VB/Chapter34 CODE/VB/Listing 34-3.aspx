<%@ Page Language="VB" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            Dim pcc As List(Of String) = New List(Of String)
                           
            For Each item As PerformanceCounterCategory In _
              PerformanceCounterCategory.GetCategories()
                pcc.Add(item.CategoryName)
            Next

            pcc.Sort()
            pcc.Remove(".NET CLR Data")

            DropDownList1.DataSource = pcc
            DropDownList1.DataBind()

            Dim myPcc As PerformanceCounterCategory
            myPcc = New PerformanceCounterCategory(DropDownList1.SelectedItem.Text)

            DisplayCounters(myPcc)
        End If
    End Sub
    
    Protected Sub DisplayCounters(ByVal pcc As PerformanceCounterCategory)
        DisplayInstances(pcc)
        
        Dim myPcc As List(Of String) = New List(Of String)

        If DropDownList3.Items.Count > 0 Then
            For Each pc As PerformanceCounter In _
              pcc.GetCounters(DropDownList3.Items(0).Value)
                myPcc.Add(pc.CounterName)
            Next
        Else
            For Each pc As PerformanceCounter In pcc.GetCounters()
                myPcc.Add(pc.CounterName)
            Next
        End If

        myPcc.Sort()

        DropDownList2.DataSource = myPcc
        DropDownList2.DataBind()
    End Sub
    
    Protected Sub DisplayInstances(ByVal pcc As PerformanceCounterCategory)
        Dim listPcc As List(Of String) = New List(Of String)

        For Each item As String In pcc.GetInstanceNames()
            listPcc.Add(item.ToString())
        Next

        listPcc.Sort()

        DropDownList3.DataSource = listPcc
        DropDownList3.DataBind()
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Dim pcc As PerformanceCounterCategory
        pcc = New PerformanceCounterCategory(DropDownList1.SelectedItem.Text)

        DropDownList2.Items.Clear()
        DropDownList3.Items.Clear()

        DisplayCounters(pcc)
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, _
      ByVal e As System.EventArgs)
        Dim pc As PerformanceCounter
        
        If DropDownList3.Items.Count > 0 Then
            pc = New PerformanceCounter(DropDownList1.SelectedItem.Text, _
              DropDownList2.SelectedItem.Text, DropDownList3.SelectedItem.Text)
        Else
            pc = New PerformanceCounter(DropDownList1.SelectedItem.Text, _
              DropDownList2.SelectedItem.Text)
        End If

        Label1.Text = "<b>Latest Value:</b> " & pc.NextValue().ToString()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Working with Performance Counters</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong>Performance Object:</strong><br />
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True"   
         OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList><br />
        <br />
        <strong>Performance Counter:</strong><br />
        <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList><br />
        <br />
        <strong>Instances:</strong><br />
        <asp:DropDownList ID="DropDownList3" runat="server">
        </asp:DropDownList><br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" 
         Text="Retrieve Value" /><br />
        <br />
        <asp:Label ID="Label1" runat="server"></asp:Label></div>
    </form>
</body>
</html>
