﻿Partial Class SlideShowExtender
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethodAttribute()> _
    <System.Web.Script.Services.ScriptMethodAttribute()> _
    Public Shared Function GetSlides(ByVal contextKey As System.String) _
      As AjaxControlToolkit.Slide()

        Return New AjaxControlToolkit.Slide() { _
           New AjaxControlToolkit.Slide("Images/Creek.jpg", _
              "The Creek", "This is a picture of a creek."), _
           New AjaxControlToolkit.Slide("Images/Dock.jpg", _
              "The Dock", "This is a picture of a Dock."), _
           New AjaxControlToolkit.Slide("Images/Garden.jpg", _
              "The Garden", "This is a picture of a Garden.")}
    End Function
End Class
