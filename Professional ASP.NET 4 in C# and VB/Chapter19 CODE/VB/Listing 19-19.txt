<ItemTemplate>
   <tr style="background-color:#DCDCDC;color:#000000;">
      <td>
         <asp:HoverMenuExtender ID="HoverMenuExtender1" runat="server"
          TargetControlID="ProductNameLabel" PopupControlID="Panel1"
          PopDelay="25" OffsetX="-50">
         </asp:HoverMenuExtender>
         <asp:Panel ID="Panel1" runat="server" Height="50px"
          Width="125px">
            <asp:Button ID="EditButton" runat="server"
             CommandName="Edit" Text="Edit" />
         </asp:Panel>                      
      </td>
      <td>
         <asp:Label ID="ProductIDLabel" runat="server"
          Text='<%# Eval("ProductID") %>' />
      </td>
      <td>
         <asp:Label ID="ProductNameLabel" runat="server"
          Text='<%# Eval("ProductName") %>' />
      </td>

      <!-- Code removed for clarity -->

   </tr>
</ItemTemplate>
