<%@ Page Language="VB" %>

<%@ Register Assembly="AjaxControlToolkit, Version=3.5.11119.20050, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MaskedEditExtender Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
         TargetControlID="TextBox1" MaskType="Number" Mask="999">
        </asp:MaskedEditExtender>        
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:MaskedEditValidator ID="MaskedEditValidator1" runat="server" 
         ControlExtender="MaskedEditExtender1" ControlToValidate="TextBox1" 
         IsValidEmpty="False" EmptyValueMessage="A three digit number is required!" 
         Display="Dynamic"></asp:MaskedEditValidator>
    </div>
    </form>
</body>
</html>
