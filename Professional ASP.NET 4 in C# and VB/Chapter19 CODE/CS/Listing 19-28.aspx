<%@ Page Language="C#" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RoundedCornersExtender Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:RoundedCornersExtender ID="RoundedCornersExtender1" runat="server" 
         TargetControlID="Panel1">
        </asp:RoundedCornersExtender>
        <asp:Panel ID="Panel1" runat="server" Width="250px" 
         HorizontalAlign="Center" BackColor="Orange">
            <asp:Login ID="Login1" runat="server">
            </asp:Login>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
