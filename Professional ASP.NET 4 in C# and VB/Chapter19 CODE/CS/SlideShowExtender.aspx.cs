﻿public partial class SlideShowExtender : System.Web.UI.Page
{
    [System.Web.Services.WebMethodAttribute(),
     System.Web.Script.Services.ScriptMethodAttribute()]
    public static AjaxControlToolkit.Slide[] GetSlides(string contextKey)
    {
        return new AjaxControlToolkit.Slide[] {
            new AjaxControlToolkit.Slide("Images/Creek.jpg", 
              "The Creek", "This is a picture of a creek."),
            new AjaxControlToolkit.Slide("Images/Dock.jpg", 
              "The Dock", "This is a picture of a Dock."),
            new AjaxControlToolkit.Slide("Images/Garden.jpg", 
              "The Garden", "This is a picture of a Garden.") };
    }
}
