<%@ Page Language="C#" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        Image1.ImageUrl = "Images/Creek.jpg";
    }
    
    protected void Option_Click(object sender, EventArgs e)
    {
        Image1.ImageUrl = "Images/" + ((LinkButton)sender).Text + ".jpg";
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DropDownExtender Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:DropDownExtender ID="DropDownExtender1" runat="server" 
                 DropDownControlID="Panel1" TargetControlID="Image1">
                </asp:DropDownExtender>
                <asp:Image ID="Image1" runat="server">
                </asp:Image>
            <asp:Panel ID="Panel1" runat="server" Height="50px" Width="125px">
                <asp:LinkButton ID="Option1" runat="server" 
                 OnClick="Option_Click">Creek</asp:LinkButton>
                <asp:LinkButton ID="Option2" runat="server" 
                 OnClick="Option_Click">Dock</asp:LinkButton>
                <asp:LinkButton ID="Option3" runat="server" 
                 OnClick="Option_Click">Garden</asp:LinkButton>
            </asp:Panel>  
            </ContentTemplate>
        </asp:UpdatePanel>    
    </div>
    </form>
</body>
</html>
