    <Bindable(True), Category("Appearance"), DefaultValue("Enter Value"), _
     Localizable(True), Themeable(False)> Property HeaderText() As String
        Get
            Dim s As String = CStr(ViewState("HeaderText"))
            If s Is Nothing Then
                Return String.Empty
            Else
                Return s
            End If
        End Get

        Set(ByVal Value As String)
            ViewState("HeaderText") = Value
        End Set
    End Property
