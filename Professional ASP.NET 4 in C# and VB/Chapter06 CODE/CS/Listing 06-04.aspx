<%@ Page Language="C#" Theme="Summer" %>

<script runat="server">    
    protected void Button1_Click(object sender, System.EventArgs e)
    {
        Label1.Text = "Hello " + TextBox1.Text.ToString();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>St. Louis .NET User Group</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Textbox ID="TextBox1" Runat="server">
        </asp:Textbox>
        <br />
        <br />
        <asp:Button ID="Button1" Runat="server" Text="Submit Your Name" 
         OnClick="Button1_Click" />
        <br />
        <br />
        <asp:Label ID="Label1" Runat="server" />
    </form>
</body>
</html>
