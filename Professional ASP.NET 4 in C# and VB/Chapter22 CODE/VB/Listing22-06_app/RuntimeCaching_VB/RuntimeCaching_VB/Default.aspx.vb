﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Runtime.Caching
Imports System.Xml.Linq

Partial Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object,
      ByVal e As EventArgs)

        Dim cache As ObjectCache = MemoryCache.Default

        Dim usernameFromXml As String =
          TryCast(cache("userFromXml"), String)

        If usernameFromXml Is Nothing Then
            Dim userFilePath As New List(Of String)()
            userFilePath.Add("C:\Username.xml")

            Dim policy As New CacheItemPolicy()
            policy.ChangeMonitors.Add(New HostFileChangeMonitor(userFilePath))

            Dim xdoc As XDocument =
              XDocument.Load("C:\Username.xml")
            Dim query = From u In xdoc.Elements("usernames")
                Select u.Value

            usernameFromXml = query.First().ToString()

            cache.Set("userFromXml", usernameFromXml, policy)
        End If

        Label1.Text = usernameFromXml
    End Sub
End Class
