
Partial Class _Default
    Inherits System.Web.UI.Page
    Public Shared Function GetUpdatedTime(ByVal context As HttpContext) As String
        Return DateTime.Now.ToLongTimeString() + " by " + 
                context.User.Identity.Name
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) 
            Handles Me.Load
        Label1.Text = DateTime.Now.ToLongTimeString()
    End Sub
End Class

