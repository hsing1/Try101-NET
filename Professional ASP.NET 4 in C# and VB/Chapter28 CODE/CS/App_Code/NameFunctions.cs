using System;

namespace NameComponent
{
    public class NameFunctions
    {

        private string m_FirstName;
        private string m_LastName;

        public string FirstName
        {
            get
            {
                return m_FirstName;
            }
            set
            {
                m_FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return m_LastName;
            }
            set
            {
                m_LastName = value;
            }
        }

        public string FullName
        {
            get
            {
                return m_FirstName + " " + m_LastName;
            }
            set
            {
                m_FirstName = value.Split(' ')[0];
                m_LastName = value.Split(' ')[1];
            }
        }

        public long FullNameLength
        {
            get
            {
                return this.FullName.Length;
            }
        }

    }
}
