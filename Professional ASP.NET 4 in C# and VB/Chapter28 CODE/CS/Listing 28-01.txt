' VB6 Code

Option Explicit

Private m_sFirstName As String
Private m_sLastName As String

Public Property Let FirstName(Value As String)
  m_sFirstName = Value
End Property

Public Property Get FirstName() As String
  FirstName = m_sFirstName
End Property
    
Public Property Let LastName(Value As String)
  m_sLastName = Value
End Property

Public Property Get LastName() As String
  LastName = m_sLastName
End Property

Public Property Let FullName(Value As String)
  m_sFirstName = Split(Value, " ")(0)
  If (InStr(Value, " ") > 0) Then
    m_sLastName = Split(Value, " ")(1)
  Else
    m_sLastName = ""
  End If
End Property

Public Property Get FullName() As String
  FullName = m_sFirstName + " " + m_sLastName
End Property

Public Property Get FullNameLength() As Long
  FullNameLength = Len(Me.FullName)
End Property
