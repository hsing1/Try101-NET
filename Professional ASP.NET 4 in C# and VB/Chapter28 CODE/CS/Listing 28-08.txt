using System;

namespace DivideComponent
{
    public class CustomException:Exception
    {
      public CustomException(string message):base(message)
      {
      }
    }

  public class DivideFunction
  {
    public double Divide(double Numerator, double Denominator)
    {
      if ((Numerator > 1000) || (Denominator > 1000))
        throw new CustomException("Numerator and denominator " +
                    "both have to be less than or equal to 1000.");
 
      return Numerator / Denominator;
    }
  }
}
