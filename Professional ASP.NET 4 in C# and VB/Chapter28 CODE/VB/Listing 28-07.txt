' VB6 Code

Option Explicit

Public Sub Main()

  Dim o As Object
    
  Set o = CreateObject("NameComponent.NameFunctions")
    
  o.FirstName = "Bill"
  o.LastName = "Evjen"
    
  MsgBox "Full Name is: " + o.FullName
    
  MsgBox "Length of Full Name is: " + CStr(o.FullNameLength)
    
  o.FullName = "Scott Hanselman"
    
  MsgBox "First Name is: " + o.FirstName
    
  MsgBox "Last Name is: " + o.LastName
    
  o.LastName = "Evjen"
    
  MsgBox "Full Name is: " + o.FullName
    
  Set o = Nothing

End Sub
