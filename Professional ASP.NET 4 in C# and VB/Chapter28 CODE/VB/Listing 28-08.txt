Public Class CustomException
  Inherits Exception

  Sub New(ByVal Message As String)
    MyBase.New(Message)
  End Sub
End Class

Public Class DivideFunction

  Public Function Divide(ByVal Numerator As Double, _
                         ByVal Denominator As Double) As Double

    If ((Numerator > 1000) Or (Denominator > 1000)) Then
      Throw New CustomException("Numerator and denominator both " +
                          "have to be less than or equal to 1000.")
    End If

    Divide = Numerator / Denominator

  End Function

End Class
