Imports Microsoft.VisualBasic

Public Class NameFunctions

    Private m_FirstName As String
    Private m_LastName As String

    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get

        Set(ByVal Value As String)
            m_FirstName = Value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_LastName
        End Get

        Set(ByVal Value As String)
            m_LastName = Value
        End Set
    End Property

    Public Property FullName() As String
        Get
            Return m_FirstName + " " + m_LastName
        End Get

        Set(ByVal Value As String)
            m_FirstName = Split(Value, " ")(0)
            m_LastName = Split(Value, " ")(1)
        End Set
    End Property

    Public ReadOnly Property FullNameLength() As Long
        Get
            FullNameLength = Len(Me.FullName)
        End Get
    End Property

End Class

