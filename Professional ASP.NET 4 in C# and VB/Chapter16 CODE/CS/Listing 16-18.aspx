<%@ Page Language="C#" %>
<%@ Register Namespace="Wrox.ConnectionManagement" TagPrefix="connectionControls" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Connecting Web Parts</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:WebPartManager ID="WebPartManager1" runat="server">
            <StaticConnections>
                <asp:WebPartConnection ID="WebPartConnection1" 
                 ConsumerID="ModifyableCalendar1" 
                 ConsumerConnectionPointID="CalendarTitleConsumer" 
                 ProviderID="TextBoxChanger1" 
                 ProviderConnectionPointID="TextBoxStringProvider">                    
                </asp:WebPartConnection>
            </StaticConnections> 
        </asp:WebPartManager>
        <table cellpadding="3">
            <tr valign="top">
                <td style="width: 100px">
                    <asp:WebPartZone ID="WebPartZone1" runat="server">
                        <ZoneTemplate>
                            <connectionControls:TextBoxChanger 
                             ID="TextBoxChanger1" 
                             runat="server" Title="Provider Web Part" />
                        </ZoneTemplate>
                    </asp:WebPartZone>
                </td>
                <td style="width: 100px">
                    <asp:WebPartZone ID="WebPartZone2" runat="server">
                        <ZoneTemplate>
                            <connectionControls:ModifyableCalendar 
                             ID="ModifyableCalendar1" runat="server" 
                             Title="Consumer Web Part" />
                        </ZoneTemplate>
                    </asp:WebPartZone>
                </td>
            </tr>
        </table>    
    </div>
    </form>
</body>
</html>
