<tr valign="top">
   <td>
      <asp:WebPartZone ID="WebPartZone2" runat="server">
         <ZoneTemplate>
            <asp:Image ID="Image1" runat="server" 
             ImageUrl="~/Images/Tuija.jpg" Width="150px" 
             Title="Tuija at the Museum">
            </asp:Image>
            <uc1:DailyLinks ID="DailyLinks1" runat="server" 
             Title="Daily Links">
            </uc1:DailyLinks>
         </ZoneTemplate>
      </asp:WebPartZone>
   </td>
   <td>
      <asp:WebPartZone ID="WebPartZone3" runat="server">
         <ZoneTemplate>
            <asp:Calendar ID="Calendar1" runat="server"
             Title="Calendar">
            </asp:Calendar>
         </ZoneTemplate>
      </asp:WebPartZone>
   </td>
   <td>
      <asp:CatalogZone ID="Catalogzone1" runat="server">
         <ZoneTemplate>
            <asp:PageCatalogPart ID="Pagecatalogpart1" runat="server" />
         </ZoneTemplate>
      </asp:CatalogZone>
   </td>
</tr>
