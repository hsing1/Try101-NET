<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:LinqDataSource ID="LinqDataSource1" runat="server"
    ContextTypeName="CS.NorthwindCSDataContext" TableName="Products"
    Select="new (key as Category, it as Products,
            Average(UnitPrice) as Average_UnitPrice)"
    GroupBy="Category">
</asp:LinqDataSource>
     <asp:GridView ID="GridView1" runat="server" DataSourceID="LinqDataSource1" />
    </div>
    </form>
</body>
</html>
