<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If (Not Page.IsPostBack) Then
            Dim config As Configuration =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath)

            Dim conn As ConnectionStringSettings =
                config.ConnectionStrings.ConnectionStrings("NewVBConnectionString1")
            
            Dim builder As New System.Data.SqlClient.SqlConnectionStringBuilder(conn.ConnectionString)

            ' Change the connection string properties
            builder.DataSource = "localhost"
            builder.InitialCatalog = "Northwind1"
            builder.UserID = "sa"
            builder.Password = "password"
            builder.PersistSecurityInfo = True

            conn.ConnectionString = builder.ConnectionString
            
            config.ConnectionStrings.ConnectionStrings.Add(conn)
            config.Save(ConfigurationSaveMode.Minimal)
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modifying the Connection String</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" Runat="server">
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
