<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If (Not Page.IsPostBack) Then
            ' Create a new ConnectionStringSettings object and populate it
            Dim conn As New ConnectionStringSettings()
            conn.ConnectionString = _
                 "Server=localhost;User ID=sa;Password=password" & _
                 "Database=Northwind;Persist Security Info=True"
            conn.Name = "AppConnectionString1"
            conn.ProviderName = "System.Data.SqlClient"

            ' Add the new connection string to the web.config
            Dim config As Configuration =
                System.Web.Configuration.
                WebConfigurationManager.OpenWebConfiguration(
                    Request.ApplicationPath)
            config.ConnectionStrings.ConnectionStrings.Add(conn)
            config.Save(ConfigurationSaveMode.Minimal)
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modifying the Connection String</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>