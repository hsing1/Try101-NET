<%@ Page Language="C#" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
Configuration config = System.Web.Configuration.
    WebConfigurationManager.OpenWebConfiguration(
    Request.ApplicationPath);
ConnectionStringSettings conn = config.ConnectionStrings.
    ConnectionStrings["NewCSConnectionString1"];

System.Data.SqlClient.SqlConnectionStringBuilder builder =
    new System.Data.SqlClient.SqlConnectionStringBuilder(
            conn.ConnectionString
        );

// Change the connection string properties
builder.DataSource = "localhost";
builder.InitialCatalog = "Northwind1";
builder.UserID = "sa";
builder.Password = "password";
builder.PersistSecurityInfo = true;

conn.ConnectionString = builder.ConnectionString;

config.ConnectionStrings.ConnectionStrings.Add(conn);
config.Save(ConfigurationSaveMode.Minimal);
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Modifying the Connection String</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:SqlDataSource ID="SqlDataSource1" Runat="server">
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>