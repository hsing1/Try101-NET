<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:LinqDataSource ID="LinqDataSource1" runat="server"
     ContextTypeName="CS.LINQ2SQL.NorthwindCSDataContext" TableName="Customers"
     Select="new (CustomerID, ContactName, ContactTitle, Region)"
     Where="Country == @Country"
        OrderBy="Region, ContactTitle, ContactName">
    <WhereParameters>
        <asp:querystringparameter DefaultValue="0" Name="CustomerID"
            QueryStringField="ID" Type="String" />
        <asp:querystringparameter DefaultValue="USA" Name="Country"
            QueryStringField="country" Type="String" />
        <asp:FormParameter DefaultValue="AZ" Name="Region"
            FormField="region" Type="String" />
     </WhereParameters>
</asp:LinqDataSource>
     <asp:GridView ID="GridView1" runat="server" DataSourceID="LinqDataSource1" />
    </div>
    </form>
</body>
</html>
