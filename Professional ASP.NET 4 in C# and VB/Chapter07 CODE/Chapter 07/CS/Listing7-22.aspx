<%@ Page Language="C#" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Create a new ConnectionStringSettings object and populate it
ConnectionStringSettings conn = new ConnectionStringSettings();
conn.ConnectionString =
     "Server=localhost;User ID=sa;Password=password; " +
     "Database=Northwind;Persist Security Info=True";
conn.Name = "NewCSConnectionString1";
conn.ProviderName = "System.Data.SqlClient";

// Add the new connection string to the web.config
Configuration config = 
    System.Web.Configuration.
    WebConfigurationManager.OpenWebConfiguration(
        Request.ApplicationPath);
config.ConnectionStrings.ConnectionStrings.Add(conn);
config.Save(ConfigurationSaveMode.Minimal);
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Modifying the Connection String</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
