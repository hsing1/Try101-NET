﻿<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:LinqDataSource ID="LinqDataSource1" runat="server"
    ContextTypeName="CS.NorthwindCSDataContext" TableName="Customers"
    EnableInsert="True" EnableUpdate="True" EnableDelete="True" 
    EntityTypeName="" />
        <asp:GridView ID="GridView1" runat="server" DataSourceID="LinqDataSource1" 
            AutoGenerateColumns="True" DataKeyNames="CustomerID" >           
        </asp:GridView>
    </div>
    <asp:QueryExtender ID="QueryExtender1" runat="server" TargetControlID="LinqDataSource1">
        <asp:SearchExpression SearchType="StartsWith" DataFields="CustomerID">
            <asp:QueryStringParameter DefaultValue="A" QueryStringField="s" />
        </asp:SearchExpression>
    </asp:QueryExtender>
    </form>
</body>
</html>
