<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:LinqDataSource ID="LinqDataSource1" runat="server"
     ContextTypeName="CS.LINQ2SQL.NorthwindCSDataContext" TableName="Customers"
    Select="new (CustomerID, ContactName, ContactTitle, Region)"
    Where="CustomerID == @CustomerID">
    <whereparameters>
        <asp:querystringparameter DefaultValue="0" Name="CustomerID"
            QueryStringField="ID" Type="String" />
    </whereparameters>
</asp:LinqDataSource>
     <asp:GridView ID="GridView1" runat="server" DataSourceID="LinqDataSource1" />
    </div>
    </form>
</body>
</html>

