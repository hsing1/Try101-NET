using System;
using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;

namespace Wrox.CS
{
[ExpressionPrefix("MySecondCustomExpression")]
[ExpressionEditor("MySecondCustomExpressionEditor")]
public class MySecondCustomExpression : ExpressionBuilder
{
    public override System.CodeDom.CodeExpression
        GetCodeExpression(BoundPropertyEntry entry, object parsedData,
            ExpressionBuilderContext context)
    {
        return new CodeCastExpression("Int64",
            new CodePrimitiveExpression(parsedData));
    }
    public override object ParseExpression
        (string expression, Type propertyType, 
         ExpressionBuilderContext context)
    {
        return expression;
    }
}
}
