using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int bookcount = 0;
        XmlReaderSettings settings = new XmlReaderSettings();

        string booksSchemaFile = Server.MapPath("books.xsd");

        settings.Schemas.Add(null, XmlReader.Create(booksSchemaFile));
        settings.ValidationType = ValidationType.Schema;
        settings.ValidationFlags =

        XmlSchemaValidationFlags.ReportValidationWarnings;
        settings.ValidationEventHandler +=
                new ValidationEventHandler(settings_ValidationEventHandler);

        settings.IgnoreWhitespace = true;
        settings.IgnoreComments = true;

        string booksFile = Server.MapPath("books.xml");
        using (XmlReader reader = XmlReader.Create(booksFile, settings))
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element &&
                        "book" == reader.LocalName)
                {
                    bookcount++;
                }
            }
        }
        Response.Write(String.Format("Found {0} books!", bookcount));
    }

    void settings_ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
    {
        Response.Write(e.Message);
    }

}
