using System;
using System.Xml;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Double price = 49.99;
        DateTime publicationdate = new DateTime(2005, 1, 1);
        String isbn = "1-057-610-0";
        Author a = new Author();
        a.FirstName = "Scott";
        a.LastName = "Hanselman";

        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.NewLineOnAttributes = true;

        Response.ContentType = "text/xml";

        XmlSerializerFactory factory = new XmlSerializerFactory();

        using (XmlWriter writer =
                XmlWriter.Create(Response.OutputStream, settings))
        {
            //Note the artificial, but useful, indenting
            writer.WriteStartDocument();
            writer.WriteStartElement("bookstore");
            writer.WriteStartElement("book");
            writer.WriteStartAttribute("publicationdate");
            writer.WriteValue(publicationdate);
            writer.WriteEndAttribute();
            writer.WriteStartAttribute("ISBN");
            writer.WriteValue(isbn);
            writer.WriteEndAttribute();
            writer.WriteElementString("title", "ASP.NET 2.0");
            writer.WriteStartElement("price");
            writer.WriteValue(price);
            writer.WriteEndElement(); //price
            XmlSerializer xs = factory.CreateSerializer(typeof(Author));
            xs.Serialize(writer, a);
            writer.WriteEndElement(); //book
            writer.WriteEndElement(); //bookstore
            writer.WriteEndDocument();
        }

    }
}
