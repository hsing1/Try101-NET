using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Create factory early
        XmlSerializerFactory factory = new XmlSerializerFactory();
        
        XmlReaderSettings settings = new XmlReaderSettings();

        NameTable nt = new NameTable();
        object book = nt.Add("book");
        object price = nt.Add("price");
        object author = nt.Add("author");
        settings.NameTable = nt;

        string booksSchemaFile = Path.Combine(Request.PhysicalApplicationPath, "books.xsd");

        settings.Schemas.Add(null, XmlReader.Create(booksSchemaFile));
        settings.ValidationType = ValidationType.Schema;
        settings.ValidationFlags =

        XmlSchemaValidationFlags.ReportValidationWarnings;
        settings.ValidationEventHandler +=
                new ValidationEventHandler(settings_ValidationEventHandler);

        settings.IgnoreWhitespace = true;
        settings.IgnoreComments = true;

        string booksFile = Path.Combine(Request.PhysicalApplicationPath, "books.xml");
        using (XmlReader reader = XmlReader.Create(booksFile, settings))
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element &&
                   author.Equals(reader.LocalName))
                {
                    //Then use the factory to create and cache serializers
                    XmlSerializer xs = factory.CreateSerializer(typeof(Author));
                    Author a = (Author)xs.Deserialize(reader.ReadSubtree());
                    Response.Write(String.Format("Author: {1}, {0}<BR/>",
                        a.FirstName, a.LastName));
                }
            }
        }
    }

    void settings_ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
    {
        Response.Write(e.Message);
    }

}
