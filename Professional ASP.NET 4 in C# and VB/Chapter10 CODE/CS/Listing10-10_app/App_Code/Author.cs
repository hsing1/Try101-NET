using System.Xml.Serialization;

[XmlRoot(ElementName = "author", Namespace = "http://example.books.com")]
public class Author
{
    [XmlElement(ElementName = "first-name")]
    public string FirstName;

    [XmlElement(ElementName = "last-name")]
    public string LastName;
}

