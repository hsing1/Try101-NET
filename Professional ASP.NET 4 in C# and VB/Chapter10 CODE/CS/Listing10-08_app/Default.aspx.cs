using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int bookcount = 0;
        decimal booktotal = 0; 
        XmlReaderSettings settings = new XmlReaderSettings();

        NameTable nt = new NameTable();
        object book = nt.Add("book");
        object price = nt.Add("price"); 
        settings.NameTable = nt;

        string booksSchemaFile = Path.Combine(Request.PhysicalApplicationPath, "books.xsd");

        settings.Schemas.Add(null, XmlReader.Create(booksSchemaFile));
        settings.ValidationType = ValidationType.Schema;
        settings.ValidationFlags =

        XmlSchemaValidationFlags.ReportValidationWarnings;
        settings.ValidationEventHandler +=
                new ValidationEventHandler(settings_ValidationEventHandler);

        settings.IgnoreWhitespace = true;
        settings.IgnoreComments = true;

        string booksFile = Path.Combine(Request.PhysicalApplicationPath, "books.xml");
        using (XmlReader reader = XmlReader.Create(booksFile, settings))
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element &&
                    book.Equals(reader.LocalName)) //A subtle, but significant change!
                {
                    bookcount++;
                }
                if (reader.NodeType == XmlNodeType.Element && price.Equals(reader.LocalName))
                {
                    booktotal +=
                        reader.ReadElementContentAsDecimal();
                }
            }
        }
        Response.Write(String.Format("Found {0} books that total {1:C}!",
            bookcount, booktotal));
    }

    void settings_ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
    {
        Response.Write(e.Message);
    }

}
