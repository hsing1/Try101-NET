using System;
using System.IO;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int bookcount = 0;
        XmlReaderSettings settings = new XmlReaderSettings();

        settings.IgnoreWhitespace = true;
        settings.IgnoreComments = true;

        string booksFile = Server.MapPath("books.xml");
        using (XmlReader reader = XmlReader.Create(booksFile, settings))
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element &&
                        "book" == reader.LocalName)
                {
                    bookcount++;
                }
            }
        }
        Response.Write(String.Format("Found {0} books!", bookcount));
    }
}
