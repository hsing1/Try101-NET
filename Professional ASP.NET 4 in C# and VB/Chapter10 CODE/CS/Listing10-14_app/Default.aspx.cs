using System;
using System.Data;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string connStr = "Data Source=.\\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;";

        using (SqlConnection conn = new SqlConnection(connStr))
        {
            SqlCommand command = new SqlCommand("select * from customers", conn);
            conn.Open();
            DataSet ds = new DataSet();
            ds.DataSetName = "Customers";
            ds.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, "Customer");
            Response.ContentType = "text/xml";
            ds.WriteXml(Response.OutputStream);
        }
    }

}
