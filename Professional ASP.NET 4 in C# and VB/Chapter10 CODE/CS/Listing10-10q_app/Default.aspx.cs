using System;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Xml.Schema;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XDocument booksXML = XDocument.Load(Server.MapPath("books.xml"));
        XNamespace ns = "http://example.books.com";

        var authors = from book in booksXML.Descendants(ns + "author")
                    select new Author
                    {
                        FirstName = book.Element(ns + "first-name").Value,
                        LastName = book.Element(ns + "last-name").Value
                    };
        foreach (Author a in authors)
        {
            Response.Write(String.Format("Author: {1}, {0}<BR/>", a.FirstName, a.LastName));
        }
    }
}