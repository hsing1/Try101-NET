using System;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Xml.Schema;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string booksSchemaFile = Server.MapPath("books.xsd");
        string booksFile = Server.MapPath("books.xml");

        XmlSchemaSet schemas = new XmlSchemaSet();
        schemas.Add(null, XmlReader.Create(booksSchemaFile));
        XDocument booksXML = XDocument.Load(booksFile);
        booksXML.Validate(schemas, (senderParam, eParam) =>
                                   {
                                     Response.Write(eParam.Message);
                                   }, true);

        XNamespace ns = "http://example.books.com";
        var books = from book in booksXML.Descendants(ns + "book")
                    select book.Element(ns + "title").Value;
        Response.Write(String.Format("Found {0} books!", books.Count()));

    }
}


