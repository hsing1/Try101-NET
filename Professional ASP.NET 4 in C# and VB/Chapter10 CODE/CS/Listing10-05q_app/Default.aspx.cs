using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XDocument booksXML = XDocument.Load(Server.MapPath("books.xml"));

        var books = from book in booksXML.Descendants("{http://example.books.com}book")
                    select book.Element("{http://example.books.com}title").Value;
        Response.Write(String.Format("Found {0} books!", books.Count()));
    }
}

