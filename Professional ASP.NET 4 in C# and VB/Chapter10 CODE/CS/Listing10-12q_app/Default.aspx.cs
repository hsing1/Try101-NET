using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load document
        string booksFile = Server.MapPath("books.xml");
        XDocument document = XDocument.Load(booksFile);

        //Add a namespace prefix that can be used in the XPath expression. 
        // Note the need for a NameTable. It could be new or come from elsewhere.
        XmlNamespaceManager namespaceMgr = new XmlNamespaceManager(new NameTable());
        namespaceMgr.AddNamespace("b", "http://example.books.com");

        var nodes = document.XPathSelectElements("//b:book[not(b:price[. > 10.00])]/b:price",namespaceMgr);

        //All books whose price is not greater than 10.00
        foreach (var node in nodes)
        {
            Response.Write(node.Value + "<BR/>");
        }
    }
}
