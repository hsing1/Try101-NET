<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <xsl:element name="Authors">
            <xsl:apply-templates select="//book"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="book">
        <xsl:element name="Author">
            <xsl:value-of select="author/first-name"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="author/last-name"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
