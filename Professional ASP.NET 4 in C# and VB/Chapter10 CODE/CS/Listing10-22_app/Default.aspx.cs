using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string connStr = "Data Source=.\\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;";
        XmlDocument x = new XmlDocument();
        XPathNavigator xpathnav = x.CreateNavigator();
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            conn.Open();
            SqlCommand command = new SqlCommand(
                "select * from Customers as Customer for XML AUTO, ELEMENTS", conn);
            using (XmlWriter xw = xpathnav.PrependChild())
            {
                xw.WriteStartElement("Customers");
                using (XmlReader xr = command.ExecuteXmlReader())
                {
                    xw.WriteNode(xr, true);
                }
                xw.WriteEndElement();
            }
        }
        //Response.ContentType = "text/xml";
        //x.Save(Response.Output);
        Xml1.XPathNavigator = xpathnav;

    }

}
