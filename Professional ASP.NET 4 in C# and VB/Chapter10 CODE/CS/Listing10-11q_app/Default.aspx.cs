using System;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;


public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Double price = 49.99;
        DateTime publicationdate = new DateTime(2005, 1, 1);
        String isbn = "1-057-610-0";
        Author a = new Author();
        a.FirstName = "Scott";
        a.LastName = "Hanselman";

        Response.ContentType = "text/xml";
        XNamespace ns = "http://example.books.com";
        XDocument books = new XDocument(
            new XElement(ns + "bookstore",
                new XElement(ns + "book",
                    new XAttribute("publicationdate", publicationdate),
                    new XAttribute("ISBN", isbn),
                    new XElement(ns + "title", "ASP.NET 2.0 Book"),
                    new XElement(ns + "price", price),
                    new XElement(ns + "author",
                        new XElement(ns + "first-name", a.FirstName),
                        new XElement(ns + "last-name", a.LastName)
                        )
                    )
               )
            );

        //XmlSerializer xs = new XmlSerializer(typeof(Author));
        //XDocument books = new XDocument(
        //    new XElement(ns + "bookstore",
        //        new XElement(ns + "book",
        //            new XAttribute("publicationdate", publicationdate),
        //            new XAttribute("ISBN", isbn),
        //            new XElement(ns + "title", "ASP.NET 2.0 Book"),
        //            new XElement(ns + "price", price),
        //            xs.SerializeAsXElement(a)
        //            )
        //       )
        //    );

        Response.Write(books);
    }
}

static class XmlSerializerExtension
{
    public static XElement SerializeAsXElement(this XmlSerializer xs, object o)
    {
        XDocument d = new XDocument();
        using (XmlWriter w = d.CreateWriter())
        {
            xs.Serialize(w, o);
        }
        XElement e = d.Root;
        e.Remove();
        return e;
    }
}
