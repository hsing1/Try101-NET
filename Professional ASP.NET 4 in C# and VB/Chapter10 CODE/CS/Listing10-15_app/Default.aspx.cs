using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string connStr = "Data Source=.\\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;";

        using (SqlConnection conn = new SqlConnection(connStr))
        {
            SqlCommand command = new SqlCommand("select * from customers", conn);
            conn.Open();
            DataSet ds = new DataSet();
            ds.DataSetName = "Customers";
            ds.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, "Customer");
            //Response.ContentType = "text/xml";
            //ds.WriteXml(Response.OutputStream);

            //Added in Listing 13-15
            XmlDataDocument doc = new XmlDataDocument(ds);
            doc.DataSet.EnforceConstraints = false;
            XmlNode node = doc.SelectSingleNode(@"//Customer[CustomerID = 'ANATR']/ContactTitle");
            node.InnerText = "Boss";
            doc.DataSet.EnforceConstraints = true;

            DataRow dr = doc.GetRowFromElement((XmlElement)node.ParentNode);
            Response.Write(dr["ContactName"].ToString() + " is the ");
            Response.Write(dr["ContactTitle"].ToString());
        }
    }

}
