using System;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.XPath;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string connStr = "Data Source=.\\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;";
        XmlDocument x = new XmlDocument();
        XPathNavigator xpathnav = x.CreateNavigator();
        using (SqlConnection conn = new SqlConnection(connStr))
        {
            conn.Open();
            SqlCommand command = new SqlCommand(
                "select * from Customers as Customer for XML AUTO, ELEMENTS", conn);
            using (XmlWriter xw = xpathnav.PrependChild())
            {
                xw.WriteStartElement("Customers");
                using (XmlReader xr = command.ExecuteXmlReader())
                {
                    xw.WriteNode(xr, true);
                }
                xw.WriteEndElement();
            }
        }
        Response.ContentType = "text/xml";
        x.Save(Response.Output);

    }

}
