Imports Microsoft.VisualBasic
Imports System.Xml.Serialization

<XmlRoot(ElementName:="author", _
Namespace:="http://example.books.com")> Public Class Author
    <XmlElement(ElementName:="first-name")> Public FirstName As String
    <XmlElement(ElementName:="last-name")> Public LastName As String
End Class
