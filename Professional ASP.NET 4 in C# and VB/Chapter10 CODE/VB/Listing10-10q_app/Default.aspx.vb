Imports System
Imports System.Xml
Imports System.Linq
Imports System.Xml.Linq

Imports <xmlns:books="http://example.books.com">

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load

        Dim booksXML = XDocument.Load(Server.MapPath("books.xml"))

        Dim authors = From book In booksXML...<books:book> Select New Author _
                With {.FirstName = book.<books:author>.<books:first-name>.Value, _
                      .LastName = book.<books:author>.<books:last-name>.Value}

        For Each a As Author In authors
            Response.Write(String.Format("Author: {1}, {0}<BR/>", a.FirstName, a.LastName))
        Next
    End Sub
End Class