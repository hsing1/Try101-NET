Imports System.Xml
Imports System.Xml.Linq

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim price As Double = 49.99
        Dim publicationdate As New DateTime(2005, 1, 1)
        Dim isbn As String = "1-057-610-0"
        Dim a As New Author()
        a.FirstName = "Scott"
        a.LastName = "Hanselman"

        Response.ContentType = "text/xml"

        Dim books = <bookstore xmlns="http://examples.books.com">
                        <book publicationdate=<%= publicationdate %> ISBN=<%= isbn %>>
                            <title>ASP.NET 2.0</title>
                            <price><%= price %></price>
                            <author>
                                <first-name><%= a.FirstName %></first-name>
                                <last-name><%= a.LastName %></last-name>
                            </author>
                        </book>
                    </bookstore>

        Response.Write(books)
    End Sub
End Class
