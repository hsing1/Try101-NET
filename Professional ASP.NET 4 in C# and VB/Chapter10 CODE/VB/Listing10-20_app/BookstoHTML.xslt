<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
        xmlns:b="http://example.books.com" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <h3>List of Authors</h3>
        <table border="1">
            <tr>
                <th>First</th><th>Last</th>
            </tr>
            <xsl:apply-templates select="//b:book"/>
        </table>
    </xsl:template>
    <xsl:template match="b:book">
        <tr>
            <td><xsl:value-of select="b:author/b:first-name"/></td>
            <td><xsl:value-of select="b:author/b:last-name"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
