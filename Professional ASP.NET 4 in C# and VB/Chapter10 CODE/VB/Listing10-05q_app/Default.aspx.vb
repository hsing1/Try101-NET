Imports System.IO
Imports System.Xml
Imports System.Linq
Imports System.Xml.Linq
Imports <xmlns:b="http://example.books.com">

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load

        Dim booksXML = XDocument.Load(Server.MapPath("books.xml"))

        Dim books = From book In booksXML...<b:book> Select book.<b:title>.Value

        Response.Write(String.Format("Found {0} books!", books.Count()))
    End Sub
End Class
