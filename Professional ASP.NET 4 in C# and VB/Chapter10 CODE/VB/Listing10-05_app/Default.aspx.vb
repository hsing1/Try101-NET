Imports System.IO
Imports System.Xml

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Dim bookcount As Integer = 0
        Dim settings As New XmlReaderSettings()

        settings.IgnoreWhitespace = True
        settings.IgnoreComments = True

        Dim booksFile As String = Server.MapPath("books.xml")
        Using reader As XmlReader = XmlReader.Create(booksFile, settings)
            While (reader.Read())
                If (reader.NodeType = XmlNodeType.Element _
                        And "book" = reader.LocalName) Then
                    bookcount += 1
                End If
            End While
        End Using
        Response.Write(String.Format("Found {0} books!", bookcount))
    End Sub
End Class
