<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>XmlDataSource</title>
    </head>
    <body>
    <form id="form1" runat="server">
        <asp:datalist id="DataList1" DataSourceID="XmlDataSource1" runat="server">
            <ItemTemplate>
                <p><b><%# XPath("author/first-name") %> 
                    <%# XPath("author/last-name")%></b>
                    wrote <%# XPath("title") %></p>
            </ItemTemplate>
        </asp:datalist>
        <asp:xmldatasource id="XmlDataSource1" runat="server"
            datafile="~/Books.xml" 
            xpath="//bookstore/book"/>
    </form>
    </body>
</html>

