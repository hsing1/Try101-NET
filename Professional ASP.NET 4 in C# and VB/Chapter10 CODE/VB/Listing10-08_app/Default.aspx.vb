Imports System.IO
Imports System.Xml
Imports System.Xml.Schema

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Dim bookcount As Integer = 0
        Dim booktotal As Decimal = 0
        Dim settings As New XmlReaderSettings()

        Dim nt As New NameTable()
        Dim book As Object = nt.Add("book")
        Dim price As Object = nt.Add("price")
        settings.NameTable = nt

        Dim booksSchemaFile As String = Path.Combine(Request.PhysicalApplicationPath, "books.xsd")

        settings.Schemas.Add(Nothing, XmlReader.Create(booksSchemaFile))
        settings.ValidationType = ValidationType.Schema
        settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings

        AddHandler settings.ValidationEventHandler, _
        AddressOf settings_ValidationEventHandler

        settings.IgnoreWhitespace = True
        settings.IgnoreComments = True

        Dim booksFile As String = _
            Path.Combine(Request.PhysicalApplicationPath, "books.xml")
        Using reader As XmlReader = XmlReader.Create(booksFile, settings)
            While (reader.Read())
                If (reader.NodeType = XmlNodeType.Element _
                    And book.Equals(reader.LocalName)) Then
                    'A subtle, but significant change!
                    bookcount += 1
                End If
                If (reader.NodeType = XmlNodeType.Element And price.Equals(reader.LocalName)) Then
                    booktotal += reader.ReadElementContentAsDecimal()
                End If

            End While
        End Using
        Response.Write(String.Format("Found {0} books that total {1:C}!", _
                bookcount, booktotal))
    End Sub

    Sub settings_ValidationEventHandler(ByVal sender As Object, _
            ByVal e As System.Xml.Schema.ValidationEventArgs)
        Response.Write(e.Message)
    End Sub

End Class
