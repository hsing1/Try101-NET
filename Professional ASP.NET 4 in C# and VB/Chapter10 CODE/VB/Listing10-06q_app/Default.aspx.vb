Imports System
Imports System.Xml
Imports System.Linq
Imports System.Xml.Linq
Imports System.Xml.Schema

Imports <xmlns:b="http://example.books.com">

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Dim schemas = New XmlSchemaSet()
        schemas.Add(Nothing, XmlReader.Create(Server.MapPath("books.xsd")))
        Dim booksXML = XDocument.Load(Server.MapPath("books.xml"))
        booksXML.Validate(schemas, AddressOf ValidationEventHandler, True)

        Dim books = From book In booksXML...<b:book> _
                    Select book.<b:title>.Value

        Response.Write(String.Format("Found {0} books!", books.Count()))
    End Sub

    Sub ValidationEventHandler(ByVal sender As Object, _
            ByVal e As System.Xml.Schema.ValidationEventArgs)
        Response.Write(e.Message)
    End Sub
End Class