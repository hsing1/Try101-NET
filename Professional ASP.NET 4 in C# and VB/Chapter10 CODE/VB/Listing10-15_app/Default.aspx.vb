Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Dim connStr As String = "Data Source=.\\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;"

        Using conn As New SqlConnection(connStr)
            Dim command As New SqlCommand("select * from customers", conn)
            conn.Open()
            Dim ds As New DataSet()
            ds.DataSetName = "Customers"
            ds.Load(command.ExecuteReader(), LoadOption.OverwriteChanges, "Customer")
            'Response.ContentType = "text/xml"
            'ds.WriteXml(Response.OutputStream)

            'Added in Listing 13-15
            Dim doc As New XmlDataDocument(ds)
            doc.DataSet.EnforceConstraints = False
            Dim node As XmlNode = _
            doc.SelectSingleNode("//Customer[CustomerID = 'ANATR']/ContactTitle")
            node.InnerText = "Boss"
            doc.DataSet.EnforceConstraints = True

            Dim dr As DataRow = doc.GetRowFromElement(CType(node.ParentNode, XmlElement))
            Response.Write(dr("ContactName").ToString() & " is the ")
            Response.Write(dr("ContactTitle").ToString())
        End Using


    End Sub

End Class
