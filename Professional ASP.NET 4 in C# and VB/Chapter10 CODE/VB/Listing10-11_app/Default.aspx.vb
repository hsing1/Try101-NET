Imports System.Xml
Imports System.Xml.Serialization

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim price As Double = 49.99
        Dim publicationdate As New DateTime(2005, 1, 1)
        Dim isbn As String = "1-057-610-0"
        Dim a As New Author()
        a.FirstName = "Scott"
        a.LastName = "Hanselman"

        Dim settings As New XmlWriterSettings()
        settings.Indent = True
        settings.NewLineOnAttributes = True

        Response.ContentType = "text/xml"

        Dim factory As New XmlSerializerFactory

        Using writer As XmlWriter = XmlWriter.Create(Response.OutputStream, settings)

            'Note the artificial, but useful, indenting
            writer.WriteStartDocument()
            writer.WriteStartElement("bookstore")
            writer.WriteStartElement("book")
            writer.WriteStartAttribute("publicationdate")
            writer.WriteValue(publicationdate)
            writer.WriteEndAttribute()
            writer.WriteStartAttribute("ISBN")
            writer.WriteValue(isbn)
            writer.WriteEndAttribute()
            writer.WriteElementString("title", "ASP.NET 2.0")
            writer.WriteStartElement("price")
            writer.WriteValue(price)
            writer.WriteEndElement() 'price
            Dim xs As XmlSerializer = _
                factory.CreateSerializer(GetType(Author))
            xs.Serialize(writer, a)
            writer.WriteEndElement() 'book
            writer.WriteEndElement() 'bookstore
            writer.WriteEndDocument()
        End Using


    End Sub
End Class
