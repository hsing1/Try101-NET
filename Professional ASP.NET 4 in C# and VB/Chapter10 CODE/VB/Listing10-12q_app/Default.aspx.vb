Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Linq

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        'Load document
        Dim booksFile As String = Server.MapPath("books.xml")
        Dim document As XDocument = XDocument.Load(booksFile)

        'Add a namespace prefix that can be used in the XPath expression
        Dim namespaceMgr As New XmlNamespaceManager(New NameTable())
        namespaceMgr.AddNamespace("b", "http://example.books.com")

        'All books whose price is not greater than 10.00
        Dim nodes = document.XPathSelectElements("//b:book[not(b:price[. > 10.00])]/b:price", namespaceMgr)

        For Each node In nodes
            Response.Write(node.Value + "<BR/>")
        Next
    End Sub

End Class
