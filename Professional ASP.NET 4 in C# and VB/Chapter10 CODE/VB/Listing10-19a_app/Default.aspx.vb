Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Response.ContentType = "text/xml"

        Dim xsltFile As String = Server.MapPath("books.xslt")
        Dim xmlFile As String = Server.MapPath("books.xml")

        Dim xslt As New XslCompiledTransform(True) 'Pass in true to enable XSLT Debugging
        xslt.Load(GetType(Wrox.Book.MyCompiledStyleSheet))

        Dim doc As New XPathDocument(xmlFile)
        xslt.Transform(doc, New XmlTextWriter(Response.Output))

    End Sub

End Class
