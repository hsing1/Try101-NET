<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <h3>List of Customers</h3>
        <table border="1">
            <tr>
                <th>Company Name</th><th>Contact Name</th><th>Contact Title</th>
            </tr>
            <xsl:apply-templates select="//Customer"/>
        </table>
    </xsl:template>
    <xsl:template match="Customer">
        <tr>
            <td><xsl:value-of select="CompanyName"/></td>
            <td><xsl:value-of select="ContactName"/></td>
            <td><xsl:value-of select="ContactTitle"/></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
