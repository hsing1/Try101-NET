Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        Dim connStr As String = "Data Source=.\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=SSPI;"
        Dim x As New XmlDocument()
        Dim xpathnav As XPathNavigator = x.CreateNavigator()
        Using conn As New SqlConnection(connStr)
            conn.Open()
            Dim command As New SqlCommand("select * from Customers as Customer " & _
                "for XML AUTO, ELEMENTS", conn)
            Using xw As XmlWriter = xpathnav.PrependChild()
                xw.WriteStartElement("Customers")
                Using xr As XmlReader = command.ExecuteXmlReader()
                    xw.WriteNode(xr, True)
                End Using
                xw.WriteEndElement()
            End Using
        End Using
        Response.ContentType = "text/xml"
        x.Save(Response.Output)

    End Sub

End Class
