Imports System.IO
Imports System.Xml
Imports System.Xml.XPath

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
           Handles Me.Load
        'Load document
        Dim booksFile As String = Server.MapPath("books.xml")

        Dim document As New XPathDocument(booksFile)
        Dim nav As XPathNavigator = document.CreateNavigator()

        'Add a namespace prefix that can be used in the XPath expression
        Dim namespaceMgr As New XmlNamespaceManager(nav.NameTable)
        namespaceMgr.AddNamespace("b", "http://example.books.com")

        'All books whose price is not greater than 10.00
        For Each node As XPathNavigator In nav.Select( _
                "//b:book[not(b:price[. > 10.00])]/b:price", namespaceMgr)
            Dim price As Decimal = _
                CType(node.ValueAs(GetType(Decimal)), Decimal)
            Response.Write(String.Format("Price is {0}<BR/>", _
                price))
        Next


    End Sub

End Class
