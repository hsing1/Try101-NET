use Northwind;
WITH XMLNAMESPACES (
	 DEFAULT 'urn:hanselman.com/northwind'
	 , 'urn:hanselman.com/northwind/address' as "addr"
)
SELECT CustomerID as "@ID", 
           CompanyName,
           Address as "addr:Address/addr:Street",
           City as "addr:Address/addr:City",
           Region as "addr:Address/addr:Region",
           PostalCode as "addr:Address/addr:Zip",
           Country as "addr:Address/addr:Country",
           ContactName as "Contact/Name",
           ContactTitle as "Contact/Title",
           Phone as "Contact/Phone", 
           Fax as "Contact/Fax"
FROM Customers
FOR XML PATH('Customer'), ROOT('Customers'), ELEMENTS XSINIL
