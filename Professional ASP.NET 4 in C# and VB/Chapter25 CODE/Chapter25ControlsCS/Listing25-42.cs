﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{

    [Designer(typeof(MultiRegionControlDesigner))]
    [ToolboxData("<{0}:MultiRegionControl runat=\"server\" width=\"100%\">" +
        "</{0}:MultiRegionControl>")]
    public class MultiRegionControl : CompositeControl
    {
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(null)]
        public virtual ITemplate View1 {get;set;}

        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(null)]
        public virtual ITemplate View2 { get; set; }

        private int _currentView = 0;
        public int CurrentView
        {
            get { return _currentView; }
            set { _currentView = value; }
        }

        protected override void CreateChildControls()
        {
            Controls.Clear();

            ITemplate template = View1;
            if (_currentView == 1)
                template = View2;

            Panel p = new Panel();
            Controls.Add(p);

            if (template != null)
                template.InstantiateIn(p);
        }
    }

}
