﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
[DefaultProperty("Text")]
[ToolboxData("<{0}:ServerControl29 runat=server></{0}:ServerControl29>")]
public class ServerControl29 : WebControl
{
    string state;
    protected override void OnInit(EventArgs e)
    {
        Page.RegisterRequiresControlState(this);
        
        base.OnInit(e);
    }

    protected override void LoadControlState(object savedState)
    {
        state = (string)savedState;
    }

    protected override object SaveControlState()
    {
        return (object)"ControlSpecificData";
    }

    protected override void RenderContents(HtmlTextWriter output)
    {
        output.Write("Control State: " + state);
    }
}
}
