﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ServerControl39 runat=server></{0}:ServerControl39>")]
    public class ServerControl39 : WebControl
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [TypeConverter(typeof(GuidConverter))]
        public Guid BookId {get;set;}

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(BookId.ToString());
        }
    }
}
