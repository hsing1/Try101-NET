﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
    internal sealed class DefaultMessageTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            Literal l = new Literal();
            l.Text = "No MessageTemplate was included.";
            container.Controls.Add(l);
        }
    }
}
