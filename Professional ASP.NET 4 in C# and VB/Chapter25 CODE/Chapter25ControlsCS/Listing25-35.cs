﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace ServerControl.CS
{
    public class Message : Panel, INamingContainer
    {
        public string Name { get; internal set; }
        public string Text { get; internal set; }
    }
}
