﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ServerControl31 runat=server></{0}:ServerControl31>")]
    public class ServerControl31 : WebControl, IPostBackEventHandler
    {
        protected override void RenderContents(HtmlTextWriter output)
        {
            PostBackOptions p = new PostBackOptions(this);

            output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                Page.ClientScript.GetPostBackEventReference(p));
            output.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
            output.AddAttribute(HtmlTextWriterAttribute.Name, this.ClientID);
            output.RenderBeginTag(HtmlTextWriterTag.Button);
            output.Write("My Button");
            output.RenderEndTag();
        }

        #region IPostBackEventHandler Members
        public event EventHandler Click;

        public virtual void OnClick(EventArgs e)
        {
            if (Click != null)
            {
                Click(this, e);
            }
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            OnClick(EventArgs.Empty);
        }

        #endregion
    }
}
