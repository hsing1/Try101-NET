﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:TemplatedControl runat=server></{0}:TemplatedControl>")]
    public class TemplatedControl : WebControl
    {
        [Browsable(false)]
        public Message TemplateMessage {get;internal set;}

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(Message))]
        public virtual ITemplate MessageTemplate {get;set;}

        [Bindable(true)]
        [DefaultValue("")]
        public string Name {get;set;}

        [Bindable(true)]
        [DefaultValue("")]
        public string Text { get; set; }

        public override void DataBind()
        {
            EnsureChildControls();
            ChildControlsCreated = true;

            base.DataBind();
        }

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            this.TemplateMessage = new Message() { Name=Name, Text=Text};

            if (this.MessageTemplate == null)
            {
                this.MessageTemplate = new DefaultMessageTemplate();
            }

            this.MessageTemplate.InstantiateIn(this.TemplateMessage);
            Controls.Add(this.TemplateMessage);
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            EnsureChildControls();
            ChildControlsCreated = true;

            base.RenderContents(writer);
        }

    }
}
