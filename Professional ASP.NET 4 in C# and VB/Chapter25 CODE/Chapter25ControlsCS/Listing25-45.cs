﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServerControl.CS
{
    
    [ToolboxData("<{0}:ServerControl45 runat=server></{0}:ServerControl45>")]
    public class ServerControl45 : WebControl
    { 
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Editor(typeof(System.Web.UI.Design.UrlEditor),
            typeof(System.Drawing.Design.UITypeEditor))]
        public string Url { get; set; }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(this.Url);
        }
    }
}
