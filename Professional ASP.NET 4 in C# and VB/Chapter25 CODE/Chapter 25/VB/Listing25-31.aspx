﻿<%@ Page Language="VB" %>

<%@ Register Assembly="Chapter25ControlsVB, Version=1.0.3526.27784, Culture=neutral, PublicKeyToken=null"
    Namespace="Chapter25ControlsVB.ServerControl.VB" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub ServerControl311_Click()
        Me.Label1.Text = "Button Clicked!"
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Adding a Custom Web Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <cc1:ServerControl31 runat="server" ID="ServerControl311" OnClick="ServerControl311_Click">
        </cc1:ServerControl31>
        <asp:Label runat="server" ID="Label1" Text="Hello World!" />
    </div>
    </form>
</body>
</html>