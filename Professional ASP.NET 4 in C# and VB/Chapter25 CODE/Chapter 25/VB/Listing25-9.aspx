﻿<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'Make sure the control has not already been added to the page
        If IsNothing(Session("WebUserControlAdded")) Or
            Not (CBool(Session("WebUserControlAdded"))) Then
        
            Dim myForm As Control = Page.FindControl("Form1")
            Dim c1 As Control = LoadControl("Listing25-9.ascx")
            myForm.Controls.Add(c1)

            Session("WebUserControlAdded") = True
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Check to see if the control should be added to the page
        If Not IsNothing(Session("WebUserControlAdded")) And
            (CBool(Session("WebUserControlAdded"))) Then
        
            Dim myForm As Control = Page.FindControl("Form1")
            Dim c1 As Control = LoadControl("Listing25-9.ascx")
            myForm.Controls.Add(c1)
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" 
            Text="Load Control" OnClick="Button1_Click" />
    </div>
    </form>
</body>
</html>
