﻿<%@ Page Language="VB" %>

<%@ Reference Control="Listing25-8.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myForm As Control = Page.FindControl("Form1")
        Dim c1 As Control = LoadControl("Listing25-8.ascx")
        myForm.Controls.Add(c1)

        If c1.GetType() Is GetType(Listing25_8) Then
            'This control is not participating in OutputCache
            CType(c1, Listing25_8).ID = "myListing258"
            CType(c1, Listing25_8).Text = "My users controls text"
        ElseIf c1.GetType() Is GetType(PartialCachingControl) And
            Not (IsNothing(CType(c1, PartialCachingControl).CachedControl)) Then
        
            'The control is participating in output cache, but has expired
            Dim myListing258 As Listing25_8 =
                CType(CType(c1, PartialCachingControl).CachedControl, Listing25_8)
            myListing258.ID = "myListing258"
            myListing258.Text = "My users controls text"
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
