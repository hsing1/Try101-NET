﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Make sure the control has not already been added to the page
        if ((Session["WebUserControlAdded"] == null) || 
            (!(bool)Session["WebUserControlAdded"]))
        {
            Control myForm = Page.FindControl("Form1");
            Control c1 = LoadControl("Listing25-9.ascx");
            myForm.Controls.Add(c1);

            Session["WebUserControlAdded"] = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Check to see if the control should be added to the page
        if ((Session["WebUserControlAdded"] != null) && 
            ((bool)Session["WebUserControlAdded"]))
        {
            Control myForm = Page.FindControl("Form1");
            Control c1 = LoadControl("Listing25-9.ascx");
            myForm.Controls.Add(c1);
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" 
            Text="Load Control" OnClick="Button1_Click" />
    </div>
    </form>
</body>
</html>
