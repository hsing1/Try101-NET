﻿<%@ Page Language="C#" %>

<%@ Register Assembly="Chapter25ControlsCS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
    Namespace="ServerControl.CS" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Adding a Custom Web Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <cc1:ServerControl15 runat="server" ID="ServerControl151" Text="Hello World">
        </cc1:ServerControl15>
    </div>
    </form>
</body>
</html>
