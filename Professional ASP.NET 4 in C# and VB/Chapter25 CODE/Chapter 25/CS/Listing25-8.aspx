﻿<%@ Page Language="C#" %>

<%@ Reference Control="Listing25-8.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    void Page_Load(object sender, EventArgs e)
    {
        Control myForm = Page.FindControl("Form1");
        Control c1 = LoadControl("Listing25-8.ascx");
        myForm.Controls.Add(c1);

        if (c1 is Listing25_8)
        {
            //This control is not participating in OutputCache
            ((Listing25_8)c1).ID = "myListing258";
            ((Listing25_8)c1).Text = "My users controls text";
        }
        else if ((c1 is PartialCachingControl) &&
            ((PartialCachingControl)c1).CachedControl != null)
        {
            //The control is participating in output cache, but has expired
            Listing25_8 myListing258 =
                ((Listing25_8)((PartialCachingControl)c1).CachedControl);
            myListing258.ID = "myListing258";
            myListing258.Text = "My users controls text";
        }        
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
