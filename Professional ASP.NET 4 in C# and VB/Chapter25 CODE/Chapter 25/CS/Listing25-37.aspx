﻿<%@ Page Language="C#" %>

<%@ Register Assembly="Chapter25ControlsCS, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
    Namespace="ServerControl.CS" TagPrefix="cc1" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        this.TemplatedControl1.DataBind();
    }
</script>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Templated Web Controls</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <cc1:TemplatedControl Name="John Doe" Text="Hello World!" 
            ID="TemplatedControl1" runat="server" Height="33px" Width="377px">
            <MessageTemplate>The user '<%# Container.Name %>' 
                has a message for you: <br />"<%#Container.Text%>"
            </MessageTemplate>

        </cc1:TemplatedControl>
    </div>
    </form>
</body>
</html>
