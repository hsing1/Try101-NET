﻿<%@ Control Language="C#" ClassName="Listing25_8" %>

<script runat="server">
    public string Text { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Label1.Text = this.Text;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        this.Label1.Text = "The quick brown fox clicked the button on the page";
    }
</script>

<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
<asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
