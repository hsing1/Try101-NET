﻿Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB
    <Designer(GetType(MultiRegionControlDesigner))>
    <ToolboxData("<{0}:MultiRegionControl runat=server width=100%>" &
        "</{0}:MultiRegionControl>")>
    Public Class MultiRegionControl
        Inherits CompositeControl

        ' These properties are inner properties
        <PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("")>
        Public Overridable Property View1() As ITemplate

        <PersistenceMode(PersistenceMode.InnerProperty), DefaultValue("")>
        Public Overridable Property View2() As ITemplate

        ' The current view on the control; 0= view1, 1=view2, 2=all views
        Private _currentView As Int32 = 0
        Public Property CurrentView() As Int32
            Get
                Return _currentView
            End Get
            Set(ByVal value As Int32)
                _currentView = value
            End Set
        End Property

        Protected Overrides Sub CreateChildControls()
            MyBase.CreateChildControls()

            Controls.Clear()

            Dim template As ITemplate = View1
            If (_currentView = 1) Then
                template = View2
            End If

            Dim p As New Panel()
            Controls.Add(p)

            If (Not template Is Nothing) Then
                template.InstantiateIn(p)
            End If

        End Sub

    End Class
End Namespace