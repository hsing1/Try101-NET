﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB

    <DefaultProperty("Text"),
    ToolboxData("<{0}:ServerControl33 runat=server></{0}:ServerControl33>")>
    Public Class ServerControl33
        Inherits System.Web.UI.WebControls.CompositeControl

        Protected textbox As TextBox = New TextBox()

        Protected Overrides Sub CreateChildControls()
            Me.Controls.Add(textbox)
        End Sub

    End Class
End Namespace
