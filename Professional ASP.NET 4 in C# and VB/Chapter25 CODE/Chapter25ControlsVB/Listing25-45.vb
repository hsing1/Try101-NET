﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB

    <ToolboxData("<{0}:ServerControl45 runat=server></{0}:ServerControl45>")>
    Public Class ServerControl45
        Inherits System.Web.UI.WebControls.WebControl

        <Bindable(True), Category("Appearance"), DefaultValue(""),
        Editor(
                GetType(System.Web.UI.Design.UrlEditor),
                GetType(System.Drawing.Design.UITypeEditor))>
        Public Property Url() As String

        Protected Overrides Sub RenderContents(ByVal output As HtmlTextWriter)
            output.Write(Url.ToString())
        End Sub

    End Class
End Namespace

