﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB

    <DefaultProperty("Text"),
    ToolboxData("<{0}:ServerControl29 runat=server></{0}:ServerControl29>")>
    Public Class ServerControl29
        Inherits WebControl

        Dim state As String
        Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
            Page.RegisterRequiresControlState(Me)
            MyBase.OnInit(e)
        End Sub

        Protected Overrides Sub LoadControlState(ByVal savedState As Object)
            state = CStr(savedState)
        End Sub

        Protected Overrides Function SaveControlState() As Object
            Return CType("ControlSpecificData", Object)
        End Function

        Protected Overrides Sub Render(ByVal output As System.Web.UI.HtmlTextWriter)
            output.Write("Control State: " & state)
        End Sub

    End Class
End Namespace

<DefaultProperty("Text"),
 ToolboxData("<{0}:ServerControl29 runat=server></{0}:ServerControl29>")>
Public Class ServerControl29
    Inherits WebControl

    Dim state As String
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Page.RegisterRequiresControlState(Me)
        MyBase.OnInit(e)
    End Sub

    Protected Overrides Sub LoadControlState(ByVal savedState As Object)
        state = CStr(savedState)
    End Sub

    Protected Overrides Function SaveControlState() As Object
        Return CType("ControlSpecificData", Object)
    End Function

    Protected Overrides Sub Render(ByVal output As System.Web.UI.HtmlTextWriter)
        output.Write("Control State: " & state)
    End Sub

End Class
