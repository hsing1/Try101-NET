﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB
    <DefaultProperty("Text"),
    ToolboxData("<{0}:ServerControl30 runat=server></{0}:ServerControl30>")>
    Public Class ServerControl30
        Inherits WebControl

        Protected Overrides Sub RenderContents(ByVal output As HtmlTextWriter)
            Dim p As New PostBackOptions(Me)

            output.AddAttribute(HtmlTextWriterAttribute.Onclick,
                Page.ClientScript.GetPostBackEventReference(p))

            output.AddAttribute(HtmlTextWriterAttribute.Value, "My Button")
            output.AddAttribute(HtmlTextWriterAttribute.Id, Me.ClientID & "_i")
            output.AddAttribute(HtmlTextWriterAttribute.Name, Me.ClientID & "_i")
            output.RenderBeginTag(HtmlTextWriterTag.Button)
            output.Write("My Button")
            output.RenderEndTag()
        End Sub
    End Class
End Namespace
