﻿Imports System
Imports System.ComponentModel
Imports System.Globalization
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB
    <DefaultProperty("Text"),
    ToolboxData("<{0}:ServerControl40 runat=server></{0}:ServerControl40>")>
    Public Class ServerControl40
        Inherits WebControl

        <Bindable(True), Category("Appearance"), DefaultValue(""), Localizable(True)>
        Property Name() As Name
            Get
                Dim s As Name = CType(ViewState("Name"), Name)
                If s Is Nothing Then
                    Return New Name() With {.First = "John", .Last = "Doe"}
                Else
                    Return s
                End If
            End Get

            Set(ByVal Value As Name)
                ViewState("Name") = Value
            End Set
        End Property

        Protected Overrides Sub RenderContents(ByVal output As HtmlTextWriter)
            output.Write(String.Format("{0} {1}", Name.First, Name.Last))
        End Sub

    End Class

    Public Class Name
        Public Property First() As String
        Public Property Last() As String
    End Class

    Public Class NameConverter
        Inherits TypeConverter

        Public Overrides Function CanConvertFrom(ByVal context As  _
           ITypeDescriptorContext, ByVal sourceType As Type) As Boolean

            If (sourceType Is GetType(String)) Then
                Return True
            End If

            Return MyBase.CanConvertFrom(context, sourceType)
        End Function

        Public Overrides Function ConvertFrom( _
                ByVal context As ITypeDescriptorContext, _
                ByVal culture As CultureInfo, ByVal value As Object) As Object
            If (value Is GetType(String)) Then
                Dim v As String() = (CStr(value).Split(New [Char]() {" "c}))
                Return New Name() With {.First = v(0), .Last = v(1)}
            End If
            Return MyBase.ConvertFrom(context, culture, value)
        End Function

        Public Overrides Function ConvertTo( _
                ByVal context As ITypeDescriptorContext, _
                ByVal culture As CultureInfo, ByVal value As Object, _
                ByVal destinationType As Type) As Object
            If (destinationType Is GetType(String)) Then
                Return (CType(value, Name).First + " " + (CType(value, Name).Last))
            End If
            Return MyBase.ConvertTo(context, culture, value, destinationType)
        End Function
    End Class
End Namespace