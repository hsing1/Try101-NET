﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Namespace ServerControl.VB
    <DefaultProperty("Text"),
    ToolboxData("<{0}:ServerControl39 runat=server></{0}:ServerControl39>")>
    Public Class ServerControl39
        Inherits System.Web.UI.WebControls.WebControl

        <Bindable(True)>
        <Category("Appearance")>
        <DefaultValue("")>
        <TypeConverter(GetType(GuidConverter))>
        Property BookId() As System.Guid

        Protected Overrides Sub RenderContents(ByVal output As HtmlTextWriter)
            output.Write(BookId.ToString())
        End Sub

    End Class
End Namespace

