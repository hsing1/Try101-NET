Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Write("Page created: " + DateTime.Now.ToLongTimeString())
        Dim connStr As String = ConfigurationManager.ConnectionStrings("AppConnectionString1").ConnectionString
        SqlDependency.Start(connStr)
        Dim connection As New SqlConnection(connStr)
        Dim command As New SqlCommand("Select * FROM Customers", connection)
        Dim depends As New SqlCacheDependency(command)

        connection.Open()
        GridView1.DataSource = command.ExecuteReader()
        GridView1.DataBind()

        connection.Close()

        'Now, do what you want with the sqlDependency object like:
        Response.AddCacheDependency(depends)
    End Sub
End Class

