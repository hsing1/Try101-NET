<Serializable()> _
Public Class Person
    Public firstName As String
    Public lastName As String
End Class

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) _
            Handles Me.Load

        If Not Page.IsPostBack Then
            HiddenField1.Value = "foo"
            ViewState("AnotherHiddenValue") = "bar"

            Dim p As New Person
            p.firstName = "Scott"
            p.lastName = "Hanselman"
            ViewState("HiddenPerson") = p
        End If

    End Sub

End Class
