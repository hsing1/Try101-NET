using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

[Serializable]
public class Person
{
    public string firstName;
    public string lastName;
}

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HiddenField1.Value = "foo";
            ViewState["AnotherHiddenValue"] = "bar";

            Person p = new Person();
            p.firstName = "Scott";
            p.lastName = "Hanselman";
            ViewState["HiddenPerson"] = p;
        }
    }
}
