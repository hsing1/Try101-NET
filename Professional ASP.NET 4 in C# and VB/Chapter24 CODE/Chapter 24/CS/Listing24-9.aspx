﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("TextFile.txt"));
        this.lblLocation.Text = file.FullName;
        this.lblSize.Text = file.Length.ToString();
        this.lblCreated.Text = file.CreationTime.ToString();
        this.lblModified.Text = file.LastWriteTime.ToString();
        this.lblAccessed.Text = file.LastAccessTime.ToString();
        this.lblAttributes.Text = file.Attributes.ToString();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using the Path Class</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Location: <asp:Label ID="lblLocation" runat="server" /><br />
        Size: <asp:Label ID="lblSize" runat="server" /><br />
        Created: <asp:Label ID="lblCreated" runat="server" /><br />
        Modified: <asp:Label ID="lblModified" runat="server" /><br />
        Accessed: <asp:Label ID="lblAccessed" runat="server" /><br />
        Attributes: <asp:Label ID="lblAttributes" runat="server" /><br />
    </div>
    </form>
</body>
</html>