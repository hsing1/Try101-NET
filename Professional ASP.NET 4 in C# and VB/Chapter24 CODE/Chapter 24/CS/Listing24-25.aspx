﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        using (System.IO.Pipes.NamedPipeClientStream pipeClient = 
            new System.IO.Pipes.NamedPipeClientStream(".", "testpipe", System.IO.Pipes.PipeDirection.Out))
        {
            pipeClient.Connect(10000);        
            
            try
            {
                byte[] bytes = ASCIIEncoding.Default.GetBytes("Hello World");
                pipeClient.Write(bytes, 0, bytes.Length);
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine("ERROR: {0}", ex.Message);
            }
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
