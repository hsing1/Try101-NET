﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(MapPath(""));
        foreach (System.IO.FileInfo file in dir.GetFiles("*.*"))
        {
            HtmlTableRow row = new HtmlTableRow();
            row.Cells.Add(new HtmlTableCell() 
            { InnerHtml = file.Name });
            row.Cells.Add(new HtmlTableCell()
            { InnerHtml = file.LastWriteTime.ToString() });
            row.Cells.Add(new HtmlTableCell() 
            { InnerHtml = file.Attributes.ToString() });
            this.HtmlTable1.Rows.Add(row);
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate a FileInfo</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table runat="server" id="HtmlTable1">
            <tr><td>Name</td><td>Last Write Time</td><td>Attributes</td></tr>
        </table>
    </div>
    </form>
</body>
</html>