﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        byte[] data = System.Text.Encoding.ASCII.GetBytes("This is a string");
        System.IO.MemoryStream ms = new System.IO.MemoryStream();
        ms.Write(data, 0, data.Length);
        ms.Close();
    }
</script>