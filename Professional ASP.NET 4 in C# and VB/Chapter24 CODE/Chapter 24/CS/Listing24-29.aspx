﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        Uri uri = new Uri("file://DEMO7/Documents/lorum.txt ");
        if (uri.Scheme == Uri.UriSchemeFile)
        {
            System.Net.FileWebRequest request = 
                (System.Net.FileWebRequest)System.Net.FileWebRequest.Create(uri);
            System.Net.FileWebResponse response = 
                (System.Net.FileWebResponse)request.GetResponse();
            System.IO.StreamReader reader = 
                new System.IO.StreamReader(response.GetResponseStream());
            string tmp = reader.ReadToEnd();
            response.Close();
        }
    }
</script>