﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Security.AccessControl.FileSecurity sec =
                 System.IO.File.GetAccessControl(Server.MapPath("TextFile.txt"));

        sec.AddAccessRule(
                 new System.Security.AccessControl.FileSystemAccessRule(
                          @"WIN7\TestUser",
                          System.Security.AccessControl.FileSystemRights.FullControl,
                          System.Security.AccessControl.AccessControlType.Allow
                          )
                 );

        System.IO.File.SetAccessControl(Server.MapPath("TextFile.txt"),sec);
    }
</script>