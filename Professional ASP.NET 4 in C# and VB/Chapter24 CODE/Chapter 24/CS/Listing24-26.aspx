﻿<%@ Page Language="C#" %>
<%@ Import Namespace=System.IO %>
<%@ Import Namespace=System.Net %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        Uri url = new Uri("http://www.wrox.com/");
        if (url.Scheme == Uri.UriSchemeHttp)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Get;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tmp = reader.ReadToEnd();
            response.Close();

            this.Panel1.GroupingText = tmp;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <p>This is the wrox.com website:</p>
        <asp:Panel ID="Panel1" runat="server" 
            Height="355px" Width="480px" ScrollBars=Auto>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
