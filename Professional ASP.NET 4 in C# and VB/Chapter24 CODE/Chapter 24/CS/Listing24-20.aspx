﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
// Opens a file and returns a FileStream
using (System.IO.FileStream stream1 = System.IO.File.Open(MapPath("TextFile.txt"), System.IO.FileMode.Open)) {}

// Opens a file and returns a StreamReader for reading the data
using (System.IO.StreamReader stream2 = System.IO.File.OpenText(MapPath("TextFile.txt"))) {}

// Opens a filestream for reading
using (System.IO.FileStream stream3 = System.IO.File.OpenRead(MapPath("TextFile.txt"))) {}

// Opens a filestream for writing 
using (System.IO.FileStream stream4 = System.IO.File.OpenWrite(MapPath("TextFile.txt"))) { }

// Reads the entire file and returns a string of data
string data = System.IO.File.ReadAllText(MapPath("TextFile.txt"));

// Using a LINQ query to locate specific lines within the Textfile
var lines1 = from line in System.IO.File.ReadLines(MapPath("TextFile.txt"))
            where line.Contains("laoreet")
            select line;

// Using a LINQ query to locate specific lines within the Textfile
var lines2 = from line in System.IO.File.ReadLines(MapPath("TextFile.txt"))
            where line.Contains("laoreet")
            select line;

// Appends the text data to a new text file
System.IO.File.AppendAllText(MapPath("TextFile1.txt"), data);

//Append the IEnumerable of strings to a new text file
System.IO.File.AppendAllLines(MapPath("TextFile1.txt"), lines1);

// Writes the string of data to a new file 
System.IO.File.WriteAllText(MapPath("TextFile2.txt"), data);

// Writes an IEnumerable of strings to a new text file
System.IO.File.WriteAllLines(MapPath("TextFile2.txt"), lines2);

    }
</script>