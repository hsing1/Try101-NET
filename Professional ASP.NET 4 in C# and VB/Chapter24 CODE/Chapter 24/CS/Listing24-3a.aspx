﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (System.IO.DriveInfo drive in 
                System.IO.DriveInfo.GetDrives())
            {
                TreeNode node = new TreeNode();
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.PopulateOnDemand = true;
                node.Expanded = false;
                node.Value = drive.Name;

                if (drive.IsReady)
                    node.Text =
                        String.Format("{0} - (free space: {1})",
                                drive.Name, drive.AvailableFreeSpace);
                else
                    node.Text =
                        String.Format("{0} - (not ready)", drive.Name);

                this.TreeView1.Nodes.Add(node);
            }
        }
    }

    private void LoadDirectories(TreeNode parent, string path)
    {
        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(path);

        try
        {
            foreach (System.IO.DirectoryInfo d in directory.EnumerateDirectories())
            {
                TreeNode node = new TreeNode(d.Name, d.FullName);
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.PopulateOnDemand = true;
                node.Expanded = false;
                
                parent.ChildNodes.Add(node);
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            parent.Text += " (Access Denied)";
        }
        catch (System.IO.IOException ex)
        {
            parent.Text += 
                string.Format(" (Unknown Error: {0})", ex.Message);
        }
    }

    protected void TreeView1_TreeNodePopulate(object sender, 
        TreeNodeEventArgs e)
    {
        LoadDirectories(e.Node, e.Node.Value);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate a Directory</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:TreeView ID="TreeView1" runat="server" OnTreeNodePopulate="TreeView1_TreeNodePopulate"></asp:TreeView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
