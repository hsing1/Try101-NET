﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    System.IO.Ports.SerialPort SerialPort1 = new System.IO.Ports.SerialPort("COM5");
    System.IO.Ports.SerialPort SerialPort2 = new System.IO.Ports.SerialPort("COM6");

    protected void Page_Load(object sender, EventArgs e)
    {
        this.SerialPort2.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(SerialPort2_DataReceived);
        
        this.SerialPort1.Open();
        this.SerialPort2.Open();

        this.SerialPort1.Write("Hello World");
        this.SerialPort1.Close();
    }

    void SerialPort2_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
    {
        string data = this.SerialPort2.ReadExisting();
        this.SerialPort2.Close();

        this.lblResult.Text = data;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>