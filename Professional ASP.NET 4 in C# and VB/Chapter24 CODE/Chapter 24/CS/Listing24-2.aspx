﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        
            foreach (System.IO.DriveInfo drive in 
                System.IO.DriveInfo.GetDrives())
            {
                TreeNode node = new TreeNode();
                node.Value = drive.Name;

                //Make sure the drive is ready before we access it
                if (drive.IsReady)
                    node.Text = 
                        String.Format("{0} - (free space: {1})",
                                drive.Name, drive.AvailableFreeSpace);
                else
                    node.Text = 
                        String.Format("{0} - (not ready)", drive.Name);

                this.TreeView1.Nodes.Add(node);
            }
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate Local System Drives</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:TreeView ID="TreeView1"
                                runat="server"></asp:TreeView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

