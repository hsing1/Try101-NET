﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        System.Net.Mail.MailMessage message =
            new System.Net.Mail.MailMessage("webmaster@example.com", "webmaster@example.com");
        message.Subject = "Sending Mail with ASP.NET";
        message.Body = 
            "This is a sample email which demonstrates sending email using ASP.NET";
        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("localhost");
        smtp.Send(message);
    }
</script>