﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        int bufferlength = 1024;
        string response, chunk;
        byte[] downbytes = new byte[bufferlength];
        byte[] sendbytes = new byte[bufferlength];
        int size;
        
        System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient("msnews.microsoft.com", 119);
        System.Net.Sockets.NetworkStream ns = client.GetStream();

        size = ns.Read(downbytes, 0, bufferlength);
        response = System.Text.Encoding.ASCII.GetString(downbytes, 0, size);

        if (response.Substring(0, 3) == "200")
        {
            sendbytes = ASCIIEncoding.Default.GetBytes("LIST" + Environment.NewLine);
            ns.Write(sendbytes, 0, sendbytes.Length);

            response = "";

            while ((size = ns.Read(downbytes, 0, downbytes.Length)) > 0)
            {
                chunk = Encoding.ASCII.GetString(downbytes, 0, size);
                response += chunk;
                
                if (chunk.Substring(chunk.Length - 5, 5) == "\r\n.\r\n")
                {
                    response = response.Substring(0, response.Length - 3);
                    break;
                }
            }
        }

        this.lblResult.Text = response;
        
        ns.Close();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Writing a Text File</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Literal runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>