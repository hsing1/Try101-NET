﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        System.IO.BinaryWriter binarywriter = 
                     new System.IO.BinaryWriter( 
                         System.IO.File.Create(MapPath("binary.dat")));
        binarywriter.Write("a string");
        binarywriter.Write(0x12346789abcdef);
        binarywriter.Write(0x12345678);
        binarywriter.Write('c');
        binarywriter.Write(1.5f);
        binarywriter.Write(100.2m);
        binarywriter.Close();

        System.IO.BinaryReader binaryreader =
                     new System.IO.BinaryReader(
                         System.IO.File.Open(MapPath("binary.dat"), 
                         System.IO.FileMode.Open));

        string a = binaryreader.ReadString();
        long l = binaryreader.ReadInt64();
        int i = binaryreader.ReadInt32();
        char c = binaryreader.ReadChar();
        float f = binaryreader.ReadSingle();
        decimal d = binaryreader.ReadDecimal();
        binaryreader.Close();
    }
</script>