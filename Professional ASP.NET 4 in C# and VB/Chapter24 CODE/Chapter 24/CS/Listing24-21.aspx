﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string filename = Server.MapPath("TextFile.txt");

        if (System.IO.File.Exists(filename))
        {
            using (System.IO.FileStream infile = System.IO.File.Open(filename, System.IO.FileMode.Open),
                outfile = System.IO.File.Create(System.IO.Path.ChangeExtension(filename, "zip")))
            {
                using (System.IO.Compression.DeflateStream cstream =
                    new System.IO.Compression.DeflateStream(outfile, System.IO.Compression.CompressionMode.Compress))
                {
                    byte[] data = new byte[infile.Length];
                    int counter = 0;

                    while ((counter = infile.Read(data, 0, data.Length)) != 0)
                    {
                        cstream.Write(data, 0, counter);
                    }
                }
            }
        }
    }
</script>