﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Label1.Text = System.IO.Directory.GetCurrentDirectory();
        System.IO.Directory.SetCurrentDirectory(MapPath(""));
        this.Label2.Text = System.IO.Directory.GetCurrentDirectory();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Set and Display the Working Directory</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Old Working Directory: 
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
        New Working Directory: 
            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
    </div>
    </form>
</body>
</html>
