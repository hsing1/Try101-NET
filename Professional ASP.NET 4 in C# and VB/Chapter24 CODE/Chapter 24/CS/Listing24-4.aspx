﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        System.IO.Directory.CreateDirectory(MapPath("Wrox"));

        if (System.IO.Directory.Exists(MapPath("Wrox")))
        {
            this.Label1.Text =
                     System.IO.Directory.GetCreationTime(MapPath("Wrox")).ToString();
            this.Label2.Text =
                     System.IO.Directory.GetLastAccessTime(MapPath("Wrox")).ToString();
            this.Label3.Text =
                     System.IO.Directory.GetLastWriteTime(MapPath("Wrox")).ToString();

            System.IO.Directory.Delete(MapPath("Wrox"));
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using Static Methods</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Creation Time: 
              <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
        Last Access Time: 
              <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label><br />
        Last Write Time:
              <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
    </div>
    </form>
</body>
</html>
