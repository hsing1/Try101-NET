﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            foreach (System.IO.DriveInfo drive in System.IO.DriveInfo.GetDrives())
            {
                TreeNode node = new TreeNode();
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.PopulateOnDemand = true;
                node.Expanded = false;      
                node.Value = drive.Name;

                if (drive.IsReady)
                    node.Text =
                        String.Format("{0} - (free space: {1})",
                                drive.Name, drive.AvailableFreeSpace);
                else
                    node.Text =
                        String.Format("{0} - (not ready)", drive.Name);

                this.TreeView1.Nodes.Add(node);
            }
        }
    }

    private void LoadDirectories(TreeNode parent, string path)
    {
        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(path);

        try
        {
            foreach (System.IO.DirectoryInfo d in directory.GetDirectories())
            {
                TreeNode node = new TreeNode(d.Name, d.FullName);
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.Expanded = false;
                node.PopulateOnDemand = true;

                parent.ChildNodes.Add(node);
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            parent.Text += " (Access Denied)";
        }
        catch (System.IO.IOException ex)
        {
            parent.Text +=
                string.Format(" (Unknown Error: {0})", ex.Message);
        }
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        LoadDirectories(e.Node, e.Node.Value);
    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        try
        {
            System.IO.DirectoryInfo directory =
                      new System.IO.DirectoryInfo(this.TreeView1.SelectedNode.Value);

            this.GridView1.DataSource = directory.GetFiles();
            this.GridView1.DataBind();
        }
        catch (System.UnauthorizedAccessException ex)
        {
            TreeView1.SelectedNode.Text += " (Access Denied)";
        }
        catch (System.IO.IOException ex)
        {
            TreeView1.SelectedNode.Text +=
                string.Format(" (Unknown Error: {0})", ex.Message);
        }

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Binding a Gridview </title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:TreeView ID="TreeView1" runat="server" 
                            OnTreeNodePopulate="TreeView1_TreeNodePopulate"
                            OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" />
                        </td>
                        <td valign="top">
                            <asp:GridView ID="GridView1" runat="server"
                                 AutoGenerateColumns="False" GridLines="None" CellPadding="3">
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Name"
                                         HeaderStyle-HorizontalAlign=Left 
                                         HeaderStyle-Font-Bold=true />
                                    <asp:BoundField DataField="Length" HeaderText="Size"
                                         ItemStyle-HorizontalAlign=Right 
                                         HeaderStyle-HorizontalAlign=Right 
                                         HeaderStyle-Font-Bold=true />
                                    <asp:BoundField DataField="LastWriteTime"
                                         HeaderText="Date Modified" 
                                         HeaderStyle-HorizontalAlign=Left 
                                         HeaderStyle-Font-Bold=true />
                                </Columns>
                            </asp:GridView>
                        </td>                
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
