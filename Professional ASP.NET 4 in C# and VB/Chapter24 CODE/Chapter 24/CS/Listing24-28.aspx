﻿<%@ Page Language="C#" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        Uri url = new Uri("ftp://ftp.microsoft.com/SoftLib/ReadMe.txt ");
        if (url.Scheme == Uri.UriSchemeFtp)
        {
            System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.FtpWebRequest.Create(url);
            request.Method = System.Net.WebRequestMethods.Ftp.DownloadFile;
            System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse();
            System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
            string tmp = reader.ReadToEnd();
            response.Close();

            this.ftpContent.InnerText = tmp;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using FTP from an ASP.NET webpage</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div runat="server" id="ftpContent" 
            style="overflow:scroll; height: 260px; width: 450px;">
        </div>
    </div>
    </form>
</body>
</html>
