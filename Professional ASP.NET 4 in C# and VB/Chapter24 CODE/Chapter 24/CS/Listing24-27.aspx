﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        Uri url = new Uri("http://api.technorati.com/tag");

        string tag = "tag=ASP.NET";
        string key = "key=77aefe8d6b12aa7ccadf4a0ab91c78c2";
        string data = string.Format("{0}&{1}", key, tag);
        
        if (url.Scheme == Uri.UriSchemeHttp)
        {
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(url);
            request.Method = System.Net.WebRequestMethods.Http.Post;
            request.ContentLength = data.Length;
            request.ContentType = "application/x-www-form-urlencoded";

           System.IO.StreamWriter writer = new System.IO.StreamWriter( request.GetRequestStream() );
            writer.Write( data );
            writer.Close();

            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
            System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
            string result = reader.ReadToEnd();
            response.Close();

            this.XmlDataSource1.Data = result;
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:XmlDataSource runat="server" ID="XmlDataSource1"></asp:XmlDataSource>
        <asp:TreeView runat="server" DataSourceID="XmlDataSource1"></asp:TreeView>
    </div>
    </form>
</body>
</html>
