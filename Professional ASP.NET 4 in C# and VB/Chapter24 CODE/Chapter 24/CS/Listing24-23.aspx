﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        byte[] bytes = new byte[100];
        
        using (System.IO.MemoryMappedFiles.MemoryMappedFile mmf = 
            System.IO.MemoryMappedFiles.MemoryMappedFile.CreateFromFile(MapPath("TextFile1.txt"),System.IO.FileMode.Open, "MyTextFile"))
            {
            using (System.IO.MemoryMappedFiles.MemoryMappedViewAccessor accessor =
                mmf.CreateViewAccessor(100, 100))
                {
                   accessor.ReadArray(0, bytes, 0, bytes.Length);
                }
        }
        
        this.lblResult.Text = ASCIIEncoding.Default.GetString(bytes);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>
