﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            this.lblRootPath.Text = 
                           System.IO.Path.GetPathRoot(this.txtPathName.Text);
            this.lblDirectoryName.Text =
                           System.IO.Path.GetDirectoryName(this.txtPathName.Text);
            this.lblFileName.Text =
                           System.IO.Path.GetFileName(this.txtPathName.Text);
            this.lblFileNameWithoutExtension.Text =
                           System.IO.Path.GetFileNameWithoutExtension(this.txtPathName.Text);
            this.lblExtension.Text =
                           System.IO.Path.GetExtension(this.txtPathName.Text);

            this.lblTemporaryPath.Text = System.IO.Path.GetTempPath();
            this.lblDirectorySeparatorChar.Text =
                           System.IO.Path.DirectorySeparatorChar.ToString();
            this.lblAltDirectorySeparatorChar.Text =
                           System.IO.Path.AltDirectorySeparatorChar.ToString();
            this.lblVolumeSeparatorChar.Text = System.IO.Path.VolumeSeparatorChar.ToString();
            this.lblPathSeparator.Text = System.IO.Path.PathSeparator.ToString();

            this.lblInvalidChars.Text =
               HttpUtility.HtmlEncode(new String(System.IO.Path.GetInvalidPathChars()));
            this.lblInvalidFileNameChars.Text =
               HttpUtility.HtmlEncode(new String(System.IO.Path.GetInvalidFileNameChars()));
        }
    }
</script>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using the Path Class</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Working with the Path Class<br />
        <br />
        Enter a path name:
        <asp:TextBox ID="txtPathName" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button1" runat="server" Text="Submit" /><br />
        <br />
        Root Path = 
        <asp:Label ID="lblRootPath" runat="server" Text="Label" />
        <br />
        Directory =
        <asp:Label ID="lblDirectoryName" runat="server" Text="Label" />
        <br />
        Filename =
        <asp:Label ID="lblFileName" runat="server" Text="Label" />
        <br />
        Filename (without extension) =
        <asp:Label ID="lblFileNameWithoutExtension" runat="server" Text="Label" />
        <br />
        Extension =
        <asp:Label ID="lblExtension" runat="server" Text="Label" />
        <br />
        <br />
        Temporary Directory =
        <asp:Label ID="lblTemporaryPath" runat="server" Text="Label" />
        <br />
        Directory Separator Character =
        <asp:Label ID="lblDirectorySeparatorChar" runat="server" Text="Label" />
        <br />
        Alt Directory Separator Character =
        <asp:Label ID="lblAltDirectorySeparatorChar" runat="server" Text="Label" />
        <br />
        Volume Separator Character =
        <asp:Label ID="lblVolumeSeparatorChar" runat="server" Text="Label" />
        <br />
        Path Separator Character =
        <asp:Label ID="lblPathSeparator" runat="server" Text="Label" />
        <br />
        Invalid Path Characters =
        <asp:Label ID="lblInvalidChars" runat="server" Text="Label" />
        <br />
        Invalid FileName Characters =
        <asp:Label ID="lblInvalidFileNameChars" runat="server" Text="Label" />
    
    </div>
    </form>
</body>
</html>
