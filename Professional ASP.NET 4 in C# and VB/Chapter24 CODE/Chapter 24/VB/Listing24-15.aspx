﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim fs As New System.IO.FileStream(Server.MapPath("TextFile.txt"),
            System.IO.FileMode.Append, System.IO.FileAccess.Write,
            System.IO.FileShare.Read, 8, System.IO.FileOptions.None)
        Dim data() As Byte =
                        System.Text.Encoding.ASCII.GetBytes("This is an additional string")
        fs.Write(data, 0, data.Length)
        fs.Flush()
        fs.Close()
        
        Me.lblResult.Text = ASCIIEncoding.Default.GetString(data)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Writing a Text File</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>