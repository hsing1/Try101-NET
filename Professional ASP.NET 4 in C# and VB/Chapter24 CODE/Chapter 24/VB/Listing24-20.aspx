﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Opens a file and returns a FileStream
        Using stream1 As System.IO.FileStream = System.IO.File.Open(MapPath("TextFile.txt"), System.IO.FileMode.Open)
        End Using

        ' Opens a file and returns a StreamReader for reading the data
        Using stream2 As System.IO.StreamReader = System.IO.File.OpenText(MapPath("TextFile.txt"))
        End Using
        
        ' Opens a filestream for reading
        Using stream3 As System.IO.FileStream = System.IO.File.OpenRead(MapPath("TextFile.txt"))
        End Using
        
        ' Opens a filestream for writing 
        Using stream4 As System.IO.FileStream = System.IO.File.OpenWrite(MapPath("TextFile.txt"))
        End Using
        
        ' Reads the entire file and returns a string of data
        Dim data As String = System.IO.File.ReadAllText(MapPath("TextFile.txt"))

        ' Using a LINQ query to locate specific lines within the Textfile
        Dim lines1 = From line In System.IO.File.ReadLines(MapPath("TextFile.txt"))
                    Where line.Contains("laoreet")
                    Select line

        ' Using a LINQ query to locate specific lines within the Textfile
        Dim lines2 = From line In System.IO.File.ReadLines(MapPath("TextFile.txt"))
                    Where line.Contains("laoreet")
                    Select line
                    
        ' Appends the text data to a new text file
        System.IO.File.AppendAllText(MapPath("TextFile1.txt"), data)

        ' Append the IEnumerable of strings to a new text file
        System.IO.File.AppendAllLines(MapPath("TextFile1.txt"), lines1)

        ' Writes the string of data to a new file 
        System.IO.File.WriteAllText(MapPath("TextFile2.txt"), data)

        ' Writes an IEnumerable of strings to a new text file
        System.IO.File.WriteAllLines(MapPath("TextFile2.txt"), lines2)

    End Sub
</script>