﻿<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As New Uri("http://www.wrox.com/")
        If (url.Scheme = uri.UriSchemeHttp) Then
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)
            request.Method = System.Net.WebRequestMethods.Http.Get
            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            Dim reader As New System.IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            response.Close()

            Me.Panel1.GroupingText = tmp
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>This is the wrox.com website:</p>
        <asp:Panel ID="Panel1" runat="server" 
            Height="355px" Width="480px" ScrollBars=Auto>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
