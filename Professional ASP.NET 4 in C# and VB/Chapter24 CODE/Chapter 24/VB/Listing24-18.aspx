﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim streamwriter As New System.IO.StreamWriter(
            System.IO.File.Open(MapPath("TextFile.txt"), System.IO.FileMode.Open))
        streamwriter.Write("This is a string")
        streamwriter.Close()

        Dim reader As New System.IO.StreamReader(
            System.IO.File.Open(MapPath("TextFile.txt"), System.IO.FileMode.Open))
        Dim tmp As String = reader.ReadToEnd()
        reader.Close()
    End Sub
</script>

