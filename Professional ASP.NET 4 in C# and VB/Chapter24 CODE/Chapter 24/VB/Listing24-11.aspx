﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim sec As System.Security.AccessControl.FileSecurity =
                 System.IO.File.GetAccessControl(Server.MapPath("TextFile.txt"))

        sec.AddAccessRule(
                 New System.Security.AccessControl.FileSystemAccessRule(
                          "WIN7\TestUser",
                          System.Security.AccessControl.FileSystemRights.FullControl,
                          System.Security.AccessControl.AccessControlType.Allow
                          )
                 )

        System.IO.File.SetAccessControl(Server.MapPath("TextFile.txt"),sec)
    End Sub
</script>