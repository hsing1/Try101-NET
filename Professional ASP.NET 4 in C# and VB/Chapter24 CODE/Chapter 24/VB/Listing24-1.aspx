﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object,
                            ByVal e As System.EventArgs)
        Dim drive As New System.IO.DriveInfo("C:\")
        lblDriveName.Text = drive.Name
        lblDriveType.Text = drive.DriveType.ToString()
        lblAvailableFreeSpace.Text = drive.AvailableFreeSpace.ToString()
        lblDriveFormat.Text = drive.DriveFormat
        lblTotalFreeSpace.Text = drive.TotalFreeSpace.ToString()
        lblTotalSize.Text = drive.TotalSize.ToString()
        lblVolumeLabel.Text = drive.VolumeLabel
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Displaying Drive Information</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr><td>Drive Name:</td><td>
                <asp:Label ID="lblDriveName" runat="server" Text="Label" />
            </td></tr>
            <tr><td>Drive Type:</td><td>
                <asp:Label ID="lblDriveType" runat="server" Text="Label"/>
            </td></tr>
            <tr><td>Available Free Space:</td><td>
               <asp:Label ID="lblAvailableFreeSpace" runat="server" Text="Label" />
            </td></tr>
            <tr><td>Drive Format:</td><td>
                <asp:Label ID="lblDriveFormat" runat="server" Text="Label" />
            </td></tr>
            <tr><td>Total Free Space:</td><td>
                <asp:Label ID="lblTotalFreeSpace" runat="server" Text="Label" />
            </td></tr>
            <tr><td>Total Size:</td><td>
                <asp:Label ID="lblTotalSize" runat="server" Text="Label" />
            </td></tr>
            <tr><td>Volume Label</td><td>
                <asp:Label ID="lblVolumeLabel" runat="server" Text="Label" /> 
            </td></tr>
        </table>
    </div>
    </form>
</body>
</html>
