﻿<%@ Page Language="VB" %>

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As Uri = New Uri("http://api.technorati.com/tag")

        Dim tag As String = "tag=ASP.NET"
        Dim key As String = "key=77aefe8d6b12aa7ccadf4a0ab91c78c2"
        Dim data As String = String.Format("{0}&{1}", key, tag)
        
        If url.Scheme = Uri.UriSchemeHttp Then
        
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)
            request.Method = System.Net.WebRequestMethods.Http.Post
            request.ContentLength = data.Length
            request.ContentType = "application/x-www-form-urlencoded"

            Dim writer As System.IO.StreamWriter = New System.IO.StreamWriter(request.GetRequestStream())
            writer.Write(data)
            writer.Close()

            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            Dim reader As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())
            Dim result As String = reader.ReadToEnd()
            response.Close()

            Me.XmlDataSource1.Data = result
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:XmlDataSource runat="server" ID="XmlDataSource1"></asp:XmlDataSource>
        <asp:TreeView ID="TreeView1" runat="server" DataSourceID="XmlDataSource1"></asp:TreeView>
    </div>
    </form>
</body>
</html>
