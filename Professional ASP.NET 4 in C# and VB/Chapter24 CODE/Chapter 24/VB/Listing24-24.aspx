﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Dim SerialPort1 As System.IO.Ports.SerialPort = New System.IO.Ports.SerialPort("COM5")
    Dim WithEvents SerialPort2 As System.IO.Ports.SerialPort = New System.IO.Ports.SerialPort("COM6")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.SerialPort1.Open()
        Me.SerialPort2.Open()

        Me.SerialPort1.Write("Hello World")
        Me.SerialPort1.Close()
    End Sub

    Protected Sub SerialPort2_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles SerialPort2.DataReceived
        Dim data As String = Me.SerialPort2.ReadExisting()
        Me.SerialPort2.Close()

        Me.lblResult.Text = data
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>
