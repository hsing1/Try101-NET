﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dir As New System.IO.DirectoryInfo(MapPath(""))
        
        For Each file As System.IO.FileInfo In dir.GetFiles("*.*")
            Dim row As HtmlTableRow = New HtmlTableRow()
            row.Cells.Add(New HtmlTableCell() With _
                          {.InnerHtml = file.Name})
            row.Cells.Add(New HtmlTableCell() With _
                          {.InnerHtml = file.LastWriteTime.ToString()})
            row.Cells.Add(New HtmlTableCell() With _
                          {.InnerHtml = file.Attributes.ToString()})
            Me.HtmlTable1.Rows.Add(row)
        Next
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate a FileInfo</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table runat="server" id="HtmlTable1">
            <tr><td>Name</td><td>Last Write Time</td><td>Attributes</td></tr>
        </table>
    </div>
    </form>
</body>
</html>