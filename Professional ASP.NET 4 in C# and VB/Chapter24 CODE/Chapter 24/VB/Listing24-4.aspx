﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
  
    Protected Sub Page_Load(ByVal sender As Object,
                            ByVal e As System.EventArgs)
        
        System.IO.Directory.CreateDirectory(MapPath("Wrox"))

        If System.IO.Directory.Exists(MapPath("Wrox")) Then
        
            Me.Label1.Text =
                     System.IO.Directory.GetCreationTime(MapPath("Wrox")).ToString()
            Me.Label2.Text =
                     System.IO.Directory.GetLastAccessTime(MapPath("Wrox")).ToString()
            Me.Label3.Text =
                     System.IO.Directory.GetLastWriteTime(MapPath("Wrox")).ToString()

            System.IO.Directory.Delete(MapPath("Wrox"))
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using Static Methods</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Creation Time: 
              <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
        Last Access Time: 
              <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label><br />
        Last Write Time:
              <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
    </div>
    </form>
</body>
</html>
