﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim bufferlength As Integer = 1024
        Dim response, chunk As String
        Dim downbytes(bufferlength) As Byte
        Dim sendbytes(bufferlength) As Byte
        Dim size As Integer
        
        Dim client As System.Net.Sockets.TcpClient = New System.Net.Sockets.TcpClient("msnews.microsoft.com", 119)
        Dim ns As System.Net.Sockets.NetworkStream = client.GetStream()

        size = ns.Read(downbytes, 0, bufferlength)
        response = System.Text.Encoding.ASCII.GetString(downbytes, 0, size)

        If response.Substring(0, 3) = "200" Then

            sendbytes = ASCIIEncoding.Default.GetBytes("LIST" + Environment.NewLine)
            ns.Write(sendbytes, 0, sendbytes.Length)

            response = ""
            size = ns.Read(downbytes, 0, downbytes.Length)

            While (size > 0)
                chunk = Encoding.ASCII.GetString(downbytes, 0, size)
                response += chunk
                
                If chunk.Substring(chunk.Length - 5, 5) = Environment.NewLine & "." & Environment.NewLine Then
                    response = response.Substring(0, response.Length - 3)
                    Exit While
                End If
                
                size = ns.Read(downbytes, 0, downbytes.Length)
            End While
        End If

        Me.lblResult.Text = response

        ns.Close()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Writing a Text File</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Literal runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>