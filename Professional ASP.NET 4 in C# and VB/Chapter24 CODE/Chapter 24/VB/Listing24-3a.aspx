﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object,
                            ByVal e As System.EventArgs)

        If (Not Page.IsPostBack) Then

            For Each drive As System.IO.DriveInfo In
                System.IO.DriveInfo.GetDrives()

                Dim node As TreeNode = New TreeNode()
                node.SelectAction = TreeNodeSelectAction.SelectExpand
                node.PopulateOnDemand = True
                node.Expanded = False
                node.Value = drive.Name

                If (drive.IsReady) Then
                    node.Text =
                        String.Format("{0} - (free space: {1})",
                                    drive.Name, drive.AvailableFreeSpace)
                Else
                    node.Text =
                        String.Format("{0} - (not ready)", drive.Name)
                End If

                Me.TreeView1.Nodes.Add(node)
            Next

        End If
        
    End Sub
    
    Private Sub LoadDirectories(ByVal parent As TreeNode,
                                ByVal path As String)
        
        Dim directory As System.IO.DirectoryInfo = _
                  New System.IO.DirectoryInfo(path)
        
        Try
            For Each d As System.IO.DirectoryInfo In
                directory.EnumerateDirectories()
            
                
                Dim node As TreeNode = New TreeNode(d.Name, d.FullName)
                node.SelectAction = TreeNodeSelectAction.SelectExpand
                node.PopulateOnDemand = True
                node.Expanded = False
                
                parent.ChildNodes.Add(node)
            Next
        Catch ex As System.UnauthorizedAccessException
            parent.Text += " (Access Denied)"
        Catch ex As System.IO.IOException
            parent.Text +=
                String.Format(" (Unknown Error: {0})", ex.Message)
        End Try
    End Sub

    Protected Sub TreeView1_TreeNodePopulate(ByVal sender As Object,
                    ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs)
        LoadDirectories(e.Node, e.Node.Value)
    End Sub
</script>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate a Directory</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:TreeView ID="TreeView1" runat="server" 
                            OnTreeNodePopulate="TreeView1_TreeNodePopulate">
                            </asp:TreeView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
