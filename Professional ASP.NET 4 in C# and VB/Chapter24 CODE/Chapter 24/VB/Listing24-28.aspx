﻿<%@ Page Language="VB" %>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As New Uri("ftp://ftp.microsoft.com/SoftLib/ReadMe.txt")
        If (url.Scheme = Uri.UriSchemeFtp) Then
            Dim request As System.Net.FtpWebRequest = System.Net.FtpWebRequest.Create(url)
            request.Method = System.Net.WebRequestMethods.Ftp.DownloadFile
            Dim response As System.Net.FtpWebResponse = request.GetResponse()
            Dim reader As New System.IO.StreamReader(response.GetResponseStream())
            Dim tmp As String = reader.ReadToEnd()
            response.Close()
        
            Me.ftpContent.InnerText = tmp
        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using FTP from an ASP.NET webpage</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div runat="server" id="ftpContent" 
            style="overflow:scroll; height: 260px; width: 450px;">
        </div>
    </div>
    </form>
</body>
</html>
