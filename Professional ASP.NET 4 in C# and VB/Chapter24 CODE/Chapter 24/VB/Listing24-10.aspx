﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim sec As System.Security.AccessControl.FileSecurity
        sec = System.IO.File.GetAccessControl(Server.MapPath("TextFile.txt"))
        
        Me.Label1.Text =
                 sec.GetOwner(GetType(System.Security.Principal.NTAccount)).Value
      
        Dim auth As  _
            System.Security.AccessControl.AuthorizationRuleCollection =
                                     sec.GetAccessRules(True, True,
                                     GetType(System.Security.Principal.NTAccount))

        For Each r As  _
            System.Security.AccessControl.FileSystemAccessRule In auth
            
            Dim tr As New TableRow()
            tr.Cells.Add(New TableCell() With {.Text = r.AccessControlType.ToString()})
            tr.Cells.Add(New TableCell() With {.Text = r.IdentityReference.Value})
            tr.Cells.Add(New TableCell() With {.Text = r.InheritanceFlags.ToString()})
            tr.Cells.Add(New TableCell() With {.Text = r.IsInherited.ToString()})
            tr.Cells.Add(New TableCell() With {.Text = r.PropagationFlags.ToString()})
            tr.Cells.Add(New TableCell() With {.Text = r.FileSystemRights.ToString()})
            tr.Cells.Add(New TableCell() With {.Text = r.IdentityReference.Value})
            Table1.Rows.Add(tr)
        Next
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Displaying ACL Information</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p><b>File Owner:</b>
              <asp:Label ID="Label1" runat="server" Text="Label" /></p>        
        <p>
        Access Rules:<br />
        <asp:Table ID="Table1" runat="server" CellPadding="2" GridLines="Both">
            <asp:TableRow>
                <asp:TableHeaderCell>Control Type</asp:TableHeaderCell>
                <asp:TableHeaderCell>Identity</asp:TableHeaderCell>
                <asp:TableHeaderCell>Inheritance Flags</asp:TableHeaderCell>
                <asp:TableHeaderCell>Is Inherited</asp:TableHeaderCell>
                <asp:TableHeaderCell>Propagation Flags</asp:TableHeaderCell>     
                <asp:TableHeaderCell>File System Rights</asp:TableHeaderCell>
            </asp:TableRow>
        </asp:Table>
        </p>        
    </div>
    </form>
</body>
</html>
