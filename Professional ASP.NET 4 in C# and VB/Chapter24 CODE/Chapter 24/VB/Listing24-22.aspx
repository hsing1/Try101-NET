﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filename As String = Server.MapPath("TextFile.zip")

        If System.IO.File.Exists(filename) Then
        
            Using infile As System.IO.FileStream = System.IO.File.Open(filename, System.IO.FileMode.Open),
                outfile As System.IO.FileStream = System.IO.File.Create(System.IO.Path.ChangeExtension(filename, "txt"))
            
                Using cstream As System.IO.Compression.GZipStream =
                    New System.IO.Compression.GZipStream(outfile, System.IO.Compression.CompressionMode.Decompress)

                    Dim data(infile.Length) As Byte
                    Dim counter As Integer = 0

                    counter = cstream.Read(data, 0, data.Length)

                    While (counter <> 0)
                        outfile.Write(data, 0, counter)
                        counter = cstream.Read(data, 0, data.Length)
                    End While
                End Using
            End Using
        End If
    End Sub
</script>