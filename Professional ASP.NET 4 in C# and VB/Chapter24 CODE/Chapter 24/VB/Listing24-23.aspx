﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bytes(100) As Byte
        
        Using mmf As System.IO.MemoryMappedFiles.MemoryMappedFile =
            System.IO.MemoryMappedFiles.MemoryMappedFile.CreateFromFile(MapPath("TextFile1.txt"), System.IO.FileMode.Open, "MyTextFile")
            Using accessor As System.IO.MemoryMappedFiles.MemoryMappedViewAccessor =
                mmf.CreateViewAccessor(100, 100)
                accessor.ReadArray(0, bytes, 0, bytes.Length)
            End Using
        End Using
        
        Me.lblResult.Text = ASCIIEncoding.Default.GetString(bytes)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblResult" />
    </div>
    </form>
</body>
</html>
