﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Using pipeClient As System.IO.Pipes.NamedPipeClientStream =
            New System.IO.Pipes.NamedPipeClientStream(".", "testpipe", System.IO.Pipes.PipeDirection.Out)
            pipeClient.Connect(10000)
            
            Try
                Dim bytes() As Byte = ASCIIEncoding.Default.GetBytes("Hello World")
                pipeClient.Write(bytes, 0, bytes.Length)
            Catch ex As System.IO.IOException
                Console.WriteLine("ERROR: {0}", ex.Message)
            End Try
            
        End Using
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
