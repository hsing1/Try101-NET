﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object,
                            ByVal e As System.EventArgs)
        If (Not Page.IsPostBack) Then

            For Each drive As System.IO.DriveInfo In
                System.IO.DriveInfo.GetDrives()

                Dim node As TreeNode = New TreeNode()
                node.Value = drive.Name

                If (drive.IsReady) Then
                    node.Text =
                        String.Format("{0} - (free space: {1})",
                                    drive.Name, drive.AvailableFreeSpace)
                Else
                    node.Text =
                        String.Format("{0} - (not ready)", drive.Name)
                End If

                Me.TreeView1.Nodes.Add(node)
            Next

        End If
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Enumerate Local System Drives</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <table>
                    <tr>
                        <td style="width: 100px" valign="top">
                            <asp:TreeView ID="TreeView1"
                                runat="server"></asp:TreeView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
