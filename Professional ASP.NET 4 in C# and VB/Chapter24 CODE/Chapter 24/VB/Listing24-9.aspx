﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim file As New System.IO.FileInfo(Server.MapPath("TextFile.txt"))
        Me.lblLocation.Text = file.FullName
        Me.lblSize.Text = file.Length.ToString()
        Me.lblCreated.Text = file.CreationTime.ToString()
        Me.lblModified.Text = file.LastWriteTime.ToString()
        Me.lblAccessed.Text = file.LastAccessTime.ToString()
        Me.lblAttributes.Text = file.Attributes.ToString()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using the Path Class</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Location: <asp:Label ID="lblLocation" runat="server" /><br />
        Size: <asp:Label ID="lblSize" runat="server" /><br />
        Created: <asp:Label ID="lblCreated" runat="server" /><br />
        Modified: <asp:Label ID="lblModified" runat="server" /><br />
        Accessed: <asp:Label ID="lblAccessed" runat="server" /><br />
        Attributes: <asp:Label ID="lblAttributes" runat="server" /><br />
    </div
    </form>
</body>
</html>