﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapter_24_NamedPipesServer
{
    class Program
    {
        static void Main(string[] args)
        {
            bool end=false;

            while (!end)
            {
                using (System.IO.Pipes.NamedPipeServerStream pipeServer =
                    new System.IO.Pipes.NamedPipeServerStream("testpipe", System.IO.Pipes.PipeDirection.In))
                {
                    Console.WriteLine("NamedPipeServerStream object created.");

                    // Wait for a client to connect
                    Console.Write("Waiting for client connection...");
                    pipeServer.WaitForConnection();

                    Console.WriteLine("Client connected.");
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(pipeServer))
                    {
                        // Display the read text to the console
                        string temp;
                        while ((temp = sr.ReadLine()) != null)
                        {
                            Console.WriteLine("Received from client: {0}", temp);
                        }
                    }
                }
            }
        }
    }
}
