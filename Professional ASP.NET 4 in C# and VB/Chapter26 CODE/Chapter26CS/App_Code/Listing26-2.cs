﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Chapter26.CS
{
    public class AppendMessage : IHttpModule
    {
        private HttpApplication _application = null;

        #region IHttpModule Members

        public void Dispose()
        {
            
        }

        public void Init(System.Web.HttpApplication context)
        {
            _application = context;

            context.EndRequest += new EventHandler(context_EndRequest);
        }

        void context_EndRequest(object sender, EventArgs e)
        {
            string message =
                string.Format("processed on {0}", 
                    System.DateTime.Now.ToString());

            _application.Context.Response.Write(message);
        }

        #endregion
    }
}
