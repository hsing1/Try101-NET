﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Chapter26.CS
{
    class SimpleModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void Init(HttpApplication context)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
