﻿using System.Web;

namespace Chapter26.CS
{
    public class MappedHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "image/jpeg";
            context.Response.WriteFile("Garden.jpg");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
