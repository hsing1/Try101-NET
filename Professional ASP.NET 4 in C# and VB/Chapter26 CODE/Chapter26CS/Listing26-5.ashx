﻿<%@ WebHandler Language="C#" Class="Handler5" %>

using System;
using System.Web;

public class Handler5 : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        //Logic to retrieve the image file
        context.Response.ContentType = "image/jpeg";
        context.Response.WriteFile("Garden.jpg");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}