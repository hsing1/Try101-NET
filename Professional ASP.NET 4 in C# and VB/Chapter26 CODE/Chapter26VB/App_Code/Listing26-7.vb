﻿Imports Microsoft.VisualBasic

Namespace Chapter26.VB
    Public Class MappedHandler : Implements IHttpHandler

        Public Sub ProcessRequest(ByVal context As HttpContext) _
                Implements IHttpHandler.ProcessRequest
            context.Response.ContentType = "image/jpeg"
            context.Response.WriteFile("Garden.jpg")
        End Sub

        Public ReadOnly Property IsReusable() As Boolean _
                Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class
End Namespace
