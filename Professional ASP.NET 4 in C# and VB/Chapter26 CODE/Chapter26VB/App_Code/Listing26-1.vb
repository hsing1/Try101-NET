﻿Imports Microsoft.VisualBasic

Namespace Chapter26.VB

    Public Class SimpleModule
        Implements IHttpModule

        Public Overridable Sub Init(ByVal context As HttpApplication) _
                Implements IHttpModule.Init

        End Sub

        Public Overridable Sub Dispose() Implements IHttpModule.Dispose

        End Sub

    End Class

End Namespace
