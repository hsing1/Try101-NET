﻿Imports Microsoft.VisualBasic

Namespace Chapter26.VB

    Public Class AppendMessage
        Implements IHttpModule

        Dim WithEvents _application As HttpApplication = Nothing

        Public Overridable Sub Init(ByVal context As HttpApplication) _
                Implements IHttpModule.Init
            _application = context
        End Sub

        Public Overridable Sub Dispose() Implements IHttpModule.Dispose

        End Sub
        Public Sub context_EndRequest(
                ByVal sender As Object,
                ByVal e As EventArgs) _
            Handles _application.EndRequest

            Dim message As String =
                String.Format("processed on {0}",
                              System.DateTime.Now.ToString())

            _application.Context.Response.Output.Write(message)
        End Sub

    End Class

End Namespace
