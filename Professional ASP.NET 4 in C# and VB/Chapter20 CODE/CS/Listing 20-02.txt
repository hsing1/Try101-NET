<system.web>
   <authentication mode="Windows" />
   <authorization>
      <allow users="REUTERS-EVJEN\Bubbles" />
      <deny users="*" />
   </authorization>
</system.web>
