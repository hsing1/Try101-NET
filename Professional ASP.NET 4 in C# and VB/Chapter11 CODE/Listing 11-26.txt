<configuration>

   <system.web>

        <webParts>
            <personalization>
                <providers>
                    <add connectionStringName="LocalSqlServer"
                     name="AspNetSqlPersonalizationProvider" 
                     type="System.Web.UI.WebControls.WebParts.SqlPersonalizationProvider, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
                </providers>

                <authorization>
                    <deny users="*" verbs="enterSharedScope" />
                    <allow users="*" verbs="modifyState" />
                </authorization>
            </personalization>

            <transformers>
                <add name="RowToFieldTransformer" 
                 type="System.Web.UI.WebControls.WebParts.RowToFieldTransformer" />
                <add name="RowToParametersTransformer" 
                type="System.Web.UI.WebControls.WebParts.RowToParametersTransformer" />
            </transformers>
        </webParts>

   </system.web>

</configuration>
