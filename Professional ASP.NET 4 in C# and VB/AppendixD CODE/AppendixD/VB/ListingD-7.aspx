﻿<%@ Page Language="VB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bankAccounts = New List(Of OfficeInteropDemo.Account)
        bankAccounts.Add(New OfficeInteropDemo.Account() _
                        With {.ID = 345678, .Balance = 541.27})
        bankAccounts.Add(New OfficeInteropDemo.Account() _
                        With {.ID = 1230221, .Balance = -127.44})

        OfficeInteropDemo.Excel.DisplayInExcel(bankAccounts)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
