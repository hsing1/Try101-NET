﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        var bankAccounts = new List<OfficeInteropDemo.Account> {
            new OfficeInteropDemo.Account { 
                          ID = 345678,
                          Balance = 541.27
                        },
            new OfficeInteropDemo.Account {
                          ID = 1230221,
                          Balance = -127.44
                        }
        };

        OfficeInteropDemo.Excel.DisplayInExcel(bankAccounts);
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
