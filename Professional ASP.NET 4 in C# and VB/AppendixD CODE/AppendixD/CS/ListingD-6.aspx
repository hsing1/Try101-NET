﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        string uri = "http://api.search.live.net/json.aspx?AppId={appID}&Market=en-US&Sources=web";
            
            dynamic bingSearch = new DynamicRest.RestClient(uri, DynamicRest.RestClientMode.Json);
            bingSearch.appID = "57784AEBFF44A93E2F27123CAECFDDDCCD4923A9";

            dynamic searchOptions = new DynamicRest.JsonObject();
            searchOptions.Query = "professional asp.net 4.0";
            dynamic response = bingSearch(searchOptions);

            dynamic results = response.SearchResponse.Web.Results;
        
            this.Repeater1.DataSource=results;
            this.Repeater1.DataBind();
    }

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        dynamic item = e.Item.DataItem;

        ((HyperLink)e.Item.FindControl("Title")).Text =item.Title;
        ((HyperLink)e.Item.FindControl("Title")).NavigateUrl = item.Url;
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Repeater runat="server" ID="Repeater1" OnItemDataBound="Repeater1_ItemDataBound">
            <ItemTemplate>
                <h2><asp:HyperLink runat="server" ID="Title" /></h2>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
