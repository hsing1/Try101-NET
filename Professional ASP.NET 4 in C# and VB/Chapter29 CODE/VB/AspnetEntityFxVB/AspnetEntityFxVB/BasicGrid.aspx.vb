﻿Partial Public Class BasicGrid
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim adventureWorks_DataEntities1 As New AdventureWorks_DataEntities1()

        Dim query = _
         From emp In adventureWorks_DataEntities1.Employees
         Select emp

        GridView1.DataSource = query
        GridView1.DataBind()
    End Sub

End Class