﻿Public Partial Class OneToMany
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim adventureWorks_DataEntities1 As New AdventureWorks_DataEntities1()

        For Each employee In adventureWorks_DataEntities1.Employees
            Dim li As New ListItem()

            li.Text = employee.EmployeeID & " "

            If (Not employee.EmployeePayHistories.IsLoaded) Then
                employee.EmployeePayHistories.Load()
            End If

            For Each pay In employee.EmployeePayHistories
                li.Text &= "Pay Rate: " & pay.Rate & " "
            Next pay

            BulletedList1.Items.Add(li)
        Next employee

    End Sub

End Class