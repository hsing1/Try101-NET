﻿Public Partial Class Inheritance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim adventureWorks_DataEntities As New AdventureWorks_DataEntities()

        Dim query = _
            From c In adventureWorks_DataEntities.Customers _
            Where TypeOf c Is IndividualCustomer _
            Select c

        GridView1.DataSource = query
        GridView1.DataBind()
    End Sub

End Class