﻿Public Partial Class ManyToMany
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim adventureWorks_DataEntities As New AdventureWorks_DataEntities()

        Dim query = _
           From o In adventureWorks_DataEntities.SalesOrderHeaders _
           Where o.SalesOrderDetails.Any(Function(Quantity) _
              Quantity.OrderQty > 5) _
           Select New With {Key o.PurchaseOrderNumber, _
                    Key o.Customer.CustomerID, _
                    Key o.SalesPersonID}

        GridView1.DataSource = query
        GridView1.DataBind()

    End Sub

End Class