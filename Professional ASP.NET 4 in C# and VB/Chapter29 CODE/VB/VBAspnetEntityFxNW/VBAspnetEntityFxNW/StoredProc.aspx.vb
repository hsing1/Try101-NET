﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.EntityClient

Public Class StoredProc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim northwndEntities As New NorthwindEntities()
        Dim entityConnection As EntityConnection = _
          CType(northwndEntities.Connection, EntityConnection)

        Dim storeConnection As DbConnection = _
          entityConnection.StoreConnection
        Dim command As DbCommand = storeConnection.CreateCommand()
        command.CommandText = "Ten Most Expensive Products"
        command.CommandType = CommandType.StoredProcedure

        Dim openConnection As Boolean = _
           command.Connection.State = ConnectionState.Closed

        If openConnection Then
            command.Connection.Open()
        End If

        Dim result = command.ExecuteReader()

        GridView1.DataSource = result
        GridView1.DataBind()
    End Sub

End Class