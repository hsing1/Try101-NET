﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EntityDataSource.aspx.cs" Inherits="AspnetEntityFx.EntityDataSource" %>

<%@ Register assembly="System.Web.Entity, Version=3.5.0.0, 
    Culture=neutral, PublicKeyToken=b77a5c561934e089" 
    namespace="System.Web.UI.WebControls" tagprefix="asp" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Using the EntityDataSource Control</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
        DataKeyNames="CustomerID" DataSourceID="EntityDataSource1" 
        ForeColor="#333333" 
        GridLines="None">
        <RowStyle BackColor="#E3EAEB" />
        <Columns>
            <asp:BoundField DataField="Address" HeaderText="Address" 
                SortExpression="Address" />
            <asp:BoundField DataField="City" HeaderText="City" 
                SortExpression="City" />
            <asp:BoundField DataField="CompanyName" 
                HeaderText="CompanyName" 
                SortExpression="CompanyName" />
            <asp:BoundField DataField="ContactName" 
                HeaderText="ContactName" 
                SortExpression="ContactName" />
            <asp:BoundField DataField="ContactTitle" 
                HeaderText="ContactTitle" 
                SortExpression="ContactTitle" />
            <asp:BoundField DataField="Country" HeaderText="Country" 
                SortExpression="Country" />
            <asp:BoundField DataField="CustomerID" 
                HeaderText="CustomerID" ReadOnly="True" 
                SortExpression="CustomerID" />
            <asp:BoundField DataField="Fax" HeaderText="Fax" 
                SortExpression="Fax" />
            <asp:BoundField DataField="Phone" HeaderText="Phone" 
                SortExpression="Phone" />
            <asp:BoundField DataField="PostalCode" 
                HeaderText="PostalCode" 
                SortExpression="PostalCode" />
            <asp:BoundField DataField="Region" HeaderText="Region" 
                SortExpression="Region" />
        </Columns>
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" 
         ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" 
         HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" 
         ForeColor="#333333" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" 
         ForeColor="White" />
        <EditRowStyle BackColor="#7C6F57" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
        ConnectionString="name=NORTHWNDEntities" 
        DefaultContainerName="NORTHWNDEntities" EnableDelete="True" 
        EnableInsert="True" 
        EnableUpdate="True" EntitySetName="Customers">
    </asp:EntityDataSource>
    </form>
</body>
</html>

