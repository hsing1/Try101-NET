﻿using System;
using System.Linq;

namespace AspnetEntityFx
{
    public partial class ManyToMany : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdventureWorks_DataEntities1 adventureWorks_DataEntities = new AdventureWorks_DataEntities1();

            var query = from o in
                            adventureWorks_DataEntities.SalesOrderHeaders
                        where o.SalesOrderDetails.Any(Quantity =>
                           Quantity.OrderQty > 5)
                        select new
                        {
                            o.PurchaseOrderNumber,
                            o.Customer.CustomerID,
                            o.SalesPersonID
                        };

            GridView1.DataSource = query;
            GridView1.DataBind();

        }
    }
}
