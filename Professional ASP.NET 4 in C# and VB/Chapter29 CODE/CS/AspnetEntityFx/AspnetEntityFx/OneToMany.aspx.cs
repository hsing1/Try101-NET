﻿using System;
using System.Web.UI.WebControls;

namespace AspnetEntityFx
{
    public partial class OneToMany : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdventureWorks_DataEntities adventureWorks_DataEntities = new AdventureWorks_DataEntities();

            foreach (var employee in adventureWorks_DataEntities.Employees)
            {
                ListItem li = new ListItem();

                li.Text = employee.EmployeeID + " ";

                if (!employee.EmployeePayHistories.IsLoaded)
                {
                    employee.EmployeePayHistories.Load();
                }

                foreach (var pay in employee.EmployeePayHistories)
                {
                    li.Text += "Pay Rate: " + pay.Rate + " ";
                }

                BulletedList1.Items.Add(li);
            }

        }
    }
}
