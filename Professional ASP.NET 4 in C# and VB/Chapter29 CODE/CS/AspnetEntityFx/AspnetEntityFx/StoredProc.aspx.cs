﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;

namespace AspnetEntityFx
{
    public partial class StoredProc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NORTHWNDEntities northwndEntities = new NORTHWNDEntities();
            EntityConnection entityConnection =
               (EntityConnection)northwndEntities.Connection;

            DbConnection storeConnection =
               entityConnection.StoreConnection;
            DbCommand command = storeConnection.CreateCommand();
            command.CommandText = "Ten Most Expensive Products";
            command.CommandType = CommandType.StoredProcedure;

            bool openConnection = command.Connection.State ==
               ConnectionState.Closed;

            if (openConnection)
            {
                command.Connection.Open();
            }

            var result = command.ExecuteReader();

            GridView1.DataSource = result;
            GridView1.DataBind();

        }
    }
}
