﻿using System;
using System.Linq;

namespace AspnetEntityFx
{
    public partial class Inheritance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdventureWorks_DataEntities1 adventureWorks_DataEntities =
                new AdventureWorks_DataEntities1();

            var query = from c in adventureWorks_DataEntities.Customers
                        where c is IndividualCustomer
                        select c;

            GridView1.DataSource = query;
            GridView1.DataBind();

        }
    }
}
