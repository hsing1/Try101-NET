﻿using System;
using System.Linq;

namespace AspnetEntityFx
{
    public partial class BasicGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdventureWorks_DataEntities adventureWorks_DataEntities =
                new AdventureWorks_DataEntities();

            var query = from emp in adventureWorks_DataEntities.Employees
                        select emp;

            GridView1.DataSource = query;
            GridView1.DataBind();
        }
    }
}
