<configuration>

   <system.web>

        <authentication mode="Forms" />

        <membership>
          <providers>
            <clear />
            <add name="AspNetSqlMembershipProvider"  
             type="System.Web.Security.SqlMembershipProvider, System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
             connectionStringName="LocalSqlServer" 
             enablePasswordRetrieval="false" 
             enablePasswordReset="true" 
             requiresQuestionAndAnswer="true" 
             requiresUniqueEmail="false" 
             passwordFormat="Hashed" 
             maxInvalidPasswordAttempts="5" 
             minRequiredPasswordLength="4" 
             minRequiredNonalphanumericCharacters="0" 
             passwordAttemptWindow="10" />
          </providers>
        </membership>

   </system.web>

</configuration>
