Public Overrides Sub AddUsersToRoles(ByVal usernames() As String, _
  ByVal roleNames() As String)
   
   For Each roleItem As String In roleNames
      If roleItem = "Administrator" Then
         Throw New _
            ProviderException("You are not authorized to add any users" & _
               " to the Administrator role")
      End If
   Next

   MyBase.AddUsersToRoles(usernames, roleNames)
End Sub
