Public Overrides Function ValidateUser(ByVal username As String, _
  ByVal password As String) As Boolean

   If (String.IsNullOrEmpty(username) Or String.IsNullOrEmpty(password)) Then
      Return False
   End If

   Try
      ReadUserFile()

      Dim mu As MembershipUser

      If (_MyUsers.TryGetValue(username.ToLower(), mu)) Then
         If (mu.Comment = password) Then
            Return True
         End If
      End If

      Return False
   Catch ex As Exception
      Throw New Exception(ex.Message.ToString())
   End Try
End Function
