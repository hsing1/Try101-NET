﻿<%@ Page Language="C#" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        Meta1.Attributes["Name"] = "description";
        Meta1.Attributes["CONTENT"] = "Generated on: " + DateTime.Now.ToString();
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Using the HtmlGenericControl class</title>
    <meta id="Meta1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        The rain in Spain stays mainly in the plains.
    </div>
    </form>
</body>
</html>
