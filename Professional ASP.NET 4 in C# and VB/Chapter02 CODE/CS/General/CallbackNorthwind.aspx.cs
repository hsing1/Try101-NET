﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class CallbackNorthwind : System.Web.UI.Page,
   System.Web.UI.ICallbackEventHandler
{
    private string _callbackResult = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        string cbReference = Page.ClientScript.GetCallbackEventReference(this,
           "arg", "GetCustDetailsFromServer", "context");
        string cbScript = "function UseCallback(arg, context)" +
           "{" + cbReference + ";" + "}";

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
           "UseCallback", cbScript, true);
    }

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return _callbackResult;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        // Put in your own connection string to make this work
        SqlConnection conn = new
           SqlConnection("Data Source=.;Initial Catalog=Northwind;User ID=sa");
        SqlCommand cmd = new
           SqlCommand("Select * From Customers Where CustomerID = '" +
           eventArgument + "'", conn);

        conn.Open();

        SqlDataReader MyReader;
        MyReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

        string[] MyValues = new string[11];

        while (MyReader.Read())
        {
            MyValues[0] = MyReader["CustomerID"].ToString();
            MyValues[1] = MyReader["CompanyName"].ToString();
            MyValues[2] = MyReader["ContactName"].ToString();
            MyValues[3] = MyReader["ContactTitle"].ToString();
            MyValues[4] = MyReader["Address"].ToString();
            MyValues[5] = MyReader["City"].ToString();
            MyValues[6] = MyReader["Region"].ToString();
            MyValues[7] = MyReader["PostalCode"].ToString();
            MyValues[8] = MyReader["Country"].ToString();
            MyValues[9] = MyReader["Phone"].ToString();
            MyValues[10] = MyReader["Fax"].ToString();
        }

        _callbackResult = String.Join("|", MyValues);
    }

    #endregion
}
