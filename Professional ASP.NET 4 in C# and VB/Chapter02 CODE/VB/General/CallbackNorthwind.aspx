﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallbackNorthwind.aspx.vb" Inherits="CallbackNorthwind" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Details</title>
    
    <script type="text/javascript">
        function GetCustomer(){     
            var customerCode = document.forms[0].TextBox1.value;   
            UseCallback(customerCode, "");
        }
        
        function GetCustDetailsFromServer(result, context){
            var i = result.split("|"); 
            customerID.innerHTML = i[0];
            companyName.innerHTML = i[1];
            contactName.innerHTML = i[2];
            contactTitle.innerHTML = i[3];
            address.innerHTML = i[4];
            city.innerHTML = i[5];
            region.innerHTML = i[6];
            postalCode.innerHTML = i[7];
            country.innerHTML = i[8];
            phone.innerHTML = i[9];
            fax.innerHTML = i[10];
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;
        <input id="Button1" type="button" value="Get Customer Details" 
         onclick="GetCustomer()" /><br />
        <br />
      <table cellspacing="0" cellpadding="4" rules="all" border="1"    
       id="DetailsView1" 
       style="background-color:White;border-color:#3366CC;border-width:1px;
          border-style:None;height:50px;width:400px;border-collapse:collapse;">
        <tr style="color:#003399;background-color:White;">
           <td>CustomerID</td><td><span id="customerID" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>CompanyName</td><td><span id="companyName" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>ContactName</td><td><span id="contactName" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>ContactTitle</td><td><span id="contactTitle" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>Address</td><td><span id="address" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>City</td><td><span id="city" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>Region</td><td><span id="region" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>PostalCode</td><td><span id="postalCode" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>Country</td><td><span id="country" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>Phone</td><td><span id="phone" /></td>
        </tr><tr style="color:#003399;background-color:White;">
           <td>Fax</td><td><span id="fax" /></td>
        </tr>
      </table>    
    </div>
    </form>
</body>
</html>

