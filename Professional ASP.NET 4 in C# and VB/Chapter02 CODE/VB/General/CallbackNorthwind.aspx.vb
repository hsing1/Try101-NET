﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CallbackNorthwind
    Inherits System.Web.UI.Page
    Implements System.Web.UI.ICallbackEventHandler

    Dim _callbackResult As String = Nothing

    Public Function GetCallbackResult() As String 
       Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
        Return _callbackResult
    End Function

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) 
       Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
        ' Please modify the connection string to make this work
        Dim conn As SqlConnection = New  _
           SqlConnection("Data Source=.\SQLEXPRESS;AttachDbFilename='C:\Users\Bill\Documents\Wiley\Professional ASP.NET 3.5\Final Code\Chapter 02\VB\General\App_Data\NORTHWND.MDF';Integrated Security=True;User Instance=True")
        Dim cmd As SqlCommand = New  _
           SqlCommand("Select * From Customers Where CustomerID = '" & 
           eventArgument & "'", conn)

        conn.Open()

        Dim MyReader As SqlDataReader
        MyReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Dim MyValues(10) As String

        While MyReader.Read()
            MyValues(0) = MyReader("CustomerID").ToString()
            MyValues(1) = MyReader("CompanyName").ToString()
            MyValues(2) = MyReader("ContactName").ToString()
            MyValues(3) = MyReader("ContactTitle").ToString()
            MyValues(4) = MyReader("Address").ToString()
            MyValues(5) = MyReader("City").ToString()
            MyValues(6) = MyReader("Region").ToString()
            MyValues(7) = MyReader("PostalCode").ToString()
            MyValues(8) = MyReader("Country").ToString()
            MyValues(9) = MyReader("Phone").ToString()
            MyValues(10) = MyReader("Fax").ToString()
        End While

        conn.Close()

        _callbackResult = String.Join("|", MyValues)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, 
       ByVal e As System.EventArgs) Handles Me.Load
        Dim cbReference As String = 
           Page.ClientScript.GetCallbackEventReference(Me, "arg", 
              "GetCustDetailsFromServer", "context")
        Dim cbScript As String = "function UseCallback(arg, context)" & 
           "{" & cbReference & ";" & "}"

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), 
           "UseCallback", cbScript, True)
    End Sub
End Class

