﻿Partial Class RandomNumber
    Inherits System.Web.UI.Page
    Implements System.Web.UI.ICallbackEventHandler

    Dim _callbackResult As String = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) 
       Handles Me.Load

        Dim cbReference As String = Page.ClientScript.GetCallbackEventReference( 
           Me, "arg", "GetRandomNumberFromServer", "context")
        Dim cbScript As String = "function UseCallback(arg, context)" & 
           "{" & cbReference & ";" & "}"

        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), 
           "UseCallback", cbScript, True)
    End Sub

    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) 
       Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        _callbackResult = Rnd().ToString()
    End Sub

    Public Function GetCallbackResult() As String 
       Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult

        Return _callbackResult
    End Function
End Class
