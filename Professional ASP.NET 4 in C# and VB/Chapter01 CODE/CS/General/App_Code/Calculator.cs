﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Calculator
/// </summary>
public class Calculator
{
    public int Add(int a, int b)
    {
        return (a + b);
    }

    public int Subtract(int a, int b)
    {
        return (a - b);
    }
}
