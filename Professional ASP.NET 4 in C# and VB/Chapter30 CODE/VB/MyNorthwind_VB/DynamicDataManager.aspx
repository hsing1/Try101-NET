<%@ Page Language="VB" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DynamicDataManager Example</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AutoGenerateColumns="True" DataKeyNames="CustomerID" 
            DataSourceID="LinqDataSource1">
        </asp:GridView>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
            ContextTypeName="NorthwindDataContext" EntityTypeName="" 
            OrderBy="CompanyName" 
            TableName="Customers">
        </asp:LinqDataSource>
        <asp:DynamicDataManager ID="DynamicDataManager1" 
         runat="server">
            <DataControls>
                <asp:DataControlReference ControlID="GridView1" />
            </DataControls>
        </asp:DynamicDataManager>
    
    </div>
    </form>
</body>
</html>
