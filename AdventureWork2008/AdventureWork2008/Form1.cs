﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdventureWork2008
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 這行程式碼會將資料載入 'northwindDataSet1.Customers' 資料表。您可以視需要進行移動或移除。
            this.customersTableAdapter.Fill(this.northwindDataSet1.Customers);
            // TODO: 這行程式碼會將資料載入 'adventureWorks2008DataSet.Person' 資料表。您可以視需要進行移動或移除。
            this.personTableAdapter.Fill(this.adventureWorks2008DataSet.Person);

        }
    }
}
