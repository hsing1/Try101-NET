﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TryWindowsForms
{
    public static class MyTimer
    {
        public static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public static int alarmCounter = 1;
        public static bool exitFlag = false;

        // This is the method to run when the timer is raised.
        public static void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            myTimer.Stop();

            // Displays a message box asking whether to continue running the timer.
            if (MessageBox.Show("Continue running?", "Count is: " + alarmCounter,
               MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Restarts the timer and increments the counter.
                alarmCounter += 1;
                myTimer.Enabled = true;
            }
            else
            {
                // Stops the timer.
                exitFlag = true;
            }

        }
    }
}