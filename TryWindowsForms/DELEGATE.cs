﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TryWindowsForms
{
    public delegate string GetSecretIngredient(int amount);

    class DELEGATE
    {
        public static string SecretMethod(int amount)
        {
            Console.WriteLine("Suzanne's method " + amount);
            return "Down";
        }

        public static string SecretMethod2(int amount)
        {
            Console.WriteLine("Suzanne's method2 " + (amount + 2));
            return "Down";
        }
    }

    class BallEventArgs : EventArgs
    {
        public int Trajectory { get; private set; }
        public int Distance { get; private set; }
        public BallEventArgs (int Trajectory, int Distance)
        {
            this.Trajectory = Trajectory;
            this.Distance = Distance;
        }
    }

    class Ball
    {
        public event EventHandler BallInPlay; //when a Ball gets hit, it raises a  BallInPlay event
        // 事件通常為公用的，因此標示為 public
        // event 為關鍵字，一般 delegate 可為區域變數或成員變數，delegate 呼叫前若未指定任何方法，則會造成編譯錯誤，且第一次指定方法不可使用「+=」
        // 標示為 event 者僅能是成員變數，呼叫前若未指定任何方法，則僅在執行期間拋出例外，並且第一次指定方法可使用「+=」
        // EventHandler 則為 delegate 定義：  public delegate void EventHandler(object sender, EventArgs e);

        public Ball()
        {
            int i = 1;
        }

        public void OnBallInPlay(BallEventArgs e)  //when the ball gets hit, OnBallInPlay gets called, it raise event BallInPlay
        {
            if (BallInPlay != null)
            {
                BallInPlay(this, e);  // 觸發 BallInPlay 事件, 2 parameter
            }
        }

        public void OnBallInPlay(int trajectory, int distance)
        {

        }
    }

    class Pitcher
    {
        //Ball ball;

        public Pitcher(Ball ball)
        {
            //this.ball = ball;
            //ball.BallInPlay += new EventHandler(ball_BallInPlay); // 訂閱 BallInPlay 事件
            ball.BallInPlay += ball_BallInPlay;
        }

        void ball_BallInPlay(object sender, EventArgs e) // BallInPlay 事件處理方法
        {
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if ((ballEventArgs.Distance < 95) && (ballEventArgs.Trajectory < 60))
                    Catch();
                else
                    CoverFirstBase();
            }
        }

        void Catch()
        {
            Console.WriteLine("Pitcher: I caugth the ball");
        }

        void CatchBall(int trajectory, int distance)
        {

        }

        void CoverFirstBase()
        {
            Console.WriteLine("Pitcher : I cover first base");
        }
    }

    class Fan
    {
        public Fan(Ball ball)
        {
            ball.BallInPlay += ball_BallInPlay;
        }

        void ball_BallInPlay(object sender, EventArgs e)
        {
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if (ballEventArgs.Distance > 400 && ballEventArgs.Trajectory > 30)
                    Console.WriteLine("Fan : Home run! I'am going for the ball!");
                else
                    Console.WriteLine("Fan : Woo-hoo! Yeah!");
            }
        }
    }


    public delegate bool Predicate(string s);

    public class StringList
    {
        private ArrayList strings;

        public StringList()
        {
            strings = new ArrayList();
            strings.Add("Banana");
            strings.Add("Apple");
            strings.Add("Manago");
        }

        public string Find(Predicate p)
        {
            for (int i = 0; i < strings.Count; i++)
            {
                string s = (string)strings[i];
                bool isMatch = p(s);
                if (isMatch)
                {
                    return s;
                }
            }
            return "";
        }
    } 
}
