﻿using System;

namespace TryWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.playBallButton = new System.Windows.Forms.Button();
            this.trajectory = new System.Windows.Forms.NumericUpDown();
            this.distance = new System.Windows.Forms.NumericUpDown();
            this.trajectoryLabel = new System.Windows.Forms.Label();
            this.distanceLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trajectory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.distance)).BeginInit();
            this.SuspendLayout();
            // 
            // playBallButton
            // 
            this.playBallButton.Location = new System.Drawing.Point(87, 211);
            this.playBallButton.Name = "playBallButton";
            this.playBallButton.Size = new System.Drawing.Size(75, 23);
            this.playBallButton.TabIndex = 0;
            this.playBallButton.Text = "Play ball!";
            this.playBallButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.playBallButton.UseVisualStyleBackColor = true;
            this.playBallButton.Click += new System.EventHandler(this.playBallButton_Click);
            // 
            // trajectory
            // 
            this.trajectory.Location = new System.Drawing.Point(87, 40);
            this.trajectory.Name = "trajectory";
            this.trajectory.Size = new System.Drawing.Size(120, 22);
            this.trajectory.TabIndex = 1;
            // 
            // distance
            // 
            this.distance.Location = new System.Drawing.Point(87, 100);
            this.distance.Name = "distance";
            this.distance.Size = new System.Drawing.Size(120, 22);
            this.distance.TabIndex = 2;
            // 
            // trajectoryLabel
            // 
            this.trajectoryLabel.AutoSize = true;
            this.trajectoryLabel.Location = new System.Drawing.Point(26, 40);
            this.trajectoryLabel.Name = "trajectoryLabel";
            this.trajectoryLabel.Size = new System.Drawing.Size(53, 12);
            this.trajectoryLabel.TabIndex = 3;
            this.trajectoryLabel.Text = "Trajectory";
            // 
            // distanceLabel
            // 
            this.distanceLabel.AutoSize = true;
            this.distanceLabel.Location = new System.Drawing.Point(28, 109);
            this.distanceLabel.Name = "distanceLabel";
            this.distanceLabel.Size = new System.Drawing.Size(44, 12);
            this.distanceLabel.TabIndex = 4;
            this.distanceLabel.Text = "Distance";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.distanceLabel);
            this.Controls.Add(this.trajectoryLabel);
            this.Controls.Add(this.distance);
            this.Controls.Add(this.trajectory);
            this.Controls.Add(this.playBallButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trajectory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.distance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button playBallButton;
        private System.Windows.Forms.NumericUpDown trajectory;
        private System.Windows.Forms.NumericUpDown distance;
        private System.Windows.Forms.Label trajectoryLabel;
        private System.Windows.Forms.Label distanceLabel;
    }
}

