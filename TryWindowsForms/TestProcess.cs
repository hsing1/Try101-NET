﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace TryWindowsForms
{
    public partial class TestProcess : Form
    {
        public TestProcess()
        {
            InitializeComponent();
        }

        private void btnOpenNotepad_Click(object sender, EventArgs e)
        {
            Process notePad = new Process();
            notePad.StartInfo.FileName = "notepad.exe";
            notePad.Start();
        }

        private void btnCloseNotePad_Click(object sender, EventArgs e)
        {
            Process[] process = Process.GetProcessesByName("NotePad");
            foreach(Process p in process)
            {
                p.WaitForExit(1000);
                p.CloseMainWindow();
            }
        }

        private void btnOpenBrowser_Click(object sender, EventArgs e)
        {
            Process exploer = new Process();
            exploer.StartInfo.FileName = "exploer.exe";
            exploer.StartInfo.Arguments = "http://mam.pts.org.tw";
            exploer.Start();
        }

        private void btnListProcess_Click(object sender, EventArgs e)
        {
            string s = null;
            DateTime t;
            listBox1.Items.Clear();

            Process[] processes = Process.GetProcesses();

            foreach(Process p in processes)
            {
                if (!p.ProcessName.Equals("Idle"))
                {
                    s = p.ProcessName;
                    t = p.StartTime;
                    listBox1.Items.Add(string.Format("{0} - {1}", p.ProcessName, p.StartTime.ToString("yyyy/MM/dd hh:mm:ss")));
                }
            }
        }
    }
}
