﻿namespace TryWindowsForms
{
    partial class TestProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnOpenNotepad = new System.Windows.Forms.Button();
            this.btnCloseNotePad = new System.Windows.Forms.Button();
            this.btnOpenBrowser = new System.Windows.Forms.Button();
            this.btnListProcess = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(12, 13);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(407, 208);
            this.listBox1.TabIndex = 0;
            // 
            // btnOpenNotepad
            // 
            this.btnOpenNotepad.Location = new System.Drawing.Point(53, 251);
            this.btnOpenNotepad.Name = "btnOpenNotepad";
            this.btnOpenNotepad.Size = new System.Drawing.Size(113, 23);
            this.btnOpenNotepad.TabIndex = 1;
            this.btnOpenNotepad.Text = "Open Notepad";
            this.btnOpenNotepad.UseVisualStyleBackColor = true;
            this.btnOpenNotepad.Click += new System.EventHandler(this.btnOpenNotepad_Click);
            // 
            // btnCloseNotePad
            // 
            this.btnCloseNotePad.Location = new System.Drawing.Point(230, 251);
            this.btnCloseNotePad.Name = "btnCloseNotePad";
            this.btnCloseNotePad.Size = new System.Drawing.Size(110, 23);
            this.btnCloseNotePad.TabIndex = 2;
            this.btnCloseNotePad.Text = "Close Notepad";
            this.btnCloseNotePad.UseVisualStyleBackColor = true;
            this.btnCloseNotePad.Click += new System.EventHandler(this.btnCloseNotePad_Click);
            // 
            // btnOpenBrowser
            // 
            this.btnOpenBrowser.Location = new System.Drawing.Point(53, 310);
            this.btnOpenBrowser.Name = "btnOpenBrowser";
            this.btnOpenBrowser.Size = new System.Drawing.Size(113, 23);
            this.btnOpenBrowser.TabIndex = 3;
            this.btnOpenBrowser.Text = "Open Browser";
            this.btnOpenBrowser.UseVisualStyleBackColor = true;
            this.btnOpenBrowser.Click += new System.EventHandler(this.btnOpenBrowser_Click);
            // 
            // btnListProcess
            // 
            this.btnListProcess.Location = new System.Drawing.Point(230, 310);
            this.btnListProcess.Name = "btnListProcess";
            this.btnListProcess.Size = new System.Drawing.Size(110, 23);
            this.btnListProcess.TabIndex = 4;
            this.btnListProcess.Text = "List Process";
            this.btnListProcess.UseVisualStyleBackColor = true;
            this.btnListProcess.Click += new System.EventHandler(this.btnListProcess_Click);
            // 
            // TestProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 381);
            this.Controls.Add(this.btnListProcess);
            this.Controls.Add(this.btnOpenBrowser);
            this.Controls.Add(this.btnCloseNotePad);
            this.Controls.Add(this.btnOpenNotepad);
            this.Controls.Add(this.listBox1);
            this.Name = "TestProcess";
            this.Text = "TestProcess";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnOpenNotepad;
        private System.Windows.Forms.Button btnCloseNotePad;
        private System.Windows.Forms.Button btnOpenBrowser;
        private System.Windows.Forms.Button btnListProcess;
    }
}