﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TryWindowsForms.MyServiceTSM;

namespace TryWindowsForms
{
    public class TRY101
    {
        public static int Main()
        {
            int tryCase = 6;

            if (tryCase == 1) //Timer
            {
                /* Adds the event and the event handler for the method that will 
                   process the timer event to the timer. */
                MyTimer.myTimer.Tick += new EventHandler(MyTimer.TimerEventProcessor);


                // Sets the timer interval to 5 seconds.
                MyTimer.myTimer.Interval = 5000;
                MyTimer.myTimer.Start();

                // Runs the timer, and raises the event.
                while (MyTimer.exitFlag == false)
                {
                    // Processes all the events in the queue.
                    Application.DoEvents();
                }
                return 0;
            }
            else if (tryCase == 2) //Timer
            {
                TimerForm t = new TimerForm();
                t.myTimer.Tick += new EventHandler(t.timer1_Tick);

                t.myTimer.Interval = 2000;
                t.myTimer.Start();

                while (t.exitFlag == false)
                {
                    Application.DoEvents();
                }
            }
            else if (tryCase == 3) //Delegate
            {
                GetSecretIngredient MySecretMethod = new GetSecretIngredient(DELEGATE.SecretMethod);
                MySecretMethod(5);
                MySecretMethod += DELEGATE.SecretMethod2;
                MySecretMethod(5);
            }
            else if (tryCase == 4) //Head first in C#, Ch11 Events and delegates
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            else if (tryCase == 5) //split()
            {
                String path = "\\\\mamstream\\MAMDFS\\hakkatv\\HVideo\\2016\\20160700490\\P201607004900001.mxf";
                char delimiter = '\\';
                String s;
                String TSMPath = "";
                String[] substrings = path.Split(delimiter);
                foreach (var str in substrings)
                    Console.WriteLine(str);

                for (var i = 4; i < substrings.Length; i++)
                {
                    TSMPath += substrings[i] + "/";
                }
                TSMPath = "/" + TSMPath.Substring(0, TSMPath.Length - 1);
                String file = "G201500300010001";
                s = file.Substring(1, 4);
                Console.WriteLine(s);

                MyServiceTSM.ServiceTSMSoapClient tsm = new MyServiceTSM.ServiceTSMSoapClient();
                int ret = (int)tsm.GetFileStatusByTsmPath(TSMPath);
            }
            else if (tryCase ==6) //Test System.Diagnostics.Process
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new TestProcess());
            }
            return 0;
        }

    }
}

