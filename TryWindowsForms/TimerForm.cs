﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TryWindowsForms
{
    public partial class TimerForm : Form
    {
        public System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        public int alarmCounter = 0;
        public bool exitFlag = false;
        public TimerForm()
        {
            InitializeComponent();
        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            if (MessageBox.Show("繼續執行?", "Count is: " + alarmCounter,
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Restarts the timer and increments the counter.
                alarmCounter += 1;
                myTimer.Enabled = true;
            }
            else
            {
                // Stops the timer.
                exitFlag = true;
            }
        }
    }
}
