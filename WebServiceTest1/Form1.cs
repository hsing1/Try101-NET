﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebServiceTest1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MyWebService.GetCustomersSoapClient ws = new MyWebService.GetCustomersSoapClient();
            textBox1.Text = ws.HelloWorld();
            UserInfoService.wsUserDataSoapClient us = new UserInfoService.wsUserDataSoapClient("wsUserDataSoap");
            textBox2.Text = us.GetExists("51204");
        }
    }
}
