﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AboutDoEvents
{
    public partial class DoEventsForm1 : Form
    {

        private System.Windows.Forms.OpenFileDialog openFileDialog1 = null;
        private PictureBox pictureBox2 = null;
        private Button btn = null;

        public DoEventsForm1()
        {
            InitializeComponent();
            InitiallizeOpenFileDialog();
            InitializePictureBox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            System.Windows.Forms.OpenFileDialog ofdialog = new System.Windows.Forms.OpenFileDialog();
            ofdialog.Title = "Select File";
            ofdialog.InitialDirectory = "c:\\";
            //ofdialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //ofdialog.Filter = "*.txt";
            ofdialog.Filter = "蚊子檔(*.txxt)|*.txt|所有檔(<<**>> | *.*| DLL 檔|*.dll";
            ofdialog.RestoreDirectory = true;

            if(ofdialog.ShowDialog() == DialogResult.OK)
            { 
                try
                { 
                    if ((myStream = ofdialog.OpenFile()) != null)
                    { 
                        using (myStream)
                        { // Insert code to read the stream here. 
                        }
                    }
                }
                catch (Exception ex)
                { 
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

        }

        private void InitializeButton()
        {
            this.btn.Location = new System.Drawing.Point(126, 191);
            this.btn.Name = "filebutton";
            this.btn.Size = new System.Drawing.Size(75, 23);
            this.btn.TabIndex = 2;
            this.btn.Text = "fileButtonaaaaaaa";
            this.btn.UseVisualStyleBackColor = true;
            //this.btn.Click += new System.EventHandler(this.btn_Click);
        }

        private void InitiallizeOpenFileDialog()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog1.Filter = "Images(*.BMP;*JPG;*.GIF)|*.BMP;*JPG;*.GIF" + "|All files(*.*)|*.*";

            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "My Image Browser";
        }

        private void InitializePictureBox()
        {
            this.pictureBox2 = new PictureBox();
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox2.Location = new System.Drawing.Point(2, 12);
            this.pictureBox2.Name = "pictureBox1";
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
        }


        private void filebutton_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileOk += openFileDialog1_FileOk;
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Activate();
            string[] files = openFileDialog1.FileNames;

            foreach (string file in files)
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(file);
                System.IO.FileStream fileStream = fileInfo.OpenRead();
                pictureBox1.Image = System.Drawing.Image.FromStream(fileStream);
                Application.DoEvents();
                fileStream.Close();

                System.Threading.Thread.Sleep(2000);

                
            }
            pictureBox1.Image = null;
        }
    }
}
