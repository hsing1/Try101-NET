﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackgroundWorkerDemo
{
    public partial class Form1 : Form
    {
        private BackgroundWorker bw;

        public Form1()
        {
            InitializeComponent();
            initBackgroundWorker();
        }

        private void initBackgroundWorker()
        {
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true; //設定是否可以接收進度報告
            bw.WorkerSupportsCancellation = true; //設定是否支援非同步取消
            bw.DoWork += new DoWorkEventHandler(bw_DoWork); //要在背景作業的程式放在DoWork事件的處理函式裡。需注意的是，在DoWork事件的處理函式裡不能有任何和UI元件的互動
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged); //處理進度的顯示。可以和UI元件互動
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted); //執行完成。可以和UI元件互動
        }

        //背景執行
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                if (bw.CancellationPending == true) {
                    e.Cancel = true;
                    break;
                } else
                {
                    // 使用sleep模擬運算時的停頓
                    System.Threading.Thread.Sleep(500);
                    bw.ReportProgress(i * 10);
                }
            }
        }

        //處理進度
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            this.lblMsg.Text = e.ProgressPercentage.ToString();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
                this.lblMsg.Text = "取消！";
            else if (!(e.Error == null))
                this.lblMsg.Text = ("Error: " + e.Error.Message);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (bw.IsBusy != true)
            {
                this.lblMsg.Text = "開始";
                this.progressBar1.Value = 0;
                bw.RunWorkerAsync(); //啟動背景執行時呼叫
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (bw.WorkerSupportsCancellation == true)
                bw.CancelAsync(); //取消背景執行
        }
    }


}
