﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Xml;
using System.Text;
using System.IO;

namespace PTSProgram
{
    /// <summary>
    /// wsUserData 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    [System.Web.Script.Services.ScriptService]
    public class wsUserData : System.Web.Services.WebService
    {
        const string EMPID_DELIMITER = " ";

        struct ResponseData
        {
            public string Code;
            public string Description;
        }
        const string ERRID_INVALID_USER = "1";
        const string ERRID_NO_ADM = "2";
        const string ERRID_UNKNOWN = "9";
        ResponseData R_Success = new ResponseData { Code = ConfigurationManager.AppSettings["SuccessCode"], Description = "成功" };
        ResponseData R_InvalidUser = new ResponseData { Code = ERRID_INVALID_USER, Description = "非系統使用者" };
        ResponseData R_NoAdmin = new ResponseData { Code = ERRID_NO_ADM, Description = "找不到系統管理員" };
        ResponseData R_Unknown = new ResponseData { Code = ERRID_UNKNOWN, Description = "不明原因" };

        class clsPTSPRogramTable
        {
            public const string Users = "TBUSERS";
            public const string Permission = "TBPERMISSION";
            public const string Administrators = "tblAdministrators";
        }

        [WebMethod]
        public string GetExists(string pstrEmpID)
        {
            //輸出資料:
            //1: .RetID() '0' 表示成功；非 '0' 表示失敗 (請自行定義，原因放置於 ErrMsg 中)
            //2: .ErrMsg() : 錯誤訊息()
            //3: .SysName() : 中文系統名稱()
            //4: .AdmEmpIDs() : 系統管理員的員工編號, 可為多人
            string myReturnCode = R_Unknown.Code;
            string myErrMessage = R_Unknown.Description;
            string myAdmEmpIDs = "";

            string myConnStr = ConfigurationManager.ConnectionStrings["PTSProgram-User"].ToString();
            string myUserTable = clsPTSPRogramTable.Users;
            string myPermissionTable = clsPTSPRogramTable.Permission;
            string myAdmTable = clsPTSPRogramTable.Administrators;

            using (SqlConnection conn = new SqlConnection(myConnStr))
            {
                //無任何權限者視為非使用者
                SqlCommand cmd = new SqlCommand("SELECT FSUSERID FROM " + myUserTable 
                                            + " WHERE FSEMPNO = @EmpID"
                                            + " AND FSUSERID IN (SELECT FSUSERID FROM " + myPermissionTable + ")", conn);
                cmd.Parameters.Add("@EmpID", SqlDbType.VarChar);
                cmd.Parameters["@EmpID"].Value = pstrEmpID;

                try
                {
                    conn.Open();

                    SqlDataReader reader = cmd.ExecuteReader();
                    if  (!reader.HasRows)
                    {
                        myReturnCode = R_InvalidUser.Code;
                        myErrMessage = R_InvalidUser.Description;
                    }
                    else
                    {
                        reader.Close();

                        cmd.CommandText = "SELECT EmpID FROM " + myAdmTable;
                        reader = cmd.ExecuteReader();
                        if (!reader.HasRows)
                        {
                            myReturnCode = R_NoAdmin.Code;
                            myErrMessage = R_NoAdmin.Description;
                        }
                        else
                        {
                            while (reader.Read())
                            {
                                myAdmEmpIDs += ((myAdmEmpIDs == "") ? "" : EMPID_DELIMITER) + reader.GetString(0);
                            }
                            myReturnCode = R_Success.Code;
                            myErrMessage = R_Success.Description;
                        };
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb, settings);
            writer.WriteStartDocument();
            writer.WriteStartElement("root");

            writer.WriteStartElement("RetID");
            writer.WriteString(myReturnCode);
            writer.WriteEndElement();

            writer.WriteStartElement("ErrMsg");
            writer.WriteString(myErrMessage);
            writer.WriteEndElement();

            writer.WriteStartElement("SysName");
            writer.WriteString(ConfigurationManager.AppSettings["SystemName"]);
            writer.WriteEndElement();

            writer.WriteStartElement("AdmEmpIDs");
            writer.WriteString(myAdmEmpIDs);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            //XmlDocument myDoc = new XmlDocument();
            //myDoc.LoadXml(sb.ToString());
            return sb.ToString();
        }
    }
}
