﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Xml;
using System.Text;

namespace PTSProgram
{
    /// <summary>
    /// wsExpenseApprove 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class wsExpenseApprove : System.Web.Services.WebService
    {
        class clsECPEvent
        {
            public const string ApproveByDepartment = "1";      //部門會計核可
            public const string ApproveByAdm = "2";             //行政部會計核可
            public const string DisapproveByDepartment = "3";   //部門會計駁回
            public const string DisapproveByAdm = "4";          //行政部會計駁回
            public const string Withdraw = "9";                 //抽單
        }
        class clsMode
        {
            public const string CheckOnly = "1";    //僅檢查不異動資料
            public const string UpdateAtOnce = "2"; //異動資料
        }
        class clsExpenseStatus
        {
            public const string Applied = "N";              //未審
            public const string PreApprovalWithdraw = "F";  //取消初核
            public const string PreApproved = "P";          //初核
            public const string ApprovalWithdraw = "V";     //取消核准
            public const string Approved = "A";            //核准
            public const string AppliedString = "未審";
            public const string PreApprovalString = "初核";
            public const string ApprovedString = "核准";
            public const string CancelPreApprovalString = "取消初核";
            public const string CancelApprovedString = "取消核准";
            public const string UnknowString = "不明";
        }
        class clsExpenseStatusSQL
        {
            public const string AppliedCondition = "FCSTATUS IN ('" + clsExpenseStatus.Applied + "', '" + clsExpenseStatus.PreApprovalWithdraw + "')";        //未審 = 未審 + 取消初核
            public const string PreApprovedCondition = "FCSTATUS IN ('" + clsExpenseStatus.PreApproved + "', '" + clsExpenseStatus.ApprovalWithdraw + "')";   //初核 = 初核 + 取消核准
            public const string ApprovedCondition = "FCSTATUS IN ('" + clsExpenseStatus.Approved + "')"; //核准

            public const string SelectSQL = "CASE WHEN " + AppliedCondition + " THEN '" + clsExpenseStatus.AppliedString + "'"
                                           + " WHEN " + PreApprovedCondition + " THEN '" + clsExpenseStatus.PreApprovalString + "'"
                                           + " WHEN " + ApprovedCondition + " THEN '" + clsExpenseStatus.ApprovedString + "'"
                                           + " ELSE '" + clsExpenseStatus.UnknowString + "' END";

            public const string SelectApproveSQL = "CASE FCSTATUS WHEN '" + clsExpenseStatus.Applied + "' THEN '" + clsExpenseStatus.AppliedString + "'"
                                           + " WHEN '" + clsExpenseStatus.PreApproved + "' THEN '" + clsExpenseStatus.PreApprovalString + "'"
                                           + " WHEN '" + clsExpenseStatus.Approved + "' THEN '" + clsExpenseStatus.ApprovedString + "'"
                                           + " WHEN '" + clsExpenseStatus.PreApprovalWithdraw + "' THEN '" + clsExpenseStatus.CancelPreApprovalString + "'"
                                           + " WHEN '" + clsExpenseStatus.ApprovalWithdraw + "' THEN '" + clsExpenseStatus.CancelApprovedString + "'"
                                           + " ELSE '" + clsExpenseStatus.UnknowString + "' END";
        }
        class clsPTSPRogramTable
        {
            public const string Users = "TBUSERS";
            public const string Expense = "TBEXPENSE";
            public const string Expense_History = "TBEXPENSE_H";
            public const string Expense_Adjusting = "tblExpenseAdjusting";
        }

        class clsEZFlowTable
        {
            public const string WorkingExpenseForms = "vwProgram";    //未抽單未結案的憑證黏存單
        }

        struct ResponseData
        {
            public string Code;
            public string Description;
        }
        const string ERRID_NO_FORM = "1";
        const string ERRID_NO_USER = "2";
        const string ERRID_INVALID_EVENT = "3";
        const string ERRID_INVALID_USER = "4";
        const string ERRID_INVALID_MODE = "5";
        const string ERRID_EXPENSE_NOT_FOUND = "6";
        const string ERRID_STATUS = "7";
        const string ERRID_NO_EXPENSE = "8";
        const string ERRID_DUP_FORM_NO = "9";
        const string ERRID_EXPENSE_LINKED = "10";
        const string ERRID_EXPENSE_EZFLOW = "11";
        const string ERRID_EXPENSE_NOT_APPROVED = "12";
        const string ERRID_EXPENSE_NOT_LINKED = "13";
        const string ERRID_EXPENSE_MULTIPLE_LINKED = "14";
        const string ERRID_UNKNOWN = "99";
        ResponseData R_Success = new ResponseData { Code = ConfigurationManager.AppSettings["SuccessCode"], Description = "成功" };
        ResponseData R_NoForm = new ResponseData { Code = ERRID_NO_FORM, Description = "未指定 ECP 表單" };
        ResponseData R_NoUser = new ResponseData { Code = ERRID_NO_USER, Description = "未指定簽核人" };
        ResponseData R_InvalidEvent = new ResponseData { Code = ERRID_INVALID_EVENT, Description = "錯誤的 ECP 事件" };
        ResponseData R_InvalidMode = new ResponseData { Code = ERRID_INVALID_MODE, Description = "錯誤的執行模式" };
        ResponseData R_InvalidUser = new ResponseData { Code = ERRID_INVALID_USER, Description = "簽核人非節目管理系統使用者" };
        ResponseData R_ExpenseNotFound = new ResponseData { Code = ERRID_EXPENSE_NOT_FOUND, Description = "找不到相關的費用報銷紀錄" };
        ResponseData R_StatusError = new ResponseData { Code = ERRID_STATUS, Description = "費用報銷紀錄的目前狀態不允許此動作" };
        ResponseData R_NoExpense = new ResponseData { Code = ERRID_NO_EXPENSE, Description = "未指定費用報銷紀錄" };
        ResponseData R_DuplicateFormNo = new ResponseData { Code = ERRID_DUP_FORM_NO, Description = "指定的 ECP 表單編號屬於其他費用報銷紀錄，無法重複指派" };
        ResponseData R_ExpenseLinked = new ResponseData { Code = ERRID_EXPENSE_LINKED, Description = "費用報銷紀錄已申請過 ECP 憑證黏存單，無法重複申請" };
        ResponseData R_ExpenseEZFlow = new ResponseData { Code = ERRID_EXPENSE_EZFLOW, Description = "費用報銷紀錄已申請過 EZFlow 憑證黏存單，請先抽單再重新申請" };
        ResponseData R_ExpenseNotApproved = new ResponseData { Code = ERRID_EXPENSE_NOT_APPROVED, Description = "費用報銷紀錄未經行政部會計核准，無法重新申請更正錯帳" };
        ResponseData R_ExpenseNotLinked = new ResponseData { Code = ERRID_EXPENSE_NOT_LINKED, Description = "費用報銷紀錄未申請過 ECP 憑證黏存單，無法重新申請更正錯帳" };
        ResponseData R_ExpenseMultipleLinked = new ResponseData { Code = ERRID_EXPENSE_MULTIPLE_LINKED, Description = "指定的費用報銷紀錄原申請的 ECP 憑證黏存單單號並不同，無法重新申請更正錯帳" };
        ResponseData R_Unknown = new ResponseData { Code = ERRID_UNKNOWN, Description = "不明原因" };

        string mstrSysErrMessage = "";

        [WebMethod]
        public string LinkECPForm(string ECPFormNo, string ExpenseIDs, string ProcessMode)
        {
            //輸出資料:
            //RetID：'0' 表示成功；非 '0' 表示失敗 (請自行定義，原因放置於 ErrMsg 中)
            //ErrMsg：錯誤訊息
            //ErrExpenses：錯誤的費用報銷紀錄 (報銷編號)
            string myExpenseIDSQL = "";
            string myReturnCode = R_Unknown.Code;
            string myErrMessage = R_Unknown.Description;
            string myErrExpenses = "";
            string myLinkedExpenses = ""; string myEZFlowExpenses = "";
            string myNotApprovedExpenses = ""; string myNotLinkedExpenses = "";   //錯帳用

            ECPFormNo = ECPFormNo.Trim();
            if (ECPFormNo == "")
            {
                myReturnCode = R_NoForm.Code;
                myErrMessage = R_NoForm.Description;
                goto EndProcess;
            }

            ExpenseIDs = ExpenseIDs.Trim();
            string[] myExpenseIDs = ExpenseIDs.Split(new Char[] { ' ' });
            foreach (string ExpenseID in myExpenseIDs)
            {
                if (ExpenseID.Trim() != "") { myExpenseIDSQL += (myExpenseIDSQL == "" ? "" : ", ") + "'" + ExpenseID.Trim() + "'"; }
            }
            if (myExpenseIDSQL == "")
            {
                myReturnCode = R_NoExpense.Code;
                myErrMessage = R_NoExpense.Description;
                goto EndProcess;
            }

            if (ProcessMode == clsMode.CheckOnly) { }
            else if (ProcessMode == clsMode.UpdateAtOnce) { }
            else
            {
                myReturnCode = R_InvalidMode.Code;
                myErrMessage = R_InvalidMode.Description;
                goto EndProcess;
            }

            string myConnStr = ConfigurationManager.ConnectionStrings["PTSProgram-Expense"].ToString();
            string myEZFlowConnStr = ConfigurationManager.ConnectionStrings["EZFlow"].ToString();
            using (SqlConnection conn = new SqlConnection(myConnStr))
            {
                using (SqlConnection connEZFlow = new SqlConnection(myEZFlowConnStr))
                { 
                    try
                    {
                        conn.Open();

                        // 檢查單號是否已連結報銷紀錄
                        myErrExpenses = "";
                        SqlCommand cmd = new SqlCommand("SELECT FSEXPENSEID FROM " + clsPTSPRogramTable.Expense 
                            + " WHERE ECPFormNo = @ECPFormNo"
                            + " ORDER BY FSEXPENSEID", conn);
                        cmd.Parameters.Add("@ECPFormNo", SqlDbType.VarChar);
                        cmd.Parameters["@ECPFormNo"].Value = ECPFormNo;
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            { 
                                myErrExpenses += (myErrExpenses == "" ? "" : " ") + reader.GetString(0);
                            }
                            myReturnCode = R_DuplicateFormNo.Code;
                            myErrMessage = R_DuplicateFormNo.Description;
                        }
                        reader.Close();

                        if (myErrExpenses == "")
                        {
                            connEZFlow.Open();

                            myLinkedExpenses = "";
                            myEZFlowExpenses = "";
                            string myExpenseID = "";
                            bool myblnAdjustPending = false; bool myblnMultipleLinks = false;
                            cmd.CommandText = "SELECT ISNULL(ECPFormNo, ''), ISNULL(AdjustPending, CONVERT(BIT, 0)),"
                                            + " CASE WHEN " + clsExpenseStatusSQL.ApprovedCondition + " THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END"
                                            + " FROM " + clsPTSPRogramTable.Expense + " WHERE FSEXPENSEID = @ExpenseID";
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@ExpenseID", SqlDbType.VarChar);
                            foreach (string ExpenseID in myExpenseIDs)
                            {
                                myExpenseID = ExpenseID.Trim();
                                if (myExpenseID != "")
                                {
                                    // 檢查費用報銷紀錄是否存在
                                    cmd.Parameters["@ExpenseID"].Value = myExpenseID;

                                    reader = cmd.ExecuteReader();
                                    if (!reader.HasRows)
                                    {
                                        myErrExpenses += (myErrExpenses == "" ? "" : " ") + myExpenseID;
                                    }
                                    else if (reader.Read())
                                    {
                                        // 檢查費用報銷紀錄是否已有 ECP 單號
                                        if (reader.GetString(0) != "")
                                        {
                                            myLinkedExpenses += (myLinkedExpenses == "" ? "" : "\t") + myExpenseID + " " + reader.GetString(0);
                                        }
                                        else
                                        {
                                            myNotLinkedExpenses += (myNotLinkedExpenses == "" ? "" : " ") + myExpenseID;
                                        }
                                        if (reader.GetBoolean(1)) { myblnAdjustPending = true; }
                                        if (! reader.GetBoolean(2))
                                        {
                                            myNotApprovedExpenses += (myNotApprovedExpenses == "" ? "" : " ") + myExpenseID;
                                        }
                                    }
                                    reader.Close();

                                    // 檢查是否有 EZFlow 中未抽單的憑證黏存單
                                    string myEZFlowFormNos = CheckEZFlow(connEZFlow, myExpenseID);
                                    if (myEZFlowFormNos != "")
                                    {
                                        myEZFlowExpenses += (myEZFlowExpenses == "" ? "" : "\t") + myExpenseID + " " + myEZFlowFormNos;
                                    }
                                }
                            }   //foreach (string ExpenseID in myExpenseIDs)
                            
                            if (myblnAdjustPending) // 錯帳
                            {
                                myLinkedExpenses = "";
                                myEZFlowExpenses = "";
                                if (myErrExpenses == "")
                                {
                                    cmd.CommandText = "SELECT COUNT(DISTINCT ECPFormNo) myCount FROM " + clsPTSPRogramTable.Expense + " WHERE FSEXPENSEID IN (" + myExpenseIDSQL + ")";
                                    reader = cmd.ExecuteReader();
                                    reader.Read();
                                    if (reader.GetInt32(0) != 1) { myblnMultipleLinks = true; }
                                    reader.Close();
                                }
                            }
                            else // 非錯帳
                            {
                                myNotLinkedExpenses = "";
                                myNotApprovedExpenses = "";
                            }

                            if ((myLinkedExpenses == "") && (myErrExpenses == "") && (myEZFlowExpenses == "")
                                && (! myblnMultipleLinks) && (myNotLinkedExpenses == "") && (myNotApprovedExpenses == ""))
                            {
                                if (ProcessMode == clsMode.CheckOnly)
                                {
                                    myReturnCode = R_Success.Code;
                                    myErrMessage = R_Success.Description;
                                }
                                else if (UpdateFormNo(conn, ECPFormNo, myExpenseIDSQL, myblnAdjustPending)) // 異動費用報銷單號
                                {
                                    myReturnCode = R_Success.Code;
                                    myErrMessage = R_Success.Description;
                                }
                            }
                            //錯誤狀態一次只回傳一種
                            else if (myErrExpenses != "")
                            {
                                myLinkedExpenses = "";
                                myEZFlowExpenses = "";
                                myNotLinkedExpenses = "";
                                myNotApprovedExpenses = "";
                                myReturnCode = R_ExpenseNotFound.Code;
                                myErrMessage = R_ExpenseNotFound.Description;
                            }
                            else if (myLinkedExpenses != "")
                            {
                                //myErrExpenses = "";
                                myEZFlowExpenses = "";
                                myReturnCode = R_ExpenseLinked.Code;
                                myErrMessage = R_ExpenseLinked.Description;
                            }
                            else if (myEZFlowExpenses != "")
                            {
                                //myLinkedExpenses = "";
                                //myErrExpenses = "";
                                myReturnCode = R_ExpenseEZFlow.Code;
                                myErrMessage = R_ExpenseEZFlow.Description;
                            }
                            else if (myNotApprovedExpenses != "")
                            {
                                myNotLinkedExpenses = "";
                                myblnMultipleLinks = false;
                                myReturnCode = R_ExpenseNotApproved.Code;
                                myErrMessage = R_ExpenseNotApproved.Description;
                            }
                            else if (myNotLinkedExpenses != "")
                            {
                                //myNotApprovedExpenses = "";
                                myblnMultipleLinks = false;
                                myReturnCode = R_ExpenseNotLinked.Code;
                                myErrMessage = R_ExpenseNotLinked.Description;
                            }
                            else if (myblnMultipleLinks)
                            {
                                //myNotLinkedExpenses = "";
                                //myNotApprovedExpenses = "";
                                myReturnCode = R_ExpenseMultipleLinked.Code;
                                myErrMessage = R_ExpenseMultipleLinked.Description;
                            }

                            connEZFlow.Close();
                        }   //if (myErrExpenses != "")
                    }
                    catch (Exception ex)
                    {
                        mstrSysErrMessage = "LinkECPForm(): " + ex.Message;
                    }
                }
            }

        EndProcess:
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb, settings);
            writer.WriteStartDocument();
            writer.WriteStartElement("root");

            writer.WriteStartElement("RetID");
            writer.WriteString(myReturnCode);
            writer.WriteEndElement();

            writer.WriteStartElement("ErrMsg");
            writer.WriteString(myErrMessage);
            writer.WriteEndElement();

            if (((myReturnCode == R_DuplicateFormNo.Code) || (myReturnCode == R_ExpenseNotFound.Code)) && (myErrExpenses != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myErrExpenses.Split(new Char[] { ' ' });
                foreach (string expID in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");
                    writer.WriteAttributeString("報銷編號", expID);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_ExpenseLinked.Code) && (myLinkedExpenses != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myLinkedExpenses.Split(new Char[] { '\t' });
                foreach (string s in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");

                    string[] myTemp = s.Split(new Char[] { ' ' });
                    writer.WriteAttributeString("報銷編號", myTemp[0]);
                    writer.WriteElementString("單號", myTemp[1]);

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_ExpenseEZFlow.Code) && (myEZFlowExpenses != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myEZFlowExpenses.Split(new Char[] { '\t' });
                foreach (string s in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");

                    string[] myTemp = s.Split(new Char[] { ' ' });
                    writer.WriteAttributeString("報銷編號", myTemp[0]);

                    writer.WriteStartElement("EZFlow單號");
                    string[] myEZFlowFormNos = myTemp[1].Split(new Char[] { '*' });
                    foreach (string fn in myEZFlowFormNos)
                    { 
                        writer.WriteElementString("單號", fn);
                    }
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_ExpenseNotApproved.Code) && (myNotApprovedExpenses != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myNotApprovedExpenses.Split(new Char[] { ' ' });
                foreach (string expID in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");
                    writer.WriteAttributeString("報銷編號", expID);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_ExpenseNotLinked.Code) && (myNotLinkedExpenses != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myNotLinkedExpenses.Split(new Char[] { ' ' });
                foreach (string expID in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");
                    writer.WriteAttributeString("報銷編號", expID);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_Unknown.Code) && (mstrSysErrMessage != ""))
            {
                writer.WriteStartElement("系統錯誤訊息");
                writer.WriteString(mstrSysErrMessage);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            //XmlDocument myDoc = new XmlDocument();
            //myDoc.LoadXml(sb.ToString());
            return sb.ToString();
        }

        [WebMethod]
        public string GoProcess(string ECPFormNos, string ECPEventCode, string EmpID, string ProcessMode)
        {
            //輸出資料:
            //RetID：'0' 表示成功；非 '0' 表示失敗 (請自行定義，原因放置於 ErrMsg 中)
            //ErrMsg：錯誤訊息
            //ErrECPForms：錯誤的ECP表單 (單號)
            //ErrExpenses：錯誤的費用報銷紀 (報銷編號、單號、目前狀態)
            string myFormNoSQL = "";
            string myReturnCode = R_Unknown.Code;
            string myErrMessage = R_Unknown.Description;
            string myErrFormNos = "";
            string myErrStatusData = "";

            ECPFormNos = ECPFormNos.Trim();
            string[] myFormNos = ECPFormNos.Split(new Char[] { ' ' });
            foreach (string FormNo in myFormNos)
            {
                if (FormNo.Trim() != "") { myFormNoSQL += (myFormNoSQL == "" ? "" : ", ") + "'" + FormNo.Trim() + "'"; }
            }
            if (myFormNoSQL == "")
            {
                myReturnCode = R_NoForm.Code;
                myErrMessage = R_NoForm.Description;
                goto EndProcess;
            }

            EmpID = EmpID.Trim();
            if (EmpID == "")
            {
                myReturnCode = R_NoUser.Code;
                myErrMessage = R_NoUser.Description;
                goto EndProcess;
            }

            if (ECPEventCode == clsECPEvent.ApproveByDepartment) { }
            else if (ECPEventCode == clsECPEvent.ApproveByAdm) { }
            else if (ECPEventCode == clsECPEvent.DisapproveByDepartment) { }
            else if (ECPEventCode == clsECPEvent.DisapproveByAdm) { }
            else if (ECPEventCode == clsECPEvent.Withdraw) { }
            else
            {
                myReturnCode = R_InvalidEvent.Code;
                myErrMessage = R_InvalidEvent.Description;
                goto EndProcess;
            }

            if (ProcessMode == clsMode.CheckOnly) { }
            else if (ProcessMode == clsMode.UpdateAtOnce) { }
            else
            {
                myReturnCode = R_InvalidMode.Code;
                myErrMessage = R_InvalidMode.Description;
                goto EndProcess;
            }

            string myConnStr = ConfigurationManager.ConnectionStrings["PTSProgram-Expense"].ToString();
            using (SqlConnection conn = new SqlConnection(myConnStr))
            {
                try
                {
                    conn.Open();

                    // 檢查執行者是否為使用者
                    string myUserID = GetUserID(conn, EmpID);
                    if (myUserID == "")
                    {
                        myReturnCode = R_InvalidUser.Code;
                        myErrMessage = R_InvalidUser.Description;
                    }
                    else
                    {
                        string myPendingFormNoSQL = "";
                        string myAdjustExpenseIDSQL = "";
                        string myFormNo;

                        SqlCommand cmd = new SqlCommand("", conn);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@ECPFormNo", SqlDbType.VarChar);
                        SqlDataReader reader;
                        myErrFormNos = "";
                        myErrStatusData = "";

                        foreach (string FormNo in myFormNos)
                        {
                            myFormNo = FormNo.Trim();
                            if (myFormNo != "")
                            {
                                // 檢查費用報銷是否可執行此動作
                                cmd.CommandText = "SELECT FSEXPENSEID, FCSTATUS, ISNULL(AdjustPending, CONVERT(BIT, 0)) myAdjustPending,"
                                    + " " + clsExpenseStatusSQL.SelectApproveSQL + " myStatus"
                                    + " FROM " + clsPTSPRogramTable.Expense
                                    + " WHERE ECPFormNo = @ECPFormNo";
                                cmd.Parameters["@ECPFormNo"].Value = myFormNo;

                                reader = cmd.ExecuteReader();
                                if (!reader.HasRows)
                                {
                                    myReturnCode = R_ExpenseNotFound.Code;
                                    myErrMessage = R_ExpenseNotFound.Description;
                                    myErrFormNos += (myErrFormNos == "" ? "" : " ") + myFormNo;
                                }
                                else
                                {
                                    string myExpenseID = ""; string myStatus = ""; bool myAdjustPending = false; string myExpenseIDSQL = "";
                                    while (reader.Read())
                                    {
                                        myExpenseID = reader.GetString(0);
                                        myExpenseIDSQL += (myExpenseIDSQL == "" ? "" : ", ") + "'" + myExpenseID + "'"; // 此單的報銷編號

                                        if (reader.GetBoolean(2)) { myAdjustPending = true; }
                                        if (!CheckStatus(reader.GetString(1), ECPEventCode))
                                        {
                                            myStatus = reader.GetString(3);
                                            myErrStatusData += (myErrStatusData == "" ? "" : "\t") + myFormNo + " " + myExpenseID + " " + myStatus;
                                        }
                                    }
                                    if (myErrStatusData != "")
                                    {
                                        myReturnCode = R_StatusError.Code;
                                        myErrMessage = R_StatusError.Description;
                                    }
                                    if (myAdjustPending)
                                    {
                                        myPendingFormNoSQL += (myPendingFormNoSQL == "" ? "" : ", ") + "'" + myFormNo + "'";
                                        myAdjustExpenseIDSQL += (myAdjustExpenseIDSQL == "" ? "" : ", ") + myExpenseIDSQL;
                                    }
                                }
                                reader.Close();
                            }
                        }   //foreach (string FormNo in myFormNos)

                        if ((myErrFormNos == "") && (myErrStatusData == ""))
                        {
                            if (ProcessMode == clsMode.CheckOnly)
                            {
                                myReturnCode = R_Success.Code;
                                myErrMessage = R_Success.Description;
                            }
                            else if (myPendingFormNoSQL == "")  //沒有錯帳表單
                            {
                                if (UpdateStatus(conn, myFormNoSQL, ECPEventCode, myUserID))    // 異動費用報銷狀態
                                {
                                    myReturnCode = R_Success.Code;
                                    myErrMessage = R_Success.Description;
                                }
                            }
                            else if (myPendingFormNoSQL == myFormNoSQL)  //全部是錯帳表單
                            {
                                if (UpdatePendingStatus(conn, myPendingFormNoSQL, ECPEventCode, myUserID, myAdjustExpenseIDSQL))
                                {
                                    myReturnCode = R_Success.Code;
                                    myErrMessage = R_Success.Description;
                                }
                            }
                            else    //部份是錯帳表單
                            {
                                string myCondition = "ECPFormNo NOT IN (" + myPendingFormNoSQL + ")";
                                if (UpdateStatus(conn, myFormNoSQL, ECPEventCode, myUserID, myCondition)    //非錯帳表單
                                    && (UpdatePendingStatus(conn, myPendingFormNoSQL, ECPEventCode, myUserID, myAdjustExpenseIDSQL))) //錯帳表單
                                {
                                    myReturnCode = R_Success.Code;
                                    myErrMessage = R_Success.Description;
                                }
                            }
                        }
                        else if ((myErrFormNos != "") && (myErrStatusData != ""))
                        {   //兩個錯誤狀態都有，先報找不到對應單號的費用報銷紀錄，方便使用者先排除
                            myErrStatusData = "";
                            myReturnCode = R_ExpenseNotFound.Code;
                            myErrMessage = R_ExpenseNotFound.Description;
                        }
                    }   //(myUserID == "")
                }
                catch (Exception ex)
                {
                    mstrSysErrMessage = "GoProcess(): " + ex.Message;
                }
            }

        EndProcess:
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlWriter.Create(sb, settings);
            writer.WriteStartDocument();
            writer.WriteStartElement("root");

            writer.WriteStartElement("RetID");
            writer.WriteString(myReturnCode);
            writer.WriteEndElement();

            writer.WriteStartElement("ErrMsg");
            writer.WriteString(myErrMessage);
            writer.WriteEndElement();

            if ((myReturnCode == R_ExpenseNotFound.Code) && (myErrFormNos != ""))
            {
                writer.WriteStartElement("ErrECPForms");
                string[] myDatas = myErrFormNos.Split(new Char[] { ' ' });
                foreach (string FormNo in myDatas)
                {
                    writer.WriteStartElement("ECP表單");
                    writer.WriteAttributeString("單號", FormNo);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_StatusError.Code) && (myErrStatusData != ""))
            {
                writer.WriteStartElement("ErrExpenses");
                string[] myDatas = myErrStatusData.Split(new Char[] { '\t' });
                foreach (string s in myDatas)
                {
                    writer.WriteStartElement("費用報銷紀錄");

                    string[] myTemp = s.Split(new Char[] { ' ' });
                    writer.WriteAttributeString("報銷編號", myTemp[1]);
                    writer.WriteElementString("單號", myTemp[0]);
                    writer.WriteElementString("目前狀態", myTemp[2]);

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            else if ((myReturnCode == R_Unknown.Code) && (mstrSysErrMessage != ""))
            {
                writer.WriteStartElement("系統錯誤訊息");
                writer.WriteString(mstrSysErrMessage);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();

            //XmlDocument myDoc = new XmlDocument();
            //myDoc.LoadXml(sb.ToString());
            return sb.ToString();
        }

        private string GetUserID(SqlConnection pConn, string pstrEmpID)
        {
            string myUserID = "";
            mstrSysErrMessage = "";

            SqlCommand cmd = new SqlCommand("SELECT FSUSERID FROM " + clsPTSPRogramTable.Users + " WHERE FSEMPNO = @EmpID", pConn);
            cmd.Parameters.Add("@EmpID", SqlDbType.VarChar);
            cmd.Parameters["@EmpID"].Value = pstrEmpID;
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    if (reader.Read()) { myUserID = reader.GetString(0); }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                mstrSysErrMessage = "GetUserID(): " + ex.Message;
            }

            return myUserID;
        }

        private bool CheckStatus(string pstrStatusCode, string pstrECPEventCode)
        {
            bool myResult = false;

            if ((pstrECPEventCode == clsECPEvent.ApproveByDepartment) || (pstrECPEventCode == clsECPEvent.DisapproveByDepartment))
            { //部門會計核可／駁回
                if ((pstrStatusCode == clsExpenseStatus.Applied) || (pstrStatusCode == clsExpenseStatus.PreApprovalWithdraw)) { myResult = true; }
            }
            else if ((pstrECPEventCode == clsECPEvent.ApproveByAdm) || (pstrECPEventCode == clsECPEvent.DisapproveByAdm))
            {//行政部會計核可／駁回
                if ((pstrStatusCode == clsExpenseStatus.PreApproved) || (pstrStatusCode == clsExpenseStatus.ApprovalWithdraw)) { myResult = true; }
            }
            else if (pstrECPEventCode == clsECPEvent.Withdraw)
            {//抽單
                if (pstrStatusCode != clsExpenseStatus.Approved) { myResult = true; }
            }

            return myResult;
        }

        private bool UpdateStatus(SqlConnection pConn, string pstrFormNoSQL, string pstrECPEventCode, string pstrUserID, string pstrCondition = "")
        {
            bool myResult = false; string mySQLCmd = "";
            mstrSysErrMessage = "";

            if (pstrECPEventCode == clsECPEvent.ApproveByDepartment)
            {//部門會計核可
                mySQLCmd = "FCSTATUS = '" + clsExpenseStatus.PreApproved + "',"
                        + " FSPREAPPROVEMAN = '" + pstrUserID + "',"
                        + " FDPREAPPROVEDATE = GETDATE()";
            }
            else if (pstrECPEventCode == clsECPEvent.ApproveByAdm)
            {//行政部會計核可
                mySQLCmd = "FCSTATUS = '" + clsExpenseStatus.Approved + "',"
                        + " FSAPPROVEMAN = '" + pstrUserID + "',"
                        + " FDAPPROVEDATE = GETDATE()";
            }
            else if ((pstrECPEventCode == clsECPEvent.DisapproveByDepartment)
                || (pstrECPEventCode == clsECPEvent.DisapproveByAdm)
                || (pstrECPEventCode == clsECPEvent.Withdraw))
            {//部門會計駁回 行政部會計駁回 抽單
                mySQLCmd = "ECPFormNo = NULL, FCSTATUS = '" + clsExpenseStatus.Applied + "',"
                        + " FSPREAPPROVEMAN = NULL, FDPREAPPROVEDATE = NULL,"
                        + " FSAPPROVEMAN = NULL, FDAPPROVEDATE = NULL";
            }
            else
            {
                return false;
            }

            SqlCommand cmd = pConn.CreateCommand();
            SqlTransaction myTran = pConn.BeginTransaction("UpdateExpenses");
            cmd.Transaction = myTran;
            try
            {
                string myFormCondition = "ECPFormNo IN (" + pstrFormNoSQL + ")"
                    + (pstrCondition == "" ? "" : " AND " + pstrCondition);

                cmd.Parameters.Clear();
                cmd.CommandText = "INSERT INTO " + clsPTSPRogramTable.Expense_History
                        + " SELECT * FROM " + clsPTSPRogramTable.Expense
                        + " WHERE " + myFormCondition;
                int intRow = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                + " SET FNCHGNO = FNCHGNO + 1, " + mySQLCmd
                                + " WHERE " + myFormCondition;
                intRow = cmd.ExecuteNonQuery();

                myTran.Commit();
                myResult = true;
            }
            catch (Exception ex)
            {
                mstrSysErrMessage = "UpdateStatus() " + ex.GetType() + ": " + ex.Message;

                try
                {
                    myTran.Rollback();
                }
                catch (Exception ex2)
                {
                    mstrSysErrMessage += "    UpdateStatus() " + ex2.GetType() + ": " + ex2.Message;
                }
            }

            return myResult;
        }

        private bool UpdatePendingStatus(SqlConnection pConn, string pstrFormNoSQL, string pstrECPEventCode, string pstrUserID, string pstrAdjustExpenseIDSQL)
        {
            bool myResult = false; string mySQLCmd = "";
            mstrSysErrMessage = "";

            if (pstrECPEventCode == clsECPEvent.ApproveByDepartment)
            { //部門會計核可
                mySQLCmd = "FCSTATUS = '" + clsExpenseStatus.PreApproved + "',"
                        + " FSPREAPPROVEMAN = '" + pstrUserID + "',"
                        + " FDPREAPPROVEDATE = GETDATE()";
            }
            else if (pstrECPEventCode == clsECPEvent.ApproveByAdm)
            { //行政部會計核可
                mySQLCmd = "FCSTATUS = '" + clsExpenseStatus.Approved + "',"
                        + " FSAPPROVEMAN = '" + pstrUserID + "',"
                        + " FDAPPROVEDATE = GETDATE()";
            }
            else if ((pstrECPEventCode == clsECPEvent.DisapproveByDepartment)
                || (pstrECPEventCode == clsECPEvent.DisapproveByAdm)
                || (pstrECPEventCode == clsECPEvent.Withdraw))
            { //部門會計駁回 行政部會計駁回 抽單
                mySQLCmd = "ECPFormNo = X.ECPFormNo,"
                        + " FCSTATUS = '" + clsExpenseStatus.Approved + "',"
                        + " FSPREAPPROVEMAN = X.FSPREAPPROVEMAN, FDPREAPPROVEDATE = X.FDPREAPPROVEDATE,"
                        + " FSAPPROVEMAN = X.FSAPPROVEMAN, FDAPPROVEDATE = X.FDAPPROVEDATE";
            }
            else
            {
                return false;
            }

            SqlCommand cmd = pConn.CreateCommand();
            SqlTransaction myTran = pConn.BeginTransaction("UpdateExpenses");
            cmd.Transaction = myTran;
            try
            {
                string myFormCondition = "ECPFormNo IN (" + pstrFormNoSQL + ")";

                cmd.Parameters.Clear();
                cmd.CommandText = "INSERT INTO " + clsPTSPRogramTable.Expense_History
                        + " SELECT * FROM " + clsPTSPRogramTable.Expense
                        + " WHERE " + myFormCondition;
                int intRow = cmd.ExecuteNonQuery();

                if (pstrECPEventCode == clsECPEvent.ApproveByDepartment)
                { //部門會計核可
                    cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                    + " SET FNCHGNO = FNCHGNO + 1, " + mySQLCmd
                                    + " WHERE " + myFormCondition;
                    intRow = cmd.ExecuteNonQuery();
                }
                else if (pstrECPEventCode == clsECPEvent.ApproveByAdm)
                { //行政部會計核可
                    cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                    + " SET FNCHGNO = FNCHGNO + 1, " + mySQLCmd
                                    + " WHERE " + myFormCondition
                                    + " AND (AdjustPending IS NULL OR AdjustPending = 0)";
                    intRow = cmd.ExecuteNonQuery();

                    cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                    + " SET FNCHGNO = FNCHGNO + 1, AdjustPending = 0, " + mySQLCmd  //清除錯帳設定
                                    + " WHERE " + myFormCondition
                                    + " AND AdjustPending <> 0";
                    intRow = cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE FROM " + clsPTSPRogramTable.Expense_Adjusting //清除錯帳前備份資料
                                    + " WHERE FSEXPENSEID IN (" + pstrAdjustExpenseIDSQL + ")";
                    intRow = cmd.ExecuteNonQuery();
                }
                else if ((pstrECPEventCode == clsECPEvent.DisapproveByDepartment)
                    || (pstrECPEventCode == clsECPEvent.DisapproveByAdm)
                    || (pstrECPEventCode == clsECPEvent.Withdraw))
                { //部門會計駁回 行政部會計駁回 抽單
                    cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                    + " SET FNCHGNO = FNCHGNO + 1, " + mySQLCmd
                                    + " FROM " + clsPTSPRogramTable.Expense_Adjusting + " X"
                                    + " WHERE " + clsPTSPRogramTable.Expense + ".ECPFormNo IN (" + pstrFormNoSQL + ")"
                                    + " AND " + clsPTSPRogramTable.Expense + ".FSEXPENSEID = X.FSEXPENSEID";
                    intRow = cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE FROM " + clsPTSPRogramTable.Expense_Adjusting //清除錯帳前備份資料
                                    + " WHERE FSEXPENSEID IN (" + pstrAdjustExpenseIDSQL + ")";
                    intRow = cmd.ExecuteNonQuery();
                }

                myTran.Commit();
                myResult = true;
            }
            catch (Exception ex)
            {
                mstrSysErrMessage = "UpdatePendingStatus() " + ex.GetType() + ": " + ex.Message;

                try
                {
                    myTran.Rollback();
                }
                catch (Exception ex2)
                {
                    mstrSysErrMessage += "    UpdatePendingStatus() " + ex2.GetType() + ": " + ex2.Message;
                }
            }

            return myResult;
        }

        private string CheckEZFlow(SqlConnection pConn, string pstrExpenseID)
        {
            string myEZFlowFormNos = "";
            mstrSysErrMessage = "";

            string strCheck = "-" + pstrExpenseID + "：";
            SqlCommand cmd = new SqlCommand("SELECT FormNo FROM " + clsEZFlowTable.WorkingExpenseForms
                + " WHERE Memo LIKE N'%" + strCheck + "%'"
                + " ORDER BY FormNo DESC", pConn);
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        myEZFlowFormNos += (myEZFlowFormNos == "" ? "" : "*") + reader.GetString(0);
                    }
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                mstrSysErrMessage = "CheckEZFlow(): " + ex.Message;
            }
            return myEZFlowFormNos;
        }

        private bool UpdateFormNo(SqlConnection pConn, string pstrECPFormNo, string pstrExpenseIDSQL, bool pblnAdjustPending)
        {
            bool myResult = false;
            mstrSysErrMessage = "";

            SqlCommand cmd = pConn.CreateCommand();
            SqlTransaction myTran = pConn.BeginTransaction("UpdateFormNo");
            cmd.Transaction = myTran;
            try
            {
                int intRow;
                if (pblnAdjustPending)
                {
                    // 紀錄原單號及原簽核資料，駁回、抽單時 Rollback 用
                    cmd.CommandText = "INSERT INTO " + clsPTSPRogramTable.Expense_Adjusting
                            + " SELECT FSEXPENSEID, FSPREAPPROVEMAN, FDPREAPPROVEDATE, FSAPPROVEMAN, FDAPPROVEDATE, ECPFormNo FROM " + clsPTSPRogramTable.Expense
                            + " WHERE FSEXPENSEID IN (" + pstrExpenseIDSQL + ")";
                    intRow = cmd.ExecuteNonQuery();

                    // 留下原會計核准資料及原單號於簽核歷程中
                    cmd.CommandText = "INSERT INTO " + clsPTSPRogramTable.Expense_History
                            + " SELECT * FROM " + clsPTSPRogramTable.Expense
                            + " WHERE FSEXPENSEID IN (" + pstrExpenseIDSQL + ")";
                    intRow = cmd.ExecuteNonQuery();
                }

                cmd.Parameters.Clear();
                
                cmd.CommandText = "UPDATE " + clsPTSPRogramTable.Expense
                                + " SET ECPFormNo = @ECPFormNo"
                                + (pblnAdjustPending ? ", FNCHGNO = FNCHGNO + 1, FCSTATUS = '" + clsExpenseStatus.Applied + "'" : "")  // 錯帳時將狀態改為未審
                                + " WHERE FSEXPENSEID IN (" + pstrExpenseIDSQL + ")";
                cmd.Parameters.Add("@ECPFormNo", SqlDbType.VarChar);
                cmd.Parameters["@ECPFormNo"].Value = pstrECPFormNo;
                intRow = cmd.ExecuteNonQuery();

                myTran.Commit();
                myResult = true;
            }
            catch (Exception ex)
            {
                mstrSysErrMessage = "UpdateFormNo() " + ex.GetType() + ": " + ex.Message;

                try
                {
                    myTran.Rollback();
                }
                catch (Exception ex2)
                {
                    mstrSysErrMessage += "    UpdateFormNo() " + ex2.GetType() + ": " + ex2.Message;
                }
            }

            return myResult;
        } 
    }
}
