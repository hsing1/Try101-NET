﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HitTheKeys
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        Stats stats = new Stats();
        int progressBarInitValue;
        int level1;
        int level2;
        int level3;
        int d1;
        int d2;
        int d3;

        public Form1()
        {
            InitializeComponent();
            difficultyProgressBar.Maximum = this.timer1.Interval - 9;
            progressBarInitValue = this.timer1.Interval;
            level1 = this.progressBarInitValue / 2;
            level2 = level1 / 2;
            level3 = level3 / 2;
            d1 = this.progressBarInitValue / 200;
            d2 = this.progressBarInitValue / 150;
            d3 = this.progressBarInitValue / 80;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            listBox1.Items.Add((Keys)random.Next(65, 90));
            if (listBox1.Items.Count > 7)
            {
                listBox1.Items.Clear();
                listBox1.Items.Add("Game over!");
                timer1.Stop();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (listBox1.Items.Contains(e.KeyCode))
            {
                listBox1.Items.Remove(e.KeyCode);
                listBox1.Refresh();
                if (timer1.Interval > level1)
                    timer1.Interval -= d1;
                if (timer1.Interval > level2)
                    timer1.Interval -= d2;
                if (timer1.Interval > level3)
                    timer1.Interval -= d3;

                difficultyProgressBar.Value = progressBarInitValue - timer1.Interval;

                stats.Update(true);
            }
            else
            {
                stats.Update(false);
            }

            correctLabel.Text = "Correct: " + stats.Correct;
            missedLabel.Text = "Missed: " + stats.Missed;
            totalLabel.Text = "Total: " + stats.Total;
            accuracyLabel.Text = "Accurace: " + stats.Accuracy + "%";
            timerTickerLabel.Text = "Timer ticker: " + timer1.Interval;
        }

    }
}
